package eu.ntdev.raidreport.parser.model.types;

import eu.ntdev.raidreport.parser.model.RaidAgent;
import lombok.Data;

@Data
public class RaidPlayer {
    private RaidAgent agent;
    private String characterName;
    private String accountName;
    private byte group;

    public boolean accountNumberMatches(String theirAccountName) {
        String thisNumbers = accountName.substring(accountName.length() - 4,accountName.length() - 1);
        String theirNumbers = theirAccountName.substring(theirAccountName.length() - 4, theirAccountName.length() - 1);
        return thisNumbers.equals(theirNumbers);
    }
}
