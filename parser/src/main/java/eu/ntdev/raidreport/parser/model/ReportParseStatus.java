package eu.ntdev.raidreport.parser.model;

public enum ReportParseStatus {
    HEADER_FAIL,
    ACTORS_FAIL,
    EVENTS_FAIL,
    SUCCESS;
}
