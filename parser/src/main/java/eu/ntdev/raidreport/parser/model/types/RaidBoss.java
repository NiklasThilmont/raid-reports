package eu.ntdev.raidreport.parser.model.types;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
public class RaidBoss {
   /* VALE_GUARDIAN(-1, Collections.singletonList(0x3C4EL)),
    GORSEVAL(15429, Collections.singletonList(0x3C45L)),
    SABETHA(-1, Collections.singletonList(0x3C0FL)),
    SLOTHASOR(-1, Collections.singletonList(0x3EFBL)),
    TRIO(-1, Arrays.asList(0x3ED8L, 0x3F09L, 0x3EFDL)),
    MATTHIAS(-1, Collections.singletonList(0x3EF3L)),
    KEEP_CONSTRUCT(-1, Collections.singletonList(0x3F6BL)),
    XERA(-1, Arrays.asList(0x3F76L, 0x3F9EL)),
    CAIRN(-1, Collections.singletonList(0x432AL)),
    OVERSEER(-1, Collections.singletonList(0x4314L)),
    SAMAROG(-1, Collections.singletonList(0x4324L)),
    DEIMOS(17221, Collections.singletonList(0x4302L));*/

    private String name = "";
    private List<Long> evtcIds = new ArrayList<>();
}
