package eu.ntdev.raidreport.parser.model;

import lombok.Data;

@Data
public class Skill {
    int id;
    String name;

    public Skill(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
