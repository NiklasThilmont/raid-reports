package eu.ntdev.raidreport.parser.model.types;

import org.omg.CORBA.UNKNOWN;

/**
 * Created by Niklas on 22.07.2017.
 */
public enum CombatActivationType {
    ACTV_NONE, // not used - not this kind of event
    ACTV_NORMAL, // activation without quickness
    ACTV_QUICKNESS, // activation with quickness
    ACTV_CANCEL_FIRE, // cancel with reaching channel time
    ACTV_CANCEL_CANCEL, // cancel without reaching full channel time
    ACTV_RESET, // agent skill animation reset/ready (no skill id, keep track locally via previous events)
    UNKNOWN;


    public static CombatActivationType from(byte val) {
        if(val == 0) return ACTV_NONE;
        if(val == 1) return ACTV_NORMAL;
        if(val == 2) return ACTV_QUICKNESS;
        if(val == 3) return ACTV_CANCEL_FIRE;
        if(val == 4) return ACTV_CANCEL_CANCEL;
        if(val == 5) return ACTV_RESET;
       return UNKNOWN;
    }
}
