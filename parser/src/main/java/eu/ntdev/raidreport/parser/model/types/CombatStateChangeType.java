package eu.ntdev.raidreport.parser.model.types;

import org.omg.CORBA.UNKNOWN;

public enum CombatStateChangeType {
    CBTS_NONE, // not used - not this kind of event
    CBTS_ENTERCOMBAT, // src_agent entered combat, dst_agent is subgroup
    CBTS_EXITCOMBAT, // src_agent left combat
    CBTS_CHANGEUP, // src_agent is now alive
    CBTS_CHANGEDEAD, // src_agent is now dead
    CBTS_CHANGEDOWN, // src_agent is now downed
    CBTS_SPAWN, // src_agent is now in game tracking range
    CBTS_DESPAWN, // src_agent is no longer being tracked
    CBTS_HEALTHUPDATE, // src_agent has reached a health marker. dst_agent = percent * 10000 (eg. 99.5% will be 9950)
    CBTS_LOGSTART, // log start. value = server unix timestamp **uint32**. buff_dmg = local unix timestamp. src_agent = 0x637261 (arcdps id)
    CBTS_LOGEND, // log end. value = server unix timestamp **uint32**. buff_dmg = local unix timestamp. src_agent = 0x637261 (arcdps id)
    CBTS_WEAPSWAP, // src_agent swapped weapon set. dst_agent = current set id (0/1 water, 4/5 land)
    CBTS_MAXHEALTHUPDATE, // src_agent has had it's maximum health changed. dst_agent = new max health
    CBTS_POINTOFVIEW, // src_agent will be agent of "recording" player
    CBTS_LANGUAGE, // src_agent will be text language
    CBTS_GWBUILD, // src_agent will be game build
    CBTS_SHARDID, // src_agent will be sever shard id
    CBTS_REWARD,
    UNKNOWN // not recognised by this parser.
    ; // src_agent is self, dst_agent is reward id, value is reward type. these are the wiggly boxes that you get

    public static CombatStateChangeType from(byte value) {
        CombatStateChangeType[] values = CombatStateChangeType.values();
        if(value < values.length) {
            return values[value];
        }
        return UNKNOWN;
    }
}
