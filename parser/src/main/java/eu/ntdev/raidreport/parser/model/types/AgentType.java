package eu.ntdev.raidreport.parser.model.types;

/**
 * Created by Niklas on 22.07.2017.
 */
public enum AgentType {
    // Constants
    NPC_CHARACTER(-1, -1),
    GADGET(0, -1),

    GUARDIAN(1, 0),
    DRAGONHUNTER(1, 27),
    FIREBRAND(1, 62),

    WARRIOR(2, 0),
    BERSERKER(2, 18),
    SPELLBREAKER(2, 61),

    ENGINEER(3, 0),
    SCRAPPER(3, 43),
    HOLOSMITH(3, 57),

    RANGER(4, 0),
    DRUID(4, 5),
    SOULBEAST(4, 55),

    THIEF(5, 0),
    DAREDEVIL(5, 7),
    DEADEYE(5, 58),

    ELEMENTALIST(6, 0),
    TEMPEST(6, 48),
    WEAVER(6, 56),

    MESMER(7, 0),
    CHRONOMANCER(7, 40),
    MIRAGE(7, 59),

    NECROMANCER(8, 0),
    REAPER(8, 34),
    SCOURGE(8, 60),

    REVENANT(9, 0),
    HERALD(9, 52),
    RENEGADE(9, 63),
    UNKOWN(-123412, -123131);

    private int professionId;
    private int eliteId;

    AgentType(int professionId, int eliteId) {
        this.professionId = professionId;
        this.eliteId = eliteId;
    }

    public static AgentType of(int professionId, int eliteId) {

        // if cbtevent.is_elite == 0xffffffff && upper half of cbtevent.prof == 0xffff, agent is a gadget with pseudo id
        // as lower half of cbtevent.prof (volatile id).
        if(eliteId == -1 && ((professionId & 0xFFFF0000) == 0xFFFF0000)) {
            return GADGET;
        }
        // if cbtevent.is_elite == 0xffffffff && upper half of cbtevent.prof != 0xffff, agent is a character with species
        // id as lower half of cbtevent.prof (reliable id).
        if(eliteId == -1 && ((professionId & 0xFFFF0000) != 0xFFFF0000)) {
            return NPC_CHARACTER;
        }
        // if cbtevent.is_elite != 0xffffffff, agent is a player with profession as cbtevent.prof and elite spec
        // as cbtevent.is_elite.
        for (AgentType agentType : AgentType.values()) {
            if(agentType.professionId == professionId && agentType.eliteId == eliteId) {
                return agentType;
            }
        }
        return UNKOWN;
    }
}
