package eu.ntdev.raidreport.parser.model;

import eu.ntdev.raidreport.parser.model.types.*;
import lombok.Data;

import java.util.Date;

@Data
public class CombatEvent {
    Date date;
    RaidAgent sourceAgent;
    RaidAgent targetAgent;
    Skill skill;


    int value;
    int buffDamage;
    short overstackValue;

    short srcInstanceId;
    short destInstanceId;
    short srcMasterInstanceId;
    TargetType iff; // supposed to be an enum; 0 = friend, 1 = foe, 2 = unknown
    byte buff;
    CombatResultType result;
    CombatActivationType combatActivationType;
    BuffRemovalType buffRemovalType;
    boolean isOverNinety; // Source agent health over 90%
    boolean isUnderFifty; // Target agent health under 50%
    boolean isMoving; // Source agent was moving
    CombatStateChangeType combatStateChangeType;
    boolean isFlanking;
    // Temporary fields used to store some data.
    long sourceAgentId;
    long targetAgentId;
    short skillId;
}
