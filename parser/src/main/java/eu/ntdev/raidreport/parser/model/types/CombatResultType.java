package eu.ntdev.raidreport.parser.model.types;

import org.omg.CORBA.UNKNOWN;

/**
 * Created by Niklas on 22.07.2017.
 */
public enum CombatResultType {
    CBTR_NORMAL, // good physical hit
    CBTR_CRIT, // physical hit was crit
    CBTR_GLANCE, // physical hit was glance
    CBTR_BLOCK, // physical hit was blocked eg. mesmer shield 4
    CBTR_EVADE, // physical hit was evaded, eg. dodge or mesmer sword 2
    CBTR_INTERRUPT, // physical hit interrupted something
    CBTR_ABSORB, // physical hit was "invlun" or absorbed eg. guardian elite
    CBTR_BLIND, // physical hit missed
    CBTR_KILLINGBLOW, // physical hit was killing hit
    UNKNOWN
    ;

    public static CombatResultType from(byte val) {
        if(val == 0) return CBTR_NORMAL;
        if(val == 1) return CBTR_CRIT;
        if(val == 2) return CBTR_GLANCE;
        if(val == 3) return CBTR_BLOCK;
        if(val == 4) return CBTR_EVADE;
        if(val == 5) return CBTR_INTERRUPT;
        if(val == 6) return CBTR_ABSORB;
        if(val == 7) return CBTR_BLIND;
        if(val == 8) return CBTR_KILLINGBLOW;
        return UNKNOWN;
    }
}
