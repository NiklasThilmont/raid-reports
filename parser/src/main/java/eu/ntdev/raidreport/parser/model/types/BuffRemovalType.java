package eu.ntdev.raidreport.parser.model.types;

/**
 * Created by Niklas on 22.07.2017.
 */
public enum BuffRemovalType {
    CBTB_NONE, // not used - not this kind of event
    CBTB_ALL, // all stacks removed
    CBTB_SINGLE, // single stack removed. disabled on server trigger, will happen for each stack on cleanse
    CBTB_MANUAL, // autoremoved by ooc or allstack (ignore for strip/cleanse calc, use for in/out volume)
    UNKNOWN;

    public static BuffRemovalType from(byte val) {
        if(val == 0) {
            return CBTB_NONE;
        }
        if(val == 1) {
            return CBTB_ALL;
        }
        if(val == 2) {
            return CBTB_SINGLE;
        }
        if(val == 3) {
            return CBTB_MANUAL;
        }
        return UNKNOWN;
    }
}
