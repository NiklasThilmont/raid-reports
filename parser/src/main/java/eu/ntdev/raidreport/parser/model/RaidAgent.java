package eu.ntdev.raidreport.parser.model;

import eu.ntdev.raidreport.parser.model.types.AgentType;
import lombok.Data;

@Data
public class RaidAgent {
    private long id;
    private int prof;
    private int isElite;
    private int toughness;
    private int healing;
    private int condition;
    private String name;
    private AgentType agentType;

    public RaidAgent(long address, int prof, int isElite, int toughness, int healing, int condition, String name) {
        this.id = address;
        this.prof = prof;
        this.isElite = isElite;
        this.toughness = toughness;
        this.healing = healing;
        this.condition = condition;
        this.name = name;
        agentType = AgentType.of(prof, isElite);
    }

    public boolean isPlayer() {
        return isElite != -1 && prof >= 0;
    }

}
