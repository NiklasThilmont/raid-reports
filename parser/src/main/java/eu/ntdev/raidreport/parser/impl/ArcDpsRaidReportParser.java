package eu.ntdev.raidreport.parser.impl;

import eu.ntdev.raidreport.parser.RaidReportParser;
import eu.ntdev.raidreport.parser.model.*;
import eu.ntdev.raidreport.parser.model.types.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ArcDpsRaidReportParser implements RaidReportParser {

    private DateFormat evtcDateDTF = new SimpleDateFormat("yyyyMMdd");

    private ByteOrder byteOrder = ByteOrder.LITTLE_ENDIAN;

    private static Log logger = LogFactory.getLog(ArcDpsRaidReportParser.class);

    public ArcDpsRaidReportParser(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    private Date parseEvtcBuildDate(String dateString) throws ParseException {
        return evtcDateDTF.parse(dateString);
    }

    /**
     * Reads the next numOfBytes as String from the stream
     * @param inputStream
     * @param numOfBytes
     * @return
     */
    private String readAsString(InputStream inputStream, int numOfBytes) throws IOException {
        byte[] bytes = new byte[numOfBytes];
        inputStream.read(bytes, 0, numOfBytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    private boolean readBoolean(InputStream inputStream) throws IOException {
        if(inputStream.available() >= 1) {
            int val = inputStream.read();
            if(val == 0) return false;
            return true;
        } else {
            throw new IllegalArgumentException("Stream has no more bytes available.");
        }
    }

    private byte readByte(InputStream inputStream) throws IOException {
        if(inputStream.available() >= 1) {
            int val = inputStream.read();
            if(val <= 255 && val >= 0) {
                return (byte)val;
            }
            throw new IllegalArgumentException("Could not read byte from stream. Was: " + val);
        } else {
            throw new IllegalArgumentException("Stream has no more bytes available.");
        }
    }

    private int readBytesAsInteger(InputStream inputStream, int numOfBytes) throws IOException {
        if(numOfBytes > 4) throw new RuntimeException("Max 4 bytes allowed for an int");
        byte[] bytes = new byte[numOfBytes];
        if(inputStream.available() >= numOfBytes) {
            inputStream.read(bytes);
            return ByteBuffer.wrap(bytes).order(byteOrder).getInt();
        } else {
            throw new RuntimeException("Not enough bytes available. Needed: " + numOfBytes + ", was " + inputStream.available());
        }
    }

    private short readBytesAsShort(InputStream inputStream, int numOfBytes) throws IOException {
        if(numOfBytes > 2) throw new IllegalArgumentException("Max 2 bytes allowed for a short. Was " + numOfBytes);
        byte[] bytes = new byte[numOfBytes];
        if(inputStream.available() >= numOfBytes) {
            inputStream.read(bytes);
            return ByteBuffer.wrap(bytes).order(byteOrder).getShort();
        } else {
            throw new RuntimeException("Not enough bytes available. Needed: " + numOfBytes + ", was " + inputStream.available());
        }
    }

    private long readBytesAsLong(InputStream inputStream, int numOfBytes) throws IOException  {
        if(numOfBytes > 8) throw new RuntimeException("Max 8 bytes allowed for an long");
        byte[] bytes = new byte[numOfBytes];
        if(inputStream.available() >= numOfBytes) {
            inputStream.read(bytes);
            return ByteBuffer.wrap(bytes).order(byteOrder).getLong();
        } else {
            throw new RuntimeException("Not enough bytes available. Needed: " + numOfBytes + ", was " + inputStream.available());
        }
    }

    private boolean isZeroByte(InputStream inputStream) throws IOException {
        return inputStream.read() == 0;
    }

    private RaidPlayer parsePlayer(RaidAgent raidAgent) {
        String[] split = raidAgent.getName().split("\u0000");
        RaidPlayer raidPlayer = new RaidPlayer();
        raidPlayer.setAgent(raidAgent);
        raidPlayer.setCharacterName(split[0]);
        raidPlayer.setAccountName(split[1].substring(1, split[1].length()));
        raidPlayer.setGroup(Byte.parseByte(split[2]));
        return raidPlayer;
    }

    private RaidAgent parseAgent(InputStream inputStream) throws IOException, ParseException {
        // 64 bit/ 8 byte id;
        Long address = readBytesAsLong(inputStream, 8);
        // Number of ints that are 4 bytes each(c++ type int32_t)
        Integer profession = readBytesAsInteger(inputStream, 4);
        Integer isElite = readBytesAsInteger(inputStream, 4);
        Integer toughness = readBytesAsInteger(inputStream, 4);
        Integer healing = readBytesAsInteger(inputStream, 4);
        Integer condition = readBytesAsInteger(inputStream, 4);
        String charName = readAsString(inputStream, 64);
        // 4 bytes padding
        inputStream.skip(4);
        return new RaidAgent(address, profession, isElite, toughness, healing, condition, charName);

    }

    private Skill parseSkill(InputStream inputStream) throws IOException {
        Integer id = readBytesAsInteger(inputStream, 4);
        String name = readAsString(inputStream, 64);
        return new Skill(id, name);
    }


    private CombatEvent parseCombatEvent(InputStream inputStream) throws IOException {
        CombatEvent combatEvent = new CombatEvent();
        long dateLong = readBytesAsLong(inputStream, 8);
        combatEvent.setDate(new Date(dateLong));

        combatEvent.setSourceAgentId(readBytesAsLong(inputStream, 8));
        combatEvent.setTargetAgentId(readBytesAsLong(inputStream, 8));
        // Up to here 24 byte

        combatEvent.setValue(readBytesAsInteger(inputStream, 4));
        combatEvent.setBuffDamage(readBytesAsInteger(inputStream, 4));
        // Up to here 32 byte

        combatEvent.setOverstackValue(readBytesAsShort(inputStream, 2));
        combatEvent.setSkillId(readBytesAsShort(inputStream, 2));
        combatEvent.setSrcInstanceId(readBytesAsShort(inputStream, 2));
        combatEvent.setDestInstanceId(readBytesAsShort(inputStream, 2));
        combatEvent.setSrcMasterInstanceId(readBytesAsShort(inputStream, 2));
        // Up to here 42 byte

        // Skip 9x 1 byte garbage
        inputStream.skip(9);
        combatEvent.setIff(TargetType.from(readByte(inputStream)));
        // 52 byte
        combatEvent.setBuff(readByte(inputStream));
        combatEvent.setResult(CombatResultType.from(readByte(inputStream)));
        combatEvent.setCombatActivationType(CombatActivationType.from(readByte(inputStream)));
        combatEvent.setBuffRemovalType(BuffRemovalType.from(readByte(inputStream)));
        combatEvent.setOverNinety(readBoolean(inputStream));
        combatEvent.setUnderFifty(readBoolean(inputStream));
        combatEvent.setMoving(readBoolean(inputStream));
        combatEvent.setCombatStateChangeType(CombatStateChangeType.from(readByte(inputStream)));
        combatEvent.setFlanking(readBoolean(inputStream));
        // 61 Byte
        // Skip 2x 1 byte garbage
        inputStream.skip(3);
        // 64 byte
        return combatEvent;
    }

    public RaidReport parse(InputStream inputStream) throws IOException, ParseException {
        RaidReport raidReport = new RaidReport();
        // Bytes 0-11 need to be "EVTCYYYYMMDD"
        // Bytes 0-3 need to be 'EVTC'
        String evtc = readAsString(inputStream, 4);
        if(!"EVTC".equals(evtc)) {
            raidReport.setReportParseStatus(ReportParseStatus.HEADER_FAIL);
            logger.error("Could not parse header. Expected: EVTC. Was:" + evtc);
            return raidReport;
        }
        // Now date
        String dateString = readAsString(inputStream, 8);
        raidReport.setBuildDate(parseEvtcBuildDate(dateString));
        // assert zero byte after meta info for version + date
        if(!isZeroByte(inputStream)) {
            raidReport.setReportParseStatus(ReportParseStatus.HEADER_FAIL);
            logger.error("Expected zero byte at after date.");
            return raidReport;
        }

        // 2 bytes boss ID
        int bossId = readBytesAsShort(inputStream, 2);
        raidReport.setTargetSpeciedId(bossId);
        // assert zero byte after meta info for boss id
        if(!isZeroByte(inputStream)) {
            raidReport.setReportParseStatus(ReportParseStatus.HEADER_FAIL);
            logger.error("Expected zero byte after boss.");
            return raidReport;
        }

        // read 32 bit/ 4 byte agent count
        Integer agentCount = readBytesAsInteger(inputStream, 4);
        raidReport.setAgentCount(agentCount);
        try {
            for(int i = 0; i < agentCount; i++) {
                RaidAgent agent = parseAgent(inputStream);
                raidReport.addAgent(agent);
                if(agent.isPlayer()) {
                    RaidPlayer player = parsePlayer(agent);
                    raidReport.getPlayers().add(player);
                }
            }
        } catch (IllegalArgumentException e) {
            raidReport.setReportParseStatus(ReportParseStatus.ACTORS_FAIL);
            logger.error("Could not parse agents:", e);
            return raidReport;
        }



        Integer skillCount = readBytesAsInteger(inputStream, 4);
        for(int i = 0; i < skillCount; i++) {
            Skill skill = parseSkill(inputStream);
            raidReport.addSkill(skill);
        }

        try {
            while(inputStream.available() >= 64) {
                CombatEvent combatEvent = parseCombatEvent(inputStream);
                raidReport.addCombatEvent(combatEvent);
            }
        } catch (IllegalArgumentException e) {
            raidReport.setReportParseStatus(ReportParseStatus.EVENTS_FAIL);
            logger.error("Could not parse combat events:", e);
            return raidReport;
        }
        raidReport.setReportParseStatus(ReportParseStatus.SUCCESS);
        return raidReport;
    }
}
