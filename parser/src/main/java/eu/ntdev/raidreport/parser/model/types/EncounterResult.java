package eu.ntdev.raidreport.parser.model.types;

/**
 * Created by Niklas on 18.11.2017.
 */
public enum EncounterResult {
    SUCCESS,
    FAIL,
    UNKNOWN;
}
