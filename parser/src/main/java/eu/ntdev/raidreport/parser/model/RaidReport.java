package eu.ntdev.raidreport.parser.model;

import eu.ntdev.raidreport.parser.model.types.RaidBoss;
import eu.ntdev.raidreport.parser.model.types.RaidPlayer;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Data
public class RaidReport {
    private Date buildDate;
    private long targetSpeciedId;
    private int agentCount;
    private List<RaidAgent> agents = new ArrayList<>();
    private List<RaidPlayer> players = new ArrayList<>();
    private List<Skill> skills = new ArrayList<>();
    private List<CombatEvent> combatEvents = new ArrayList<>();

    private ReportParseStatus reportParseStatus;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Map<Long, RaidAgent> agentsById = new HashMap<>();

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private Map<Integer, Skill> skillsById = new HashMap<>();

    public void addAgent(RaidAgent raidAgent) {
        agents.add(raidAgent);
        agentsById.put(raidAgent.getId(), raidAgent);
    }

    public void addSkill(Skill skill) {
        this.skills.add(skill);
        skillsById.put(skill.getId(), skill);
    }

    public void addCombatEvent(CombatEvent combatEvent) {
        this.combatEvents.add(combatEvent);
        combatEvent.setTargetAgent(agentsById.get(combatEvent.getTargetAgentId()));
        combatEvent.setSourceAgent(agentsById.get(combatEvent.getSourceAgentId()));
        combatEvent.setSkill(skillsById.get((int) combatEvent.getSkillId()));
    }

    public Optional<RaidBoss> tryGetRaidBoss(RaidBossData raidBossData) {
        return raidBossData.getBoss(targetSpeciedId);
    }


}
