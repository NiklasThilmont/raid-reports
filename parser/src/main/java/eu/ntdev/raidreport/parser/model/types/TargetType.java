package eu.ntdev.raidreport.parser.model.types;

/**
 * Created by Niklas on 22.07.2017.
 */
public enum TargetType {
    FRIEND,
    FOE,
    UNKNOWN,
    UNPROCESSABLE;

    public static TargetType from(byte val) {
        if(val == 0) return FRIEND;
        if(val == 1) return FOE;
        if(val == 2) return UNKNOWN;
        return UNPROCESSABLE;
    }
}
