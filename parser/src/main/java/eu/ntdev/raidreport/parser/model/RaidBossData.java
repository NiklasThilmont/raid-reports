package eu.ntdev.raidreport.parser.model;

import eu.ntdev.raidreport.parser.model.types.RaidBoss;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RaidBossData {
    private Map<Long, RaidBoss> raidBossesByClassId = new HashMap<>();

    public void addBosses(List<RaidBoss> bossList) {
        bossList.forEach(this::addBoss);
    }

    public void addBoss(RaidBoss raidBoss) {
        raidBoss.getEvtcIds().forEach(aLong -> {
            raidBossesByClassId.put(aLong, raidBoss);
        });
    }

    public Optional<RaidBoss> getBoss(long classId) {
        return Optional.ofNullable(raidBossesByClassId.get(classId));
    }

    public boolean isBoss(long id) {
        return raidBossesByClassId.containsKey(id);
    }
}
