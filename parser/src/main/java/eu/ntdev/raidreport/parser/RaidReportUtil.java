package eu.ntdev.raidreport.parser;

import eu.ntdev.raidreport.parser.model.CombatEvent;
import eu.ntdev.raidreport.parser.model.RaidAgent;
import eu.ntdev.raidreport.parser.model.RaidReport;
import eu.ntdev.raidreport.parser.model.types.CombatResultType;
import eu.ntdev.raidreport.parser.model.types.CombatStateChangeType;
import eu.ntdev.raidreport.parser.model.types.EncounterResult;
import eu.ntdev.raidreport.parser.model.types.RaidBoss;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RaidReportUtil {
    public static List<RaidAgent> filterPlayers(RaidReport raidReport) {
        return raidReport.getAgents().stream().filter(RaidAgent::isPlayer).collect(Collectors.toList());
    }

    public static List<CombatEvent> filterOutgoingDamage(RaidReport raidReport, RaidAgent raidAgent) {
        return raidReport.getCombatEvents().stream().filter(combatEvent -> raidAgent.equals(combatEvent.getSourceAgent())).collect(Collectors.toList());
    }

    private static Optional<Date> getDateValue(RaidReport raidReport, CombatStateChangeType combatStateChangeType) {
        Optional<CombatEvent> eventOptional = raidReport.getCombatEvents().stream()
                .filter(combatEvent -> combatEvent.getCombatStateChangeType().equals(combatStateChangeType))
                .findAny();
        return eventOptional.map(combatEvent -> new Date(combatEvent.getValue() * 1000L));
    }

    public static Optional<Date> getFightStart(RaidReport raidReport) {
        return getDateValue(raidReport, CombatStateChangeType.CBTS_LOGSTART);
    }

    public static Optional<Date> getFightEnd(RaidReport raidReport) {
        return getDateValue(raidReport, CombatStateChangeType.CBTS_LOGEND);
    }

    private static Predicate<CombatEvent> makeSuccessPredicate(long bossId) {
        return (CombatEvent event) ->  (event.getSourceAgent() != null
                && event.getCombatStateChangeType() == CombatStateChangeType.CBTS_CHANGEDEAD
                && event.getSourceAgent().getProf() == bossId);
    }

    private static Predicate<CombatEvent> makePredicate(CombatResultType resultType) {
        return (CombatEvent event) ->  (resultType.equals(event.getResult()));
    }

    public static EncounterResult determineResult(RaidReport raidReport, List<Long> notDeterminableSpeciesIds) {
        Long bossId = raidReport.getTargetSpeciedId();
        if (notDeterminableSpeciesIds.contains(bossId)) {
            return EncounterResult.UNKNOWN;
        }
        boolean isKill = raidReport.getCombatEvents()
                .stream()
                .anyMatch(makeSuccessPredicate(bossId));
        if(isKill) {
            return EncounterResult.SUCCESS;
        } else {
            return EncounterResult.FAIL;
        }
    }

    public static List<CombatEvent> getKillingBlows(RaidReport raidReport) {
        return raidReport.getCombatEvents().stream().filter(makePredicate(CombatResultType.CBTR_KILLINGBLOW)).collect(Collectors.toList());
    }
}
