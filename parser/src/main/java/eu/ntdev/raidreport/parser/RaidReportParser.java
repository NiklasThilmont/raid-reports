package eu.ntdev.raidreport.parser;

import eu.ntdev.raidreport.parser.model.RaidReport;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

/**
 * Created by Niklas on 11.07.2017.
 */
public interface RaidReportParser {
    RaidReport parse(InputStream inputStream) throws IOException, ParseException;
}
