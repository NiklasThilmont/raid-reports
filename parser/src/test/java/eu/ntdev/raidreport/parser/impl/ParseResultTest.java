package eu.ntdev.raidreport.parser.impl;

import eu.ntdev.raidreport.parser.RaidReportParser;
import eu.ntdev.raidreport.parser.model.RaidBossData;
import eu.ntdev.raidreport.parser.model.RaidReport;
import eu.ntdev.raidreport.parser.model.types.AgentType;
import eu.ntdev.raidreport.parser.model.types.RaidBoss;
import eu.ntdev.raidreport.parser.model.types.RaidPlayer;
import lombok.Data;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

/**
 * Validates that test reports get parsed correctly
 */
@RunWith(Parameterized.class)
public class ParseResultTest {

    private static final Logger LOG = Logger.getLogger("ParseResultTest");

    @Data
    private static class Player {
        String accountName;
        AgentType playerClass;

        public Player(String accountName, AgentType playerClass) {
            this.accountName = accountName;
            this.playerClass = playerClass;
        }

        public static Player of(String accountName, AgentType playerClass) {
            return new Player(accountName, playerClass);
        }
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {{ "DEIMOS",
                    "/deimos_unknown_6_07.evtc",
                    Arrays.asList(
                            Player.of("censored.2983", AgentType.SCOURGE),
                            Player.of("censored.8362", AgentType.SCOURGE),
                            Player.of("censored.4567", AgentType.SCOURGE),
                            Player.of("censored.5348", AgentType.BERSERKER),
                            Player.of("censored.3910", AgentType.BERSERKER),
                            Player.of("censored.8691", AgentType.CHRONOMANCER),
                            Player.of("censored.1930", AgentType.DRUID),
                            Player.of("censored.8049", AgentType.CHRONOMANCER),
                            Player.of("censored.6749", AgentType.DRUID),
                            Player.of("censored.5267", AgentType.HERALD)
                            )},
                { "DEIMOS",
                        "/deimos_unknown_7_40.evtc",
                        Arrays.asList(
                                Player.of("censored.8362", AgentType.SCOURGE),
                                Player.of("censored.4567", AgentType.SCOURGE),
                                Player.of("censored.5267", AgentType.BERSERKER),
                                Player.of("censored.3476", AgentType.TEMPEST),
                                Player.of("censored.3915", AgentType.BERSERKER),
                                Player.of("censored.8207", AgentType.CHRONOMANCER),
                                Player.of("censored.1930", AgentType.DRUID),
                                Player.of("censored.8049", AgentType.CHRONOMANCER),
                                Player.of("censored.6749", AgentType.HERALD),
                                Player.of("censored.2983", AgentType.DRUID)
                        )},
                { "GORSEVAL",
                        "/gorse_success_3_16.evtc",
                        Arrays.asList(
                                Player.of("censored.2983", AgentType.SCOURGE),
                                Player.of("censored.8362", AgentType.WEAVER),
                                Player.of("censored.4567", AgentType.RENEGADE),
                                Player.of("censored.5348", AgentType.BERSERKER),
                                Player.of("censored.3910", AgentType.BERSERKER),
                                Player.of("censored.8691", AgentType.CHRONOMANCER),
                                Player.of("censored.1930", AgentType.DRUID),
                                Player.of("censored.8049", AgentType.CHRONOMANCER),
                                Player.of("censored.6749", AgentType.DRUID),
                                Player.of("censored.5267", AgentType.SCOURGE)
                        )}

        });
    }

    private String boss;
    private String reportName;
    private List<Player> expectedPlayers;


    public ParseResultTest(String boss, String reportName, List<Player> expectedPlayers) {
        this.boss = boss;
        this.reportName = reportName;
        this.expectedPlayers = expectedPlayers;
    }

    private RaidBossData raidBossData() {
        RaidBoss deimos = new RaidBoss();
        deimos.setName("DEIMOS");
        deimos.setEvtcIds(Collections.singletonList(0x4302L));

        RaidBoss gorseval = new RaidBoss();
        gorseval.setName("GORSEVAL");
        gorseval.setEvtcIds(Collections.singletonList(0x3C45L));
        RaidBossData raidBossData = new RaidBossData();
        raidBossData.addBoss(deimos);
        raidBossData.addBoss(gorseval);
        return raidBossData;
    }

    private void assertContainsExpectedPlayers(RaidReport report) {
        List<RaidPlayer> players = report.getPlayers();
        for (Player expectedPlayer : expectedPlayers) {
            Boolean contains = players.stream().anyMatch(player -> player.accountNumberMatches(expectedPlayer.accountName)
                    && player.getAgent().getAgentType().equals(expectedPlayer.playerClass));
            if(!contains) {
                fail("Expected " + players.toString() + " to contain the follwing player: " + expectedPlayer);
            }
        }
    }

    @Test
    public void test() throws IOException, ParseException {
        RaidBossData raidBossData = raidBossData();
        InputStream inputStream = ParseResultTest.class.getResourceAsStream(reportName);
        RaidReportParser raidReportParser = new ArcDpsRaidReportParser(ByteOrder.LITTLE_ENDIAN);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        RaidReport raidReport = raidReportParser.parse(inputStream);
        stopWatch.stop();
        System.out.println("Took " + stopWatch.getTime());
        RaidBoss resboss = raidReport.tryGetRaidBoss(raidBossData).orElse(new RaidBoss());
        assertThat(resboss.getName()).isEqualTo(boss);
        assertContainsExpectedPlayers(raidReport);
        System.out.println("Status " + raidReport.getReportParseStatus());
    }
}
