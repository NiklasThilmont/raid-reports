package eu.ntdev.raidreport.parser.impl;

import eu.ntdev.raidreport.parser.RaidReportParser;
import eu.ntdev.raidreport.parser.RaidReportUtil;
import eu.ntdev.raidreport.parser.model.RaidReport;
import eu.ntdev.raidreport.parser.model.types.EncounterResult;
import org.assertj.core.api.AssertionsForInterfaceTypes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(Parameterized.class)
public class SuccessDetectionTest {

    private EncounterResult encounterResult;
    private String resourceName;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"/fails/gorse_fail.evtc", EncounterResult.FAIL},
                {"/fails/vg_fail.evtc", EncounterResult.FAIL},
                {"/fails/kc_fail.evtc", EncounterResult.FAIL},
                {"/fails/matthias_fail.evtc", EncounterResult.FAIL},
                {"/fails/sabetha_fail.evtc", EncounterResult.FAIL},
                {"/fails/sloth_fail.evtc", EncounterResult.FAIL},
                {"/fails/vg_fail.evtc", EncounterResult.FAIL},
                {"/successes/soulless_horror-success.evtc", EncounterResult.SUCCESS},
        });
    }

    public SuccessDetectionTest(String resourceName, EncounterResult encounterResult) {
        this.encounterResult = encounterResult;
        this.resourceName = resourceName;
    }

    @Test
    public void test() throws IOException, ParseException {
        InputStream inputStream = ParseResultTest.class.getResourceAsStream(resourceName);
        RaidReportParser raidReportParser = new ArcDpsRaidReportParser(ByteOrder.LITTLE_ENDIAN);
        RaidReport raidReport = raidReportParser.parse(inputStream);
        AssertionsForInterfaceTypes.assertThat(RaidReportUtil.determineResult(raidReport, new ArrayList<>())).isEqualTo(encounterResult);
    }
}
