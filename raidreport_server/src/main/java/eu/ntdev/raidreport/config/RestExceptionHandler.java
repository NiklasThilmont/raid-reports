package eu.ntdev.raidreport.config;

import eu.ntdev.raidreport.exception.*;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Responsbile for handling custom exceptions that represent some sort of invalid input.
 * Used to generate strong, error code based responses that API consumers may use to create user feedback.
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = Logger.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = {UploadDeniedException.class})
    protected ResponseEntity<Map<String, String>> handleUploadError(UploadDeniedException ex, WebRequest request) {
        LOG.error(ex);
        Map<String, String> response = new HashMap<>();
        response.put("errorCode", ex.getUploadErrorCode().name());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(value = {UserException.class})
    protected  ResponseEntity<Map<String, String>> handleUserError(UserException ex, WebRequest request) {
        LOG.error(ex);
        Map<String, String> response = new HashMap<>();
        response.put("errorCode", ex.getUserError().name());
        return ResponseEntity.badRequest().body(response);
    }

    @ExceptionHandler(value = {RaidTeamException.class, ReportException.class, TeamPermissionException.class})
    protected ResponseEntity<IRestException> handleTeamError(IRestException ex) {
        LOG.error(ex);
        return ResponseEntity.status(ex.getCode()).body(ex);
    }
}
