package eu.ntdev.raidreport.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Configures the static resources, mainly the SPA that is inteded to
 * consume the API and the uploaded reports.
 */
@Configuration
public class StaticResourceConfig extends WebMvcConfigurerAdapter {

    private static final Logger LOG = Logger.getLogger(StaticResourceConfig.class);

    @Value("${app-content-dir}")
    public String appContentDir;
    @Value("${app-path-pattern}")
    public String appPathPattern;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        LOG.info("Mapping " + appPathPattern + "onto " + appContentDir);
        registry.addResourceHandler(appPathPattern).addResourceLocations("file:///" + appContentDir)
                .setCachePeriod(0);
    }
}
