package eu.ntdev.raidreport.clients;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


@FeignClient(url = "https://api.guildwars2.com/v2", name = "accountClient")
public interface AccountsClient {


    @RequestMapping(value = "/account", method = RequestMethod.GET, produces = "application/json")
    GW2Account getAccount(@RequestParam(value = "access_token") String apiKey);

    class GW2Account {
        public String id;
        public String name;
        public String age;
        public String world;
        public List<String> guilds;
        @JsonProperty("guild_leader")
        public String guildLeader;
        public String created;
    }
}
