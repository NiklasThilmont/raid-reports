package eu.ntdev.raidreport.dto;

import lombok.Data;

@Data
public class APIKeyRequestResponse {
    public String key;

    public static APIKeyRequestResponse of(String key) {
        APIKeyRequestResponse getAPIKeyRequestResponse = new APIKeyRequestResponse();
        getAPIKeyRequestResponse.key = key;
        return getAPIKeyRequestResponse;
    }
}
