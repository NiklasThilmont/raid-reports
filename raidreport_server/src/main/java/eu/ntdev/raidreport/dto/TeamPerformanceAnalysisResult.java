package eu.ntdev.raidreport.dto;

import eu.ntdev.raidreport.model.EncounterKillStatus;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.account.ParticipatingAccount;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

/**
 */
@Data
public class TeamPerformanceAnalysisResult {

    private Map<String, List<Long>> killTimesInMs = new HashMap<>();
    private Integer successCount = 0;
    private Integer failCount = 0;
    private Integer totalTries = 0;
    /**
     * The quantity of successful kills of this player. Does not include trys or attempts.
     * The player doesn't have to be a member of the team to be included.
     */
    private Map<String, Integer> participationPerPlayer = new TreeMap<>();

    public void incSuccess() {
        this.successCount++;
        this.totalTries++;
    }

    public void incFailCount() {
        this.failCount++;
        this.totalTries++;
    }

    public void incUnknown() {
        this.totalTries++;
    }

    public void add(ReportFile reportFile) {
        if(reportFile.getEncounterKillStatus() != null) {
            reportFile.getEncounterKillStatus().addToData(this);
            addKillTime(reportFile);
            addParticipatingPlayers(reportFile);
        } else {
            this.totalTries++;
        }
    }

    private void addParticipatingPlayers(ReportFile reportFile) {
        reportFile.getParticipants().forEach(this::addParticipatingPlayer);
    }

    private void addParticipatingPlayer(ParticipatingAccount player) {
        String accountName = player.getPlayerAccount().getAccountName();
        Integer currentParticipation = participationPerPlayer.getOrDefault(accountName, 0);
        currentParticipation++;
        participationPerPlayer.put(accountName, currentParticipation);
    }

    private void addKillTime(ReportFile reportFile) {
        if(reportFile.getEncounterKillStatus() == EncounterKillStatus.SUCCESSFUL) {
            String raidBoss = reportFile.getRaidBoss().getName();
            List<Long> killTimes = killTimesInMs.getOrDefault(raidBoss, new ArrayList<>());
            killTimes.add(reportFile.getKillTimeInMs());
            killTimesInMs.put(raidBoss, killTimes);
        }
    }

    public void sort() {
        participationPerPlayer = participationPerPlayer.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
