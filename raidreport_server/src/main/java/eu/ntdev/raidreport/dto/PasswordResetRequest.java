package eu.ntdev.raidreport.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PasswordResetRequest {
    @NotNull
    @JsonProperty(value = "password", required = true)
    private String password;

    @NotNull
    @JsonProperty(value = "resetId", required = true)
    private String resetId;
}
