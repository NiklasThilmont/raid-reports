package eu.ntdev.raidreport.exception;

import eu.ntdev.raidreport.model.user.UserError;
import lombok.Getter;

public class UserException extends RuntimeException {

    @Getter
    private final UserError userError;

    public UserException(UserError userError) {
        this.userError = userError;
    }
}
