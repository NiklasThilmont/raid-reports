package eu.ntdev.raidreport.exception;

import eu.ntdev.raidreport.model.UploadErrorCode;


public class UploadDeniedException extends RuntimeException {
    private UploadErrorCode uploadErrorCode;

    public UploadDeniedException(UploadErrorCode uploadErrorCode) {
        this.uploadErrorCode = uploadErrorCode;
    }

    public UploadErrorCode getUploadErrorCode() {
        return uploadErrorCode;
    }

    public void setUploadErrorCode(UploadErrorCode uploadErrorCode) {
        this.uploadErrorCode = uploadErrorCode;
    }
}
