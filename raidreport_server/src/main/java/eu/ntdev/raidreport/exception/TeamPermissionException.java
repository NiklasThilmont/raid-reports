package eu.ntdev.raidreport.exception;

import eu.ntdev.raidreport.model.teams.TeamPermission;
import lombok.Getter;
import org.springframework.http.HttpStatus;

public class TeamPermissionException extends RuntimeException implements IRestException {
    @Getter
    private String error;

    @Getter
    private HttpStatus code;

    @Getter
    private Long teamId;

    @Getter
    private TeamPermission missingPermission;

    private TeamPermissionException(String message, String error, HttpStatus code, Long teamId, TeamPermission missingPermission) {
        super(message);
        this.error = error;
        this.code = code;
        this.teamId = teamId;
        this.missingPermission = missingPermission;
    }

    public static TeamPermissionException forbidden(TeamPermission permission, Long teamId) {
        return new TeamPermissionException("Missing permission for team " + teamId + ": " + permission,
                "ERR_MISSING_PERMISSION", HttpStatus.FORBIDDEN, teamId, permission);
    }

    public static TeamPermissionException denied(TeamPermission permission, Long teamId) {
        return new TeamPermissionException("Denied revoking for team " + teamId + ": " + permission,
                "ERR_REVOKE_DENIED_NOONE_LEFT", HttpStatus.BAD_REQUEST, teamId, permission);
    }
}
