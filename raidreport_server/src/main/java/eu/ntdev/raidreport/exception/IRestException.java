package eu.ntdev.raidreport.exception;

import org.springframework.http.HttpStatus;

public interface IRestException {
    HttpStatus getCode();
}
