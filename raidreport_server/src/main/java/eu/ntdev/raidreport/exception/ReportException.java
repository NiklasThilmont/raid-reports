package eu.ntdev.raidreport.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class ReportException extends RuntimeException implements IRestException {
    @Getter
    private Long reportId;

    @Getter
    private String error;

    @Getter
    private String shareId;

    @Getter
    private HttpStatus code;

    private ReportException(String msg) {
        super(msg);
    }

    public static ReportException deleteForbidden(Long reportId) {
        ReportException e = new ReportException("You are not allowed to delete the report file with id " + reportId);
        e.code = HttpStatus.FORBIDDEN;
        e.error = "ERR_DELETE_FORBIDDEN";
        e.reportId = reportId;
        return e;
    }

    public static ReportException sharedNotFound(String shareId) {
        ReportException e = new ReportException("Shared Report for share id " + shareId + " not found");
        e.error = "ERR_SHARED_NOT_FOUND";
        e.code = HttpStatus.NOT_FOUND;
        return e;
    }
}
