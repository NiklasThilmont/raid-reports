package eu.ntdev.raidreport.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class RaidTeamException extends RuntimeException implements IRestException {

    @Getter
    private String error;

    @Getter
    private HttpStatus code;

    @Getter
    private Long teamId;

    private RaidTeamException(String msg, String error, HttpStatus code) {
        super(msg);
        this.error = error;
        this.code = code;
    }


    public static RaidTeamException sharedNotFound(String shareId) {
        return new RaidTeamException("No team for shareId " + shareId + " found.", "ERR_SHARED_NOT_FOUND", HttpStatus.NOT_FOUND);
    }

    public static RaidTeamException notShared(Long teamId) {
        RaidTeamException e = new RaidTeamException("Team is not shared.", "ERR_NOT_SHARED", HttpStatus.BAD_REQUEST);
        e.teamId = teamId;
        return e;
    }

    public static RaidTeamException forbidden() {
        return new RaidTeamException("Access to team denied.", "ERROR_ACCESS_DENIED", HttpStatus.FORBIDDEN);
    }

    public static RaidTeamException notInvited() {
        return new RaidTeamException("Not yet invited to team.", "ERROR_NOT_INVITED", HttpStatus.BAD_REQUEST);
    }

    public static RaidTeamException notYetAccepted() {
        return new RaidTeamException("Invite not yet accepted.", "ERR_NOT_ACCEPTED", HttpStatus.BAD_REQUEST);
    }

    public static RaidTeamException canNotLeave() {
        return new RaidTeamException("Owners can not abandon their team.", "ERR_ABANDON_FORBIDDEN", HttpStatus.BAD_REQUEST);
    }

    public static RaidTeamException notFound(Long teamId) {
        RaidTeamException raidTeamException = new RaidTeamException("No team for id = " + teamId + " found.", "ERROR_NOT_FOUND", HttpStatus.NOT_FOUND);
        raidTeamException.teamId = teamId;
        return raidTeamException;
    }

    public static RaidTeamException reportNotFound(Long teamId) {
        RaidTeamException raidTeamException = new RaidTeamException("Report not found in raid team with id=" + teamId, "ERR_REPORT_NOT_FOUND", HttpStatus.BAD_REQUEST);
        raidTeamException.teamId = teamId;
        return raidTeamException;
    }

    public static RaidTeamException renderTargetNotFound(String renderTargetName) {
        return new RaidTeamException("Render target " + renderTargetName + " not found", "ERR_RENDER_TARGET_NOT_FOUND", HttpStatus.BAD_REQUEST);
    }
}
