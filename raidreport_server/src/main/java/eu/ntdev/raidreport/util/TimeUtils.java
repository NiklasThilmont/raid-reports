package eu.ntdev.raidreport.util;

/**
 * Created by Niklas on 02.07.2017.
 */
public class TimeUtils {
    /**
     * Tries to extract miliseconds from two strings representing seconds and minutes
     * Minutes format: "M minutes"
     * Seconds format: "s.s seconds"
     * @param minutesString
     * @param secondsString
     * @return
     */
    public static Integer extractMiliseconds(String minutesString, String secondsString) {
        Integer res = Integer.parseInt(minutesString) * 60 * 1000;
        if(res >= 0) {
            // Split the seconds
            res += extractMiliseconds(secondsString);
            return res;
        } else {
            res = 0;
        }
        return res;
    }

    public static Integer extractMiliseconds(String secondsString) {
        Integer res = 0;
        try {
            Float flt = Float.parseFloat(secondsString);
            if(flt >= 0) {
                res = Math.round(flt * 1000);
            }
        } catch (NumberFormatException ignored) {

        }
        return res;
    }
}
