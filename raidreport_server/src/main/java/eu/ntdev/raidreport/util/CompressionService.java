package eu.ntdev.raidreport.util;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
public class CompressionService {
    public InputStream getUncompressed(String fileName) {
        try {
            return FileUtils.openInputStream(new File(fileName));
        } catch (IOException e) {
            throw new RuntimeException("Consistency Error: Could not read file from file system!");
        }
    }

    public InputStream getCompressed(String fileName) {
        try {
            ZipFile zipFile = new ZipFile(fileName);
            List<FileHeader> fileHeaderList = (List<FileHeader>) zipFile.getFileHeaders();
            if (fileHeaderList.size() != 1) {
                throw new RuntimeException("Expected exactly one file in zip, but found  " + ((List) fileHeaderList).size());
            }
            return zipFile.getInputStream(fileHeaderList.iterator().next());
        } catch (ZipException e) {
            throw new RuntimeException(e);
        }
    }

}
