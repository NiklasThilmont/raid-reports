package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long > {
    List<User> findAllUsersByUsername(String username);

    Optional<User> findByPasswordResetId(String passwordResetId);
}