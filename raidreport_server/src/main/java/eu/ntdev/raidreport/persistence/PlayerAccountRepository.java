package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.account.PlayerAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlayerAccountRepository extends JpaRepository<PlayerAccount, Long> {
    Optional<PlayerAccount> findByAccountName(String accountName);
}
