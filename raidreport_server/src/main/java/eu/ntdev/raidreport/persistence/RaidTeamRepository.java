package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.teams.RaidTeam;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaidTeamRepository extends CrudRepository<RaidTeam, Long> {

    RaidTeam findOneByShareId(String shareId);
}
