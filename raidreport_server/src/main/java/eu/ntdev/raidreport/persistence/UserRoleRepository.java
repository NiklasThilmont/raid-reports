package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole findOneByRoleName(String roleName);
}
