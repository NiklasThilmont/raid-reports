package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.RaidBoss;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RaidBossRepository extends JpaRepository<RaidBoss, Long> {
    RaidBoss findRaidBossByName(String name);
}
