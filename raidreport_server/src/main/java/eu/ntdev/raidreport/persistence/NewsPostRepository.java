package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.NewsPost;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsPostRepository extends PagingAndSortingRepository<NewsPost, Integer> {

}
