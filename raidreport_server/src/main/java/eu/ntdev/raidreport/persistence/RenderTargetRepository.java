package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RenderTargetRepository extends JpaRepository<RaidTeamRenderTarget, Long> {

    Optional<RaidTeamRenderTarget> findRenderTargetByName(String name);
}
