package eu.ntdev.raidreport.persistence;

import eu.ntdev.raidreport.model.ReportFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportFileRepository extends JpaRepository<ReportFile, Long> {
    public ReportFile findOneByShareIdAndShared(String shareId, boolean shared);
}
