package eu.ntdev.raidreport.controller;

import eu.ntdev.raidreport.dto.APIKeyRequestResponse;
import eu.ntdev.raidreport.dto.PasswordResetRequest;
import eu.ntdev.raidreport.exception.UserException;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.model.user.UserError;
import eu.ntdev.raidreport.model.user.UserPrincipal;
import eu.ntdev.raidreport.model.user.UserRole;
import eu.ntdev.raidreport.persistence.UserRepository;
import eu.ntdev.raidreport.persistence.UserRoleRepository;
import eu.ntdev.raidreport.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/users")
public class UserController {


    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final UserService userService;

    private final UserRoleRepository userRoleRepository;

    private final Integer minUsernameLength = 4;
    private final Integer maxUsernameLength = 30;
    private final Integer minPasswordLength = 4;
    private final Integer maxPasswordLength = 512;

    @Value("${admin-username}")
    private String adminUsername;

    private static final Logger LOG = Logger.getLogger(UserController.class);

    @Autowired
    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder, UserService userService, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.userRoleRepository = userRoleRepository;
    }

    private User retrieveUser() {
        User user = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        return userRepository.findOne(user.getId());
    }

    @RequestMapping(value = "/", method = RequestMethod.HEAD)
    public ResponseEntity isAuthenticated() {
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/me/authorities")
    public ResponseEntity<List<String>> getAuthorities() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        return ResponseEntity.ok(authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()));
    }

    @GetMapping("/me/key")
    public APIKeyRequestResponse getAPIKey() {
        User user = retrieveUser();
        if(user == null) {
            throw new UserException(UserError.ERR_NOT_FOUND);
        }
        return APIKeyRequestResponse.of(user.getApiKey());
    }


    @PostMapping(value = "/me/key")
    public ResponseEntity associateWithApiKey(@RequestBody APIKeyRequestResponse request) {
        User user = retrieveUser();
        userService.associateWithGW2Account(user, request.key);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/me/key")
    public ResponseEntity removeAPIKey() {
        User user = retrieveUser();
        userService.removeGW2Account(user);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping(value = "/me/password")
    public ResponseEntity changePassword(@RequestBody PasswordResetRequest request) {
        User user = userRepository.findByPasswordResetId(request.getResetId()).orElseThrow(() -> new UserException(UserError.ERR_NOT_FOUND));
        userService.resetPassword(user, request.getPassword(), request.getResetId());
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Map<String, String>> addUser(@RequestBody User user) {
        if(user.getUsername() != null && userService.userExists(user.getUsername().trim() )) {
            throw new UserException(UserError.ERR_DUPLICATE_USER);
        }
        if(user.getUsername() == null || user.getUsername().trim().length() < minUsernameLength) {
            throw new UserException(UserError.ERR_USERNAME_TOO_SHORT);
        }
        if(user.getUsername().trim().length() > maxUsernameLength) {
            throw new UserException(UserError.ERR_USERNAME_TOO_LONG);
        }
        if(user.getPassword() == null || user.getPassword().trim().length() < minPasswordLength) {
            throw new UserException(UserError.ERR_PASSWORD_TOO_SHORT);
        }
        if(user.getPassword().trim().length() > maxPasswordLength) {
            throw new UserException(UserError.ERR_PASSWORD_TOO_LONG);
        }
        User newUser = new User();
        newUser.setUsername(user.getUsername().trim());
        newUser.setPassword(passwordEncoder.encode(user.getPassword().trim()));
        newUser.setEnabled(false);

        UserRole role = userRoleRepository.findOneByRoleName(UserRole.USER_ROLE_USER);
        newUser.setRoles(Collections.singletonList(role));

        userRepository.save(newUser);
        Map<String, String> result = new HashMap<>();
        result.put("username", newUser.getUsername());
        return ResponseEntity.ok(result);
    }

    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> result = new ArrayList<>();
        result.addAll(userRepository.findAll());
        return ResponseEntity.ok(result);
    }

    @PatchMapping(value = "/{id}/active")
    @Transactional
    public ResponseEntity<User> activateUser(@PathVariable("id") Long userId) {
        User user = userRepository.findOne(userId);
        if(user == null) {
            return ResponseEntity.notFound().build();
        }
        user.setEnabled(true);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteUser(@PathVariable("id") Long userId) {
        User user = userRepository.findOne(userId);
        if(adminUsername.equals(user.getUsername())) {
            throw new RuntimeException("Admin User can not be deleted");
        }
        userRepository.delete(user);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/name")
    public ResponseEntity<List<User>> findAllUsers(@RequestParam("username") String username) {
        return ResponseEntity.ok(userRepository.findAllUsersByUsername(username));
    }
}
