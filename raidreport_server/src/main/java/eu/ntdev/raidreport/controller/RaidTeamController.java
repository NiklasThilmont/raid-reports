package eu.ntdev.raidreport.controller;

import eu.ntdev.raidreport.dto.TeamPerformanceAnalysisResult;
import eu.ntdev.raidreport.exception.RaidTeamException;
import eu.ntdev.raidreport.exception.UploadDeniedException;
import eu.ntdev.raidreport.exception.UserException;
import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.UploadErrorCode;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.teams.RaidTeamUser;
import eu.ntdev.raidreport.model.teams.TeamPermission;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.model.user.UserError;
import eu.ntdev.raidreport.model.user.UserPrincipal;
import eu.ntdev.raidreport.persistence.RaidTeamRepository;
import eu.ntdev.raidreport.persistence.UserRepository;
import eu.ntdev.raidreport.service.RaidTeamReportService;
import eu.ntdev.raidreport.service.RaidTeamService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("/team")
public class RaidTeamController {

    private final RaidTeamService raidTeamService;

    private final RaidTeamReportService raidTeamReportService;

    private final UserRepository userRepository;

    private final RaidTeamRepository raidTeamRepository;

    private final CounterService counterService;



    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    public RaidTeamController(RaidTeamService raidTeamService, RaidTeamReportService raidTeamReportService,
                              UserRepository userRepository, RaidTeamRepository raidTeamRepository,
                              CounterService counterService) {
        this.raidTeamService = raidTeamService;
        this.raidTeamReportService = raidTeamReportService;
        this.userRepository = userRepository;
        this.raidTeamRepository = raidTeamRepository;
        this.counterService = counterService;
    }

    private User retrieveUser() {
        User user = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        return userRepository.findOne(user.getId());
    }

    private RaidTeam retrieveRaidTeam(Long id) {
        RaidTeam raidTeam = raidTeamRepository.findOne(id);
        if(raidTeam != null) {
            return raidTeam;
        } else {
            throw RaidTeamException.notFound(id);
        }
    }

    @PostMapping(value = "")
    public ResponseEntity<RaidTeam> createTeam(@RequestBody RaidTeam partialRaidTeam) {
        User user = ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
        user = userRepository.findOne(user.getId());
        RaidTeam raidTeam = raidTeamService.createNewTeam(user, partialRaidTeam.getTeamName());
        return ResponseEntity.ok(raidTeam);
    }

    @GetMapping(value = "/myteams")
    @Transactional
    public ResponseEntity<List<RaidTeam>> findRaidUserTeams() {
        User user = retrieveUser();
        List<RaidTeam> raidTeams = user.getRaidTeams().stream().map(RaidTeamUser::getRaidTeam).collect(Collectors.toList());
        return ResponseEntity.ok(raidTeams);
    }

    @PatchMapping("/{team_id}/users/{user_id}")
    public ResponseEntity<RaidTeam> inviteUserToRaidTeam(@PathVariable("team_id") Long teamId, @PathVariable("user_id") Long userId) {
        User user = retrieveUser();
        User joining = userRepository.findOne(userId);
        RaidTeam raidTeam = raidTeamRepository.findOne(teamId);
        raidTeam = raidTeamService.inviteUser(user, raidTeam, joining);
        return ResponseEntity.ok(raidTeam);
    }

    @PostMapping(value = "/{team_id}/report")
    @Transactional
    public ResponseEntity<Map<String, String>> uploadReportToRaidTeam(
            @PathVariable(value = "team_id") Long teamId,
            @RequestParam(value = "file") MultipartFile fileToUpload,
            @RequestParam(value = "comment", required = false) String comment) {
        // Necessary validations
        if(fileToUpload.isEmpty()) {
            throw new UploadDeniedException(UploadErrorCode.FILE_EMPTY);
        }
        if(comment != null && comment.length() > 200) {
            throw new UploadDeniedException(UploadErrorCode.ERR_COMMENT_TOO_LONG);
        }
        RaidTeam raidTeam = raidTeamRepository.findOne(teamId);
        User user = retrieveUser();
        if(raidTeam == null) {
            throw RaidTeamException.notFound(teamId);
        }
        // Make the report file
        try {
            raidTeamReportService.uploadToTeam(raidTeam, fileToUpload.getInputStream(), comment, user);
        } catch (IOException e) {
            throw new UploadDeniedException(UploadErrorCode.BAD_REPORT);
        }
        // Save the actual file
        //raidTeamReportService.saveUploadedFileToDisk(fileToUpload, reportFile.getFileName());
        Map<String, String> response = new HashMap<>();
        response.put("location", fileToUpload.getOriginalFilename());
        counterService.increment("controller.reportFileController.uploadFile.invoked");
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/{team_id}/report", produces = "application/json")
    @Transactional
    public ResponseEntity<Set<ReportFile>> getTeamsReportFiles(@PathVariable("team_id") Long teamId) {
        counterService.increment("controller.raidTeamController.getTeamsReportFiles.invoked");
        User user = retrieveUser();
        RaidTeam raidTeam = raidTeamRepository.findOne(teamId);
        return ResponseEntity.ok(raidTeamService.retrieveAllReports(raidTeam, user));
    }


    @DeleteMapping(value = "/{team_id}/report/{report_id}")
    public ResponseEntity deleteReportFile(@PathVariable("team_id") Long teamId, @PathVariable("report_id") Long reportId) {
        counterService.increment("controller.reportFileController.deleteReportFile.invoked");
        raidTeamReportService.deleteReportFromTeam(teamId, reportId, retrieveUser());
        return ResponseEntity.noContent().build();
    }

    @PatchMapping(value = "/{team_id}/invite")
    public ResponseEntity acceptInvited(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.acceptInvite(user, raidTeam);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{team_id}/invite")
    public ResponseEntity rejectInvite(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.rejectInvite(user, raidTeam);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/{team_id}/me")
    public ResponseEntity leaveTeam(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.leaveTeam(user, raidTeam);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/{team_id}/report/{report_id}/*.html")
    public void getReportAsFile(@PathVariable("team_id") Long teamId, @PathVariable("report_id") Long reportId, HttpServletResponse response) {
        User user = retrieveUser();
        InputStream inputStream = raidTeamReportService.retrieveFileFromFileSystem(teamId, reportId, user);
        try {
            response.setHeader("Content-Type", MediaType.TEXT_HTML_VALUE);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @PatchMapping(value = "/{team_id}/shared")
    public ResponseEntity<RaidTeam> shareTeam(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.shareTeam(user, raidTeam);
        return ResponseEntity.ok(raidTeam);
    }

    @DeleteMapping(value = "/{team_id}/shared")
    public ResponseEntity<RaidTeam> unShareTeam(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.unShareTeam(user, raidTeam);
        return ResponseEntity.ok(raidTeam);
    }

    @PostMapping(value = "/{team_id}/report/{report_id}/shared")
    public ResponseEntity<RaidTeam> shareReport(@PathVariable("team_id") Long teamId,
                                                @PathVariable("report_id") Long reportId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.shareReport(user, raidTeam, reportId);
        return ResponseEntity.ok(raidTeam);
    }

    @DeleteMapping(value = "/{team_id}/report/{report_id}/shared")
    public ResponseEntity<RaidTeam> unShareReport(@PathVariable("team_id") Long teamId,
                                                @PathVariable("report_id") Long reportId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.unShareReport(user, raidTeam, reportId);
        return ResponseEntity.ok(raidTeam);
    }

    @PostMapping(value = "/{team_id}/{user_id}/permission/{permission}")
    public ResponseEntity<RaidTeam> grantPermission(@PathVariable("team_id") Long teamId,
                                                    @PathVariable("user_id") Long userId,
                                                    @PathVariable("permission")TeamPermission permission) {
        User user = retrieveUser();
        User userGettingGrant = userRepository.findOne(userId);
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.grantPermission(user, userGettingGrant, raidTeam, permission);
        return ResponseEntity.ok(raidTeam);
    }

    @DeleteMapping(value = "/{team_id}/{user_id}/permission/{permission}")
    public ResponseEntity<RaidTeam> revokePermission(@PathVariable("team_id") Long teamId,
                                                     @PathVariable("user_id") Long userId,
                                                     @PathVariable("permission")TeamPermission permission) {
        User user = retrieveUser();
        User userGettingGrant = userRepository.findOne(userId);
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        raidTeamService.revokePermission(user, userGettingGrant, raidTeam, permission);
        return ResponseEntity.ok(raidTeam);
    }

    @GetMapping(value = "/{team_id}/{user_id}/participation")
    public Map<String, Long> calculateParticipationForTeam(@PathVariable("team_id") Long teamId,
                                                      @PathVariable("user_id") Long userId) {
        User user = userRepository.findOne(userId);
        if(user == null) {
            throw new UserException(UserError.ERR_NOT_FOUND);
        }
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        return raidTeamService.calculateParticipationForTeam(user, raidTeam);
    }

    @PostMapping(value = "/{team_id}/analysis")
    public TeamPerformanceAnalysisResult analyseTeam(@PathVariable("team_id") Long teamId) {
        User user = retrieveUser();
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        return raidTeamService.analyzeTeamPerformance(user, raidTeam);
    }

    @PatchMapping(value = "/{team_id}/rendering/target")
    public RaidTeam changeRenderTarget(@PathVariable("team_id") Long teamId, @RequestBody RaidTeamRenderTarget raidTeamRenderTarget) {
        RaidTeam raidTeam = retrieveRaidTeam(teamId);
        User user = retrieveUser();
        raidTeamService.changeRenderTarget(user, raidTeam, raidTeamRenderTarget.getName());
        return raidTeam;
    }
}
