package eu.ntdev.raidreport.controller;

import eu.ntdev.raidreport.model.RaidBoss;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.RaidBossRepository;
import eu.ntdev.raidreport.persistence.ReportFileRepository;
import eu.ntdev.raidreport.service.ReportConvertService;
import eu.ntdev.raidreport.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

@RestController()
@RequestMapping("/admin")
public class AdminEndpoint {

    private final ReportFileRepository reportFileRepository;

    private final ReportConvertService reportConvertService;

    private final UserService userService;

    private final RaidBossRepository raidBossRepository;

    @Autowired
    public AdminEndpoint(ReportFileRepository reportFileRepository, ReportConvertService reportConvertService, UserService userService, RaidBossRepository raidBossRepository) {
        this.reportFileRepository = reportFileRepository;
        this.reportConvertService = reportConvertService;
        this.userService = userService;
        this.raidBossRepository = raidBossRepository;
    }

    @PatchMapping("/report/{report_id}")
    public ResponseEntity<ReportFile> reReadReportFile(@PathVariable("report_id") Long reportId) {
        ReportFile reportFile = reportFileRepository.findOne(reportId);
        if(reportFile != null) {
            try {
                reportConvertService.reReadMetaData(reportFile);
                return ResponseEntity.ok(reportFile);
            } catch (IOException | ParseException e) {
                return ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/management/user/{user_id}/password")
    public String startPasswordReset(@PathVariable("user_id") Long userId) {
        User user = userService.startPasswordReset(userId);
        return user.getPasswordResetId();
    }

    @GetMapping("/management/raid/bosses")
    public List<RaidBoss> getAllRaidBosses() {
        return raidBossRepository.findAll();
    }

    @Transactional
    @PatchMapping("/management/raid/bosses")
    public ResponseEntity<RaidBoss> updateRaidBoss(@RequestBody RaidBoss raidBoss) {
        RaidBoss boss;
        if(raidBoss.getId() == null) {
            boss = new RaidBoss();
        } else {
            boss = raidBossRepository.findOne(raidBoss.getId());
            if(boss == null) {
                return ResponseEntity.notFound().build();
            }
        }

        boss.setClassIds(raidBoss.getClassIds());
        boss.setName(raidBoss.getName());
        return ResponseEntity.ok(raidBossRepository.save(boss));
    }

    @DeleteMapping("/management/raid/bosses/{raid_boss_id}")
    public void deleteRaidBoss(@PathVariable("raid_boss_id") Long raidBossId) {
        raidBossRepository.delete(raidBossId);
    }
}
