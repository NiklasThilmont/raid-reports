package eu.ntdev.raidreport.controller;

import eu.ntdev.raidreport.model.NewsPost;
import eu.ntdev.raidreport.persistence.NewsPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/news")
public class NewsPostController {


    private final NewsPostRepository newsPostRepository;

    @Autowired
    public NewsPostController(NewsPostRepository newsPostRepository) {
        this.newsPostRepository = newsPostRepository;
    }

    @GetMapping("")
    public Page<NewsPost> getPaginated(Pageable pageable) {
        return newsPostRepository.findAll(pageable);
    }
}
