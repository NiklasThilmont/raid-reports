package eu.ntdev.raidreport.controller;

import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.persistence.RenderTargetRepository;
import eu.ntdev.raidreport.service.RaidTeamReportService;
import eu.ntdev.raidreport.service.RaidTeamService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/share")
public class SharedContentController {

    private final RaidTeamService raidTeamService;

    private final RaidTeamReportService raidTeamReportService;

    private final RenderTargetRepository renderTargetRepository;

    @Autowired
    public SharedContentController(RaidTeamService raidTeamService, RaidTeamReportService raidTeamReportService, RenderTargetRepository renderTargetRepository) {
        this.raidTeamService = raidTeamService;
        this.raidTeamReportService = raidTeamReportService;
        this.renderTargetRepository = renderTargetRepository;
    }

    @GetMapping("/team/{share_id}")
    public ResponseEntity<Map<String, Object>> getSharedTeam(@PathVariable("share_id") String shareId) {
        RaidTeam raidTeam = raidTeamService.findSharedTeam(shareId);
        if(raidTeam == null) {
            return ResponseEntity.notFound().build();
        } else {
            Map<String, Object> res = new HashMap<>();
            res.put("id", raidTeam.getId());
            res.put("teamName", raidTeam.getTeamName());
            res.put("reports", raidTeam.getReports());
            return ResponseEntity.ok(res);
        }
    }

    @GetMapping(value = "/report/{share_id}")
    public void getSharedReport(@PathVariable("share_id") String shareId, HttpServletResponse response) {
        InputStream inputStream = raidTeamReportService.retrieveSharedReport(shareId);
        writeReport(response, inputStream);
    }

    @RequestMapping(value = "/team/{share_id}/report/{report_id}/*.html", method = RequestMethod.GET)
    public void getReportAsFile(@PathVariable("share_id") String shareId, @PathVariable("report_id") Long reportId, HttpServletResponse response) {
        InputStream inputStream = raidTeamReportService.retrieveFileFromPublicTeam(shareId, reportId);
        writeReport(response, inputStream);
    }

    @RequestMapping(value = "/rendering/targets")
    public List<RaidTeamRenderTarget> getAllAvailableRenderTarget() {
        return renderTargetRepository.findAll();
    }

    private void writeReport(HttpServletResponse response, InputStream inputStream) {
        try {
            response.setHeader("Content-Type", MediaType.TEXT_HTML_VALUE);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
