package eu.ntdev.raidreport;

import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.model.user.UserRole;
import eu.ntdev.raidreport.parser.RaidReportParser;
import eu.ntdev.raidreport.parser.impl.ArcDpsRaidReportParser;
import eu.ntdev.raidreport.persistence.UserRepository;
import eu.ntdev.raidreport.service.RaidAppUserDetailService;
import eu.ntdev.raidreport.service.UserService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.nio.ByteOrder;
import java.util.Arrays;

@SpringBootApplication
@EnableAsync // Async is fun. Especially when the database implementation handles concurrency
@EnableScheduling // Scheduling for cleanup and regular tasks
@EnableFeignClients
public class RaidreportsApplication implements InitializingBean {



    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @Value("${admin-username}")
    private String adminUsername;
    @Value("${admin-password}")
    private String adminPassword;


    @Bean
    public DaoAuthenticationProvider authProvider(RaidAppUserDetailService raidAppUserDetailService) {
        DaoAuthenticationProvider bean = new DaoAuthenticationProvider();
        bean.setUserDetailsService(raidAppUserDetailService);
        bean.setPasswordEncoder(passwordEncoder());
        return bean;
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        return new CommonsRequestLoggingFilter();
    }

    @Bean
    public RaidReportParser parser() {
        return new ArcDpsRaidReportParser(ByteOrder.LITTLE_ENDIAN);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Gurantees that an admin user exists.
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        // check if admin exists
        if(!userService.userExists(adminUsername)) {
            User user = new User();
            user.setUsername(adminUsername);
            user.setPassword(passwordEncoder().encode(adminPassword));
            user.setEnabled(true);
            user.setRoles(Arrays.asList(new UserRole(UserRole.USER_ROLE_ADMIN), new UserRole(UserRole.USER_ROLE_USER), new UserRole(UserRole.USER_ROLE_ACTUATOR)));
            userRepository.save(user);
        }
    }

	public static void main(String[] args) {
		SpringApplication.run(RaidreportsApplication.class, args);
	}

}
