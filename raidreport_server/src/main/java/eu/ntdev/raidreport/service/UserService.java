package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.clients.AccountsClient;
import eu.ntdev.raidreport.exception.UserException;
import eu.ntdev.raidreport.model.account.PlayerAccount;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.PlayerAccountRepository;
import eu.ntdev.raidreport.persistence.UserRepository;
import org.springframework.data.util.StreamUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static eu.ntdev.raidreport.model.user.UserError.ERR_NOT_FOUND;
import static eu.ntdev.raidreport.model.user.UserError.ERR_PASSWORD_RESET_NOT_POSSIBLE;

/**
 * Utility around {@link User}
 */
@Service
public class UserService {

    private final UserRepository userRepository;

    private final AccountsClient accountsClient;

    private final PlayerAccountRepository playerAccountRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, AccountsClient accountsClient, PlayerAccountRepository playerAccountRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.accountsClient = accountsClient;
        this.playerAccountRepository = playerAccountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Boolean userExists(String username) {
        return StreamUtils
                .createStreamFromIterator(userRepository
                        .findAll()
                        .iterator())
                .anyMatch(user -> user.getUsername().equals(username));
    }

    @Transactional
    public User startPasswordReset(Long userId) {
        User user = userRepository.findOne(userId);
        if(user == null) {
            throw new UserException(ERR_NOT_FOUND);
        }
        UUID uuid = UUID.randomUUID();
        user.setPasswordResetId(uuid.toString());
        user.setEnabled(false);
        return user;
    }

    @Transactional
    public void resetPassword(User user, String newPassword, String resetId) {
        if(user.resetIdMatches(resetId)) {
            user.setPassword(passwordEncoder.encode(newPassword));
            user.setPasswordResetId(null);
            user.setEnabled(true);
        } else {
            throw new UserException(ERR_PASSWORD_RESET_NOT_POSSIBLE);
        }
    }

    @Transactional
    public void removeGW2Account(User user) {
        user.setPlayerAccount(null);
        user.setApiKey(null);
    }

    @Transactional
    public void associateWithGW2Account(User user, String apiKey) {
        AccountsClient.GW2Account account = accountsClient.getAccount(apiKey);
        Optional<PlayerAccount> persistentAccount = playerAccountRepository.findByAccountName(account.name);
        PlayerAccount existingAccount = null;
        if(persistentAccount.isPresent()) {
            existingAccount = persistentAccount.get();
        } else {
            existingAccount = new PlayerAccount();
            existingAccount.setAccountName(account.name);
            existingAccount = playerAccountRepository.save(existingAccount);
        }
        user.setPlayerAccount(existingAccount);
        user.setApiKey(apiKey);
    }
}
