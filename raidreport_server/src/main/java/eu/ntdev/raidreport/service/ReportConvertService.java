package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.model.EncounterKillStatus;
import eu.ntdev.raidreport.model.RaidBoss;
import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.account.ParticipatingAccount;
import eu.ntdev.raidreport.model.account.PlayerAccount;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.parser.RaidReportParser;
import eu.ntdev.raidreport.parser.RaidReportUtil;
import eu.ntdev.raidreport.parser.model.RaidBossData;
import eu.ntdev.raidreport.parser.model.RaidReport;
import eu.ntdev.raidreport.parser.model.types.EncounterResult;
import eu.ntdev.raidreport.parser.model.types.RaidPlayer;
import eu.ntdev.raidreport.persistence.PlayerAccountRepository;
import eu.ntdev.raidreport.persistence.RaidBossRepository;
import eu.ntdev.raidreport.persistence.ReportFileRepository;
import eu.ntdev.raidreport.util.CompressionService;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import java.io.*;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;


@Service
public class ReportConvertService {

    private static final Logger LOG = Logger.getLogger(ReportConvertService.class);

    private final RaidReportParser raidReportParser;

    private final ReportFileRepository reportFileRepository;

    private final PlayerAccountRepository playerAccountRepository;

    private final RaidBossRepository raidBossRepository;

    private final CompressionService compressionService;

    /**
     * Directory under which reports are saved. Absolute path, ending with a trailing slash.
     * Set via application properties
     */
    @Value("${report-content-dir}")
    private String reportLocationDir;

    @Autowired
    public ReportConvertService(RaidReportParser raidReportParser, ReportFileRepository reportFileRepository, PlayerAccountRepository playerAccountRepository, RaidBossRepository raidBossRepository, CompressionService compressionService) {
        this.raidReportParser = raidReportParser;
        this.reportFileRepository = reportFileRepository;
        this.playerAccountRepository = playerAccountRepository;
        this.raidBossRepository = raidBossRepository;
        this.compressionService = compressionService;
    }

    /**
     */
    public ReportFile convertLogToReport(InputStream logInputStream, User user, String comment, RaidTeam raidTeam) throws IOException, ParseException, InterruptedException {
        LOG.info(format("Starting report conversion for team id %d with renderer %s", raidTeam.getId(), raidTeam.getRenderTarget()));
        final StopWatch stopWatch = new StopWatch("Report Log Converter");
        stopWatch.start("saveToEvtc");
        final File tmpEvtcFile = saveToEvtc(logInputStream, raidTeam.getRenderTarget());
        stopWatch.stop();
        stopWatch.start("createHTMLFile");
        final File tmpHtmlFile = createHTMLFile(tmpEvtcFile, raidTeam.getRenderTarget());
        stopWatch.stop();
        stopWatch.start("moveToDestination");
        final Pair<File, File> htmlAndEvtcFile = moveToDestination(tmpEvtcFile, tmpHtmlFile, user, raidTeam);
        stopWatch.stop();
        stopWatch.start("writeMetaData");
        ReportFile reportFile = new ReportFile();
        writeMetaData(reportFile, htmlAndEvtcFile.getFirst());
        reportFile.setUploadDate(LocalDateTime.now());
        reportFile.setFileName(htmlAndEvtcFile.getSecond().getAbsolutePath());
        reportFile.setEvtcFileName(htmlAndEvtcFile.getFirst().getAbsolutePath());
        reportFile.setComment(comment);
        reportFile.setUser(user);
        reportFile.setRenderTarget(raidTeam.getRenderTarget());
        stopWatch.stop();
        stopWatch.start("compression");
        compress(reportFile);
        stopWatch.stop();
        LOG.info("Report converting done. Took: " + stopWatch.getTotalTimeSeconds() + " seconds. Files: " + reportFile.getFileName() + "/" + reportFile.getEvtcFileName());
        LOG.info("Breakdown: " + stopWatch.prettyPrint());
        return reportFileRepository.save(reportFile);
    }

    private void compress(ReportFile reportFile) {
        String uncompressedEVTC = reportFile.getEvtcFileName();
        String uncompressedHTML = reportFile.getFileName();
        String compressedEVTC = compressFile(uncompressedEVTC);
        String compressedHMTL = compressFile(uncompressedHTML);
        reportFile.setFileName(compressedHMTL);
        reportFile.setEvtcFileName(compressedEVTC);
        reportFile.setCompressed(true);
        if (!new File(uncompressedEVTC).delete()) {
            throw new RuntimeException("Could not delete " + uncompressedEVTC);
        }
        if (!new File(uncompressedHTML).delete()) {
            throw new RuntimeException("Could not delete " + uncompressedHTML);
        }
    }

    private String compressFile(String pathToCompress) {
        try {
            String destinationFilePath = pathToCompress + ".zip";
            ZipParameters parameters = new ZipParameters();
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
            ZipFile zipFile = new ZipFile(destinationFilePath);
            File targetFile = new File(pathToCompress);
            if(targetFile.isFile()){
                zipFile.addFile(targetFile, parameters);
            }else if(targetFile.isDirectory()){
                zipFile.addFolder(targetFile, parameters);
            }
            return destinationFilePath;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public void reReadMetaData(ReportFile reportFile) throws IOException, ParseException {
        if(reportFile.getEvtcFileName() != null) {
            if (reportFile.isCompressed()) {
                writeMetaData(reportFile, compressionService.getCompressed(reportFile.getEvtcFileName()));
            } else {
                writeMetaData(reportFile, new File(reportFile.getEvtcFileName()));
            }
        }
    }

    private File saveFile(InputStream inputStream, String path) throws IOException {
        File file = new File(path);
        FileUtils.copyInputStreamToFile(inputStream, file);
        return file;
    }

    private File saveToEvtc(InputStream inputStream, RaidTeamRenderTarget renderTarget) throws IOException {
        String fileName = renderTarget.getLocation() + "raidreport_" + UUID.randomUUID() + ".evtc";
        return saveFile(inputStream, fileName);
    }

    /**
     * Trys to get a {@link PlayerAccount} from the database for the provided account name.
     * If no account name is present in the database, a new persisted player account is returned.
     * @param player
     * @return
     */
    private ParticipatingAccount mergeIntoContext(RaidPlayer player) {
        Optional<PlayerAccount> accoutOptional = playerAccountRepository.findByAccountName(player.getAccountName());
        PlayerAccount account  = accoutOptional.orElseGet(() -> playerAccountRepository.save(new PlayerAccount(player.getAccountName())));
        ParticipatingAccount participatingAccount = new ParticipatingAccount();
        participatingAccount.setCharName(player.getCharacterName());
        participatingAccount.setClassName(player.getAgent().getAgentType());
        participatingAccount.setPlayerAccount(account);
        return participatingAccount;
    }

    private File findHTMLReport(File evtcFile, File directory) {
        String regex = evtcFile.getName().substring(0, evtcFile.getName().lastIndexOf('.')) + "*.html";
        FileFilter fileFilter = new WildcardFileFilter(regex);
        File[] files = directory.listFiles(fileFilter);
        if(files.length != 1) {
            throw new RuntimeException("Found no or too many HTML files:" + files.length);
        } else {
            return files[0];
        }

    }

    private File createHTMLFile(File evtFile, RaidTeamRenderTarget renderTarget) throws IOException, InterruptedException {
        // This invokes the generation of a report file in the directory of the evtc file.
        invokeReportGeneration(evtFile, renderTarget);
        // This HTML file has to be found by regex to be returned in the end
        return findHTMLReport(evtFile, evtFile.getParentFile());
    }

    /**
     *
     * @param evtcFile
     * @param htmlFile
     * @param user
     * @param raidTeam
     * @return first is evtc file, second is htmlFile
     */
    private Pair<File, File> moveToDestination(final File evtcFile, final File htmlFile, User user, RaidTeam raidTeam) throws IOException {
        final String path = reportLocationDir + "team_" + raidTeam.getId() + "/";
        final String filename = "report_" + user.getUsername() + "_" + new Date().getTime();
        final File evtcRes = new File(path + filename + ".evtc");
        final File htmlRes = new File(path + filename + ".html");
        FileUtils.moveFile(evtcFile, evtcRes);
        FileUtils.moveFile(htmlFile, htmlRes);
        return Pair.of(evtcRes, htmlRes);
    }

    private EncounterKillStatus map(EncounterResult result) {
        switch (result) {
            case SUCCESS:
                return EncounterKillStatus.SUCCESSFUL;
            case FAIL:
                return EncounterKillStatus.FAILURE;
            case UNKNOWN:
                return EncounterKillStatus.UNKNOWN;
        }
        throw new IllegalArgumentException("TODO");
    }


    private ReportFile writeMetaData(ReportFile reportFile, File file) throws IOException, ParseException {
        try (FileInputStream inputStream = FileUtils.openInputStream(file)) {
           return writeMetaData(reportFile, inputStream);
        }
    }

    private ReportFile writeMetaData(ReportFile reportFile, InputStream inputStream) throws IOException, ParseException {
        RaidReport raidReport = raidReportParser.parse(inputStream);
        Date combatStart = RaidReportUtil.getFightStart(raidReport).orElse(null);
        Date combatEnd = RaidReportUtil.getFightEnd(raidReport).orElse(null);
        if (combatStart != null && combatEnd != null) {
            Long killTimeInMs = combatEnd.getTime() - combatStart.getTime();
            reportFile.setKillTimeInMs(killTimeInMs);
        }
        if (combatEnd != null) {
            LocalDateTime date = combatEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            reportFile.setKillDate(date);
        }
        raidReport.tryGetRaidBoss(assembleRaidBossData())
                .map(this::findRaidBoss)
                .ifPresent(reportFile::setRaidBoss);

        List<ParticipatingAccount> players = raidReport.getPlayers().stream().map(this::mergeIntoContext).collect(Collectors.toList());
        reportFile.setParticipants(players);
        EncounterResult encounterResult = RaidReportUtil.determineResult(raidReport, new ArrayList<>());
        reportFile.setEncounterKillStatus(map(encounterResult));
        return reportFile;
    }



    private void invokeReportGeneration(File file, RaidTeamRenderTarget renderTarget) throws IOException, InterruptedException {
        List<String> commands = Arrays.asList(renderTarget.getCommand(), file.getAbsolutePath());
        ProcessBuilder processBuilder = new ProcessBuilder(commands).directory(new File(renderTarget.getLocation()));
        Process p = processBuilder.start();
        p.waitFor();
    }

    private RaidBossData assembleRaidBossData() {
        RaidBossData data = new RaidBossData();
        raidBossRepository.findAll().stream()
                .map(RaidBoss::map)
                .forEach(data::addBoss);
        return data;
    }

    private RaidBoss findRaidBoss(eu.ntdev.raidreport.parser.model.types.RaidBoss boss) {
        return raidBossRepository.findRaidBossByName(boss.getName());
    }

}
