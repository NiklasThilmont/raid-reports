package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.model.user.UserPrincipal;
import eu.ntdev.raidreport.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.StreamUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RaidAppUserDetailService implements UserDetailsService {

    private final UserRepository userRepository;


    @Autowired
    public RaidAppUserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = StreamUtils.createStreamFromIterator(userRepository.findAll().iterator())
                .filter(user1 -> user1.getUsername().equals(username))
                .findAny();
        if(!userOptional.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipal(userOptional.get());
    }
}
