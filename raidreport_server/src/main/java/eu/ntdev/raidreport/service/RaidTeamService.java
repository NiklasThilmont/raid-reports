package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.dto.TeamPerformanceAnalysisResult;
import eu.ntdev.raidreport.exception.RaidTeamException;
import eu.ntdev.raidreport.exception.TeamPermissionException;
import eu.ntdev.raidreport.model.RaidBoss;
import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.teams.RaidTeamUser;
import eu.ntdev.raidreport.model.teams.TeamInviteStatus;
import eu.ntdev.raidreport.model.teams.TeamPermission;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.RaidTeamRepository;
import eu.ntdev.raidreport.persistence.RenderTargetRepository;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Niklas on 14.10.2017.
 */
@Service
@Transactional
public class RaidTeamService {

    private static final Logger LOG = Logger.getLogger(RaidTeamService.class);
    private static final Long DEFAULT_RENDER_TARGET_ID = 1L;

    private final RaidTeamRepository raidTeamRepository;
    private final RenderTargetRepository renderTargetRepository;


    @Autowired
    public RaidTeamService(RaidTeamRepository raidTeamRepository, RenderTargetRepository renderTargetRepository) {
        this.raidTeamRepository = raidTeamRepository;
        this.renderTargetRepository = renderTargetRepository;
    }


    private static Optional<RaidTeamUser> getUser(User user, RaidTeam raidTeam) {
        return user.getRaidTeams().stream().filter(raidTeamUser -> raidTeamUser.getRaidTeam().equals(raidTeam)).findAny();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void checkAccess(User user, RaidTeam raidTeam) {
        getUser(user, raidTeam).orElseThrow(RaidTeamException::forbidden);
    }

    static boolean checkAccess(User user, RaidTeam raidTeam, TeamPermission permission) {
        RaidTeamUser raidTeamUser = getUser(user, raidTeam).orElseThrow(RaidTeamException::forbidden);
        if(!raidTeamUser.getPermissions().contains(permission)) {
            throw TeamPermissionException.forbidden(permission, raidTeam.getId());
        }
        return true;
    }

    public RaidTeam createNewTeam(User user, String teamName) {
        RaidTeam raidTeam = new RaidTeam();
        raidTeam.setTeamName(teamName);
        // Raid team <> user association
        // This association is bidirectional, thats why it has to be manually set.
        RaidTeamUser raidTeamUser = associate(user, raidTeam, TeamInviteStatus.NONE);
        raidTeamUser.setPermissions(TeamPermission.asSet());

        RaidTeamRenderTarget renderTarget = renderTargetRepository.getOne(DEFAULT_RENDER_TARGET_ID);
        raidTeam.setRenderTarget(renderTarget);
        return raidTeamRepository.save(raidTeam);
    }

    @Transactional
    public RaidTeam inviteUser(User inviter, RaidTeam raidTeam, User joining) {
        LOG.info(inviter + " invited " + joining + " to " + raidTeam);
        checkAccess(inviter, raidTeam, TeamPermission.INVITE);
        if(!raidTeam.has(joining)) {
            associate(joining, raidTeam, TeamInviteStatus.INVITE_SENT);
        }
        return raidTeam;
    }

    public Set<ReportFile> retrieveAllReports(RaidTeam raidTeam, User user) {
        if(raidTeam.has(user)) {
            return raidTeam.getReports();
        }
        throw RaidTeamException.forbidden();
    }


    @Transactional
    public void acceptInvite(User invited, RaidTeam raidTeam) {
        Optional<RaidTeamUser> user = getUser(invited, raidTeam);
        RaidTeamUser actualUser = user.orElseThrow(RaidTeamException::notInvited);
        actualUser.setTeamInviteStatus(TeamInviteStatus.JOINED);
    }

    @Transactional
    public void rejectInvite(User invited, RaidTeam raidTeam) {
        Optional<RaidTeamUser> user = getUser(invited, raidTeam);
        RaidTeamUser actualUser = user.orElseThrow(RaidTeamException::notInvited);
        deAssociate(invited, raidTeam);
    }

    @Transactional
    public void leaveTeam(User leavingUser, RaidTeam raidTeam) {
        Optional<RaidTeamUser> userOptional = getUser(leavingUser, raidTeam);
        RaidTeamUser raidTeamUser = userOptional.orElseThrow(RaidTeamException::notInvited);
        if(raidTeamUser.getTeamInviteStatus().equals(TeamInviteStatus.NONE)) {
            throw RaidTeamException.canNotLeave();
        }
        if(raidTeamUser.getTeamInviteStatus().equals(TeamInviteStatus.INVITE_SENT)) {
            throw RaidTeamException.notYetAccepted();
        } else {
            deAssociate(leavingUser, raidTeam);
        }
    }

    @Transactional
    public void shareTeam(User sharingUser, RaidTeam raidTeam) {
        LOG.info("User " + sharingUser.toString() + " invoked sharing of team id=" + raidTeam.getId());
        checkAccess(sharingUser, raidTeam);
        if(!raidTeam.isShared()) {
            raidTeam.setShared(true);
            raidTeam.setShareId(UUID.randomUUID().toString());
        }
    }

    @Nullable
    public RaidTeam findSharedTeam(String shareId) {
        LOG.info("Retrieving shared team for shareId " + shareId);
        return raidTeamRepository.findOneByShareId(shareId);
    }

    @Transactional
    public void unShareTeam(User user, RaidTeam raidTeam) {
        LOG.info("User " + user.toString() + " unsharing sharing of team id=" + raidTeam.getId());
        checkAccess(user, raidTeam);
        raidTeam.setShared(false);
        raidTeam.setShareId(null);
    }

    @Transactional
    public void grantPermission(User grantingUser, User grantedUser, RaidTeam team, TeamPermission permission) {
        checkAccess(grantingUser, team, TeamPermission.MANAGE_PERMISSION);
        RaidTeamUser user = team.findForUser(grantedUser).get();
        user.getPermissions().add(permission);
    }

    @Transactional
    public void revokePermission(User revokingUser, User revokedUser, RaidTeam team, TeamPermission permission) {
        checkAccess(revokingUser, team, TeamPermission.MANAGE_PERMISSION);
        RaidTeamUser user = team.findForUser(revokedUser).get();
        user.getPermissions().remove(permission);
        // This is supposed to circumvent leaving a team without permission management
        if(permission == TeamPermission.MANAGE_PERMISSION) {
            boolean hasOneRemaining = team.getUsers()
                    .stream()
                    .flatMap(raidTeamUser -> raidTeamUser.getPermissions().stream())
                    .anyMatch(permission1 -> permission1.equals(permission));
            if(!hasOneRemaining) {
                user.getPermissions().add(TeamPermission.MANAGE_PERMISSION);
                throw TeamPermissionException.denied(TeamPermission.MANAGE_PERMISSION, team.getId());
            }
        }
    }

    public Map<String, Long> calculateParticipationForTeam(User user, RaidTeam raidTeam) {
        checkAccess(user, raidTeam);
        return raidTeam.getReports()
                .stream()
                .filter(raid -> hasParticipated(user, raid))
                .map(ReportFile::getRaidBoss)
                .filter(Objects::nonNull)
                .map(RaidBoss::getName)
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    @Transactional
    public void shareReport(User sharingUser, RaidTeam raidTeam, Long reportId) {
        checkAccess(sharingUser, raidTeam);
        Optional<ReportFile> file = raidTeam.getReports()
                .stream()
                .filter(reportFile -> reportFile.getId().equals(reportId))
                .findAny();
        file.ifPresent(ReportFile::share);
    }

    @Transactional
    public void unShareReport(User unSharingUser, RaidTeam raidTeam, Long reportId) {
        checkAccess(unSharingUser, raidTeam);
        Optional<ReportFile> file = raidTeam.getReports()
                .stream()
                .filter(reportFile -> reportFile.getId().equals(reportId))
                .findAny();
        file.ifPresent(ReportFile::unShare);
    }

    public TeamPerformanceAnalysisResult analyzeTeamPerformance(User requestingUser, RaidTeam raidTeam) {
        checkAccess(requestingUser, raidTeam);
        TeamPerformanceAnalysisResult result = new TeamPerformanceAnalysisResult();
        raidTeam.getReports().forEach(result::add);
        result.sort();
        return result;
    }

    @Transactional
    public void changeRenderTarget(User requestingUser, RaidTeam raidTeam, String newRenderTargetName) {
        checkAccess(requestingUser, raidTeam, TeamPermission.MANAGE_PERMISSION);
        RaidTeamRenderTarget renderTarget = renderTargetRepository
                .findRenderTargetByName(newRenderTargetName)
                .orElseThrow(() -> RaidTeamException.renderTargetNotFound(newRenderTargetName));
        raidTeam.setRenderTarget(renderTarget);
    }

    private boolean hasParticipated(User user, ReportFile reportFile) {
        return reportFile.getParticipants()
                .stream()
                .anyMatch(participatingAccount -> participatingAccount.getPlayerAccount().equals(user.getPlayerAccount()));
    }

    /**
     * Associates the given user with the given raid team.
     * @param user to be associated to the team
     * @param raidTeam to which the user is associated
     * @return the association
     */
    private RaidTeamUser associate(User user, RaidTeam raidTeam, TeamInviteStatus teamInviteStatus) {
        RaidTeamUser raidTeamUser = new RaidTeamUser();
        raidTeamUser.setRaidTeam(raidTeam);
        raidTeamUser.setUser(user);
        raidTeamUser.setTeamInviteStatus(teamInviteStatus);
        raidTeamUser.setUserName(user.getUsername());

        raidTeam.getUsers().add(raidTeamUser);
        user.getRaidTeams().add(raidTeamUser);
        return raidTeamUser;
    }

    private void deAssociate(User user, RaidTeam raidTeam) {
        boolean removedFromTeam = raidTeam.getUsers().removeIf(raidTeamUser -> raidTeamUser.getUser().equals(user));
        boolean removedFromUser = user.getRaidTeams().removeIf(raidTeamUser -> raidTeamUser.getRaidTeam().equals(raidTeam));
        if(!(removedFromTeam && removedFromUser)) {
            throw new IllegalStateException();
        }
    }
}
