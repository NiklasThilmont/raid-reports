package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.exception.RaidTeamException;
import eu.ntdev.raidreport.exception.ReportException;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.teams.TeamPermission;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.RaidTeamRepository;
import eu.ntdev.raidreport.persistence.ReportFileRepository;
import eu.ntdev.raidreport.util.CompressionService;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

/**
 *
 */
@Service
@Transactional
public class RaidTeamReportService {

    private static final Logger LOG = Logger.getLogger(RaidTeamReportService.class);

    private final ReportFileRepository reportFileRepository;

    private final RaidTeamRepository raidTeamRepository;

    private final ReportConvertService reportConvertService;

    private final CompressionService compressionService;


    /**
     * Expiration time of report files in hours.
     */
    @Value("${report-expiration-hours}")
    private Integer expirationTime;




    @Autowired
    public RaidTeamReportService(ReportFileRepository reportFileRepository, RaidTeamRepository raidTeamRepository, ReportConvertService reportConvertService, CompressionService compressionService) {
        this.reportFileRepository = reportFileRepository;
        this.raidTeamRepository = raidTeamRepository;
        this.reportConvertService = reportConvertService;
        this.compressionService = compressionService;
    }

    @Transactional
    public void uploadToTeam(@NotNull RaidTeam raidTeam, @NotNull InputStream inputStream, @Nullable String comment, @NotNull User user) {
        RaidTeamService.checkAccess(user, raidTeam, TeamPermission.UPLOAD);
        ReportFile reportFile;
        try {
            reportFile = reportConvertService.convertLogToReport(inputStream, user, comment, raidTeam);
            raidTeam.getReports().add(reportFile);
        } catch (IOException | ParseException | InterruptedException e) {
           throw new RuntimeException(e);
        }
    }

    @Transactional
    public void deleteReportFromTeam(Long teamId, Long reportId, User user) {
        RaidTeam raidTeam = raidTeamRepository.findOne(teamId);
        ReportFile reportFile = reportFileRepository.findOne(reportId);
        // There are two possible ways for a user to delete their file:
        // A) User has DELETE_REPORT permissions in the team
        // B) User owns the team
        if(raidTeam == null) {
            throw RaidTeamException.notFound(teamId);
        }
        if(!raidTeam.has(user)) {
            throw RaidTeamException.forbidden();
        }
        boolean hasAccess = (reportFile != null && reportFile.getUser().equals(user)) ||  RaidTeamService.checkAccess(user, raidTeam, TeamPermission.DELETE_REPORT);
        if(hasAccess) {
            reportFileRepository.delete(reportFile);
            raidTeam.getReports().remove(reportFile);
            deleteFile(reportFile);
        } else {
            throw ReportException.deleteForbidden(reportId);
        }
    }

    private InputStream retrieveFile(RaidTeam raidTeam, Long fileId) {
        ReportFile reportFile = raidTeam.getReports()
                .stream()
                .filter(file -> file.getId().equals(fileId))
                .findAny()
                .orElseThrow(() -> RaidTeamException.reportNotFound(raidTeam.getId()));
        return getFileStream(reportFile);
    }
    private InputStream getFileStream(ReportFile reportFile) {
        if (!reportFile.isCompressed()) {
            return compressionService.getUncompressed(reportFile.getFileName());
        } else {
            return compressionService.getCompressed(reportFile.getFileName());
        }
    }

    public InputStream retrieveFileFromPublicTeam(String shareId, Long fileId) {
        RaidTeam raidTeam = raidTeamRepository.findOneByShareId(shareId);
        if(raidTeam == null) {
            throw RaidTeamException.sharedNotFound(shareId);
        }
        if(!raidTeam.isShared()) {
            throw RaidTeamException.notShared(raidTeam.getId());
        }
        return retrieveFile(raidTeam, fileId);
    }

    public InputStream retrieveSharedReport(String shareId) {
        ReportFile reportFile = reportFileRepository.findOneByShareIdAndShared(shareId, true);
        if(reportFile == null) {
            throw ReportException.sharedNotFound(shareId);
        }
        return getFileStream(reportFile);
    }


    public InputStream retrieveFileFromFileSystem(Long teamId, Long fileId, User user) {
        RaidTeam raidTeam = raidTeamRepository.findOne(teamId);
        if(raidTeam == null) {
            throw RaidTeamException.notFound(teamId);
        }
        if(!raidTeam.has(user)) {
            throw RaidTeamException.forbidden();
        }
        return retrieveFile(raidTeam, fileId);
    }

    private void deleteFile(File file) {
        if(file.delete()) {
            LOG.info("File " + file.getAbsolutePath() + " successfully deleted.");
        } else {
            LOG.warn("File " + file.getAbsolutePath() + " could not be deleted.");
        }
    }

    private void deleteFile(ReportFile reportFile) {
        File htmlFile = new File(reportFile.getFileName());
        deleteFile(htmlFile);

        String evtcFileName = reportFile.getEvtcFileName();
        if(evtcFileName != null) {
            File evtcFile = new File(reportFile.getEvtcFileName());
            deleteFile(evtcFile);
        }
    }
}
