package eu.ntdev.raidreport.model.teams;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Converter
public class PermissionConverter implements AttributeConverter<Set<TeamPermission>, String> {
    @Override
    public String convertToDatabaseColumn(Set<TeamPermission> attribute) {
        return String.join(",", attribute.stream().map(Enum::toString).collect(Collectors.toSet()));
    }

    @Override
    public Set<TeamPermission> convertToEntityAttribute(String dbData) {
        if(dbData == null || dbData.trim().isEmpty()) {
            return new HashSet<>();
        }
        List<String> permissions = Arrays.asList(dbData.split(","));
        return permissions.stream().map(TeamPermission::valueOf).collect(Collectors.toSet());
    }
}
