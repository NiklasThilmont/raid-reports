package eu.ntdev.raidreport.model.account;

import eu.ntdev.raidreport.model.user.User;
import lombok.Data;

import javax.persistence.*;

/**
 * Purpose of this {@link PlayerAccount} is to provide a layer of abstraction for the linking of {@link eu.ntdev.raidreport.model.ReportFile}
 * to {@link User}.
 * <p>
 *     When a report is parsed, up to 10 accounts are provided by their account name. These account names do not necessary
 *     reflect an existing user. Instead, if not yet created, an appropriate account is made and persisted. When
 *     a user identifies themselves with an API key, they will be assigned to that account
 * </p>
 * <p>
 *     This way, account data is saved and stored for each participant without them needing to be registered
 *     or without the need for re-parsing of evtc report.
 * </p>
 */
@Entity
@Table(name = "player_account")
@Data
public class PlayerAccount {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "account_name", length = 100, unique = true, nullable = false)
    private String accountName;


    public PlayerAccount() {
    }

    public PlayerAccount(String accountName) {
        this.accountName = accountName;
    }
}
