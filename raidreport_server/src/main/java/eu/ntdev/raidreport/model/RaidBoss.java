package eu.ntdev.raidreport.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "raid_boss")
@Entity
@Data
public class RaidBoss {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @Column(name = "boss_order")
    private Integer order;

    @ElementCollection
    private List<Long> classIds;

    public eu.ntdev.raidreport.parser.model.types.RaidBoss map() {
        eu.ntdev.raidreport.parser.model.types.RaidBoss boss = new eu.ntdev.raidreport.parser.model.types.RaidBoss();
        boss.setName(name);
        boss.setEvtcIds(new ArrayList<>(classIds));
        return boss;
    }
}
