package eu.ntdev.raidreport.model.account;

import eu.ntdev.raidreport.parser.model.types.AgentType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "participating_account")
public class ParticipatingAccount {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "player_account_id", nullable = false)
    private PlayerAccount playerAccount;

    private String charName;

    @Column(name = "class_name")
    @Enumerated(EnumType.STRING)
    private AgentType className;

}
