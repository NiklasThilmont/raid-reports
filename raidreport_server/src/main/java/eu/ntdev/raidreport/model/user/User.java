package eu.ntdev.raidreport.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.ntdev.raidreport.model.account.PlayerAccount;
import eu.ntdev.raidreport.model.teams.RaidTeamUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

/**
 * A user. Password store in plain text (for now...)
 */
@Entity
@Data
@EqualsAndHashCode(exclude = {"roles", "raidTeams"})
@ToString(exclude = {"roles", "raidTeams"})
public class User {
    @Id
    @GeneratedValue
    private Long id;

    private String username;

    private String password;

    private Boolean enabled = false;

    private String apiKey;

    @JsonIgnore
    private String passwordResetId;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cascade(value = {CascadeType.PERSIST})
    @Fetch(FetchMode.SUBSELECT)
    private List<UserRole> roles;


    @OneToMany(mappedBy = "user", cascade = ALL, orphanRemoval = true)
    @JsonIgnore
    @Fetch(FetchMode.SUBSELECT)
    private List<RaidTeamUser> raidTeams = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "account_id")
    @JsonIgnore
    private PlayerAccount playerAccount;

    public User() {
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public List<UserRole> getRoles() {
        return roles;
    }

    @JsonProperty("roles")
    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    public boolean resetIdMatches(@Nullable String passwordResetId) {
        if(this.passwordResetId == null || passwordResetId == null) {
            return false;
        }
        return passwordResetId.equals(this.passwordResetId);
    }
}
