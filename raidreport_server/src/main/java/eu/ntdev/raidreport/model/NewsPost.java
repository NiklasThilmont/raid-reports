package eu.ntdev.raidreport.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity()
@Table(name = "news_post")
public class NewsPost {
    @Id
    private Integer id;

    @Column(name = "post_date")
    private LocalDateTime postDate;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "title")
    private String title;

    @Column(name = "subtitle")
    private String subtitle;

    @Column(name = "body", length = 20000)
    private String body;

    @Column(name = "author")
    private String author;
}
