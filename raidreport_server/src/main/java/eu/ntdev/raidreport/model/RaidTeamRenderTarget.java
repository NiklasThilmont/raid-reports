package eu.ntdev.raidreport.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Table(name = "render_target")
@Entity()
@Data
public class RaidTeamRenderTarget {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "name", length = 50)
    private String name;

    @Column(name = "icon")
    private String iconLocation;

    @Column(name = "command", length = 2000, nullable = false)
    @JsonIgnore
    private String command;

    @Column(name = "location", length = 2000, nullable = false)
    @JsonIgnore
    private String location;

}
