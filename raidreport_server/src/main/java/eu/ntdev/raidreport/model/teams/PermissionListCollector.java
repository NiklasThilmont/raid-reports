package eu.ntdev.raidreport.model.teams;

import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
public class PermissionListCollector implements Collector<TeamPermission, StringBuilder, String> {

    @Override
    public Supplier<StringBuilder> supplier() {
        return StringBuilder::new;
    }

    @Override
    public BiConsumer<StringBuilder, TeamPermission> accumulator() {
        return (stringBuilder, permission) -> stringBuilder.append(",").append(permission);
    }

    @Override
    public BinaryOperator<StringBuilder> combiner() {
        return StringBuilder::append;
    }

    @Override
    public Function<StringBuilder, String> finisher() {
        return StringBuilder::toString;
    }

    @Override
    public Set<Collector.Characteristics> characteristics() {
        return new HashSet<>();
    }
}
