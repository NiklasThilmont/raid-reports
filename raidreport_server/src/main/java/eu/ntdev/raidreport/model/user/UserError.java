package eu.ntdev.raidreport.model.user;

/**
 * Created by Niklas on 09.07.2017.
 */
public enum UserError {
    ERR_UNKNOWN,
    ERR_NOT_FOUND,
    ERR_DUPLICATE_USER,
    ERR_USERNAME_TOO_SHORT,
    ERR_PASSWORD_TOO_SHORT,
    ERR_PASSWORD_TOO_LONG,
    ERR_USERNAME_TOO_LONG,
    ERR_PASSWORD_RESET_NOT_POSSIBLE
}
