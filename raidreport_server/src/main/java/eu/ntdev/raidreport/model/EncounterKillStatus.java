package eu.ntdev.raidreport.model;


import eu.ntdev.raidreport.dto.TeamPerformanceAnalysisResult;

/**
 * Created by Niklas on 02.07.2017.
 */
public enum  EncounterKillStatus {
    SUCCESSFUL {
        @Override
        public void addToData(TeamPerformanceAnalysisResult data) {
            data.incSuccess();
        }
    },
    FAILURE {
        @Override
        public void addToData(TeamPerformanceAnalysisResult data) {
            data.incFailCount();
        }
    },
    UNKNOWN {
        @Override
        public void addToData(TeamPerformanceAnalysisResult data) {
            data.incUnknown();
        }
    };

    public abstract void addToData(TeamPerformanceAnalysisResult data);
}
