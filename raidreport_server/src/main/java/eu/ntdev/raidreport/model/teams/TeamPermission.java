package eu.ntdev.raidreport.model.teams;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum  TeamPermission {
    INVITE,
    KICK,
    UPLOAD,
    DELETE_REPORT,
    MANAGE_PERMISSION;

    public static Set<TeamPermission> asSet() {
        return new HashSet<>(Arrays.asList(TeamPermission.values()));
    }
}
