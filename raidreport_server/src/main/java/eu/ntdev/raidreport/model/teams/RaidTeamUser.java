package eu.ntdev.raidreport.model.teams;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eu.ntdev.raidreport.model.user.User;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Table(name = "RAID_TEAM_USER")
@IdClass(RaidTeamUserPK.class)
public class RaidTeamUser {


    @Id
    @ManyToOne
    @JoinColumn(name = "RAID_TEAM_ID")
    @JsonIgnore
    private RaidTeam raidTeam;

    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "INVITE_STATUS")
    @Enumerated(value = EnumType.STRING)
    private TeamInviteStatus teamInviteStatus;

    private String userName;

    @Column(name = "permissions", length = 5000)
    @Convert(converter = PermissionConverter.class)
    private Set<TeamPermission> permissions = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RaidTeamUser that = (RaidTeamUser) o;

        if (raidTeam != null ? !raidTeam.equals(that.raidTeam) : that.raidTeam != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return teamInviteStatus == that.teamInviteStatus;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (raidTeam != null ? raidTeam.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (teamInviteStatus != null ? teamInviteStatus.hashCode() : 0);
        return result;
    }


}
