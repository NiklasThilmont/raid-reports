package eu.ntdev.raidreport.model.teams;

import eu.ntdev.raidreport.model.RaidTeamRenderTarget;
import eu.ntdev.raidreport.model.ReportFile;
import eu.ntdev.raidreport.model.user.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.*;

import static javax.persistence.CascadeType.ALL;

@Entity
@Data
@Table(name = "raid_team")
@EqualsAndHashCode(exclude = {"users"})
@ToString(exclude = "users")
public class RaidTeam {

    @Id
    @GeneratedValue
    private Long id;

    private String teamName;

    @Column(name = "is_shared")
    private boolean shared;

    @Column(name = "share_id")
    private String shareId;

    @ManyToOne()
    @JoinColumn(name = "render_target_id", nullable = false)
    private RaidTeamRenderTarget renderTarget;

    @OneToMany(mappedBy = "raidTeam", cascade = ALL, orphanRemoval = true)
    @Cascade(CascadeType.ALL)
    private List<RaidTeamUser> users = new ArrayList<>();

    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "RAID_TEAM_ID", nullable = false)
    private Set<ReportFile> reports = new HashSet<>();

    public boolean has(User user) {
        return this.users.stream().anyMatch(raidTeamUser -> raidTeamUser.getUser().equals(user));
    }

    public Optional<RaidTeamUser> findForUser(User user) {
        return this.users.stream().filter(raidTeamUser -> raidTeamUser.getUser().equals(user)).findAny();
    }

}
