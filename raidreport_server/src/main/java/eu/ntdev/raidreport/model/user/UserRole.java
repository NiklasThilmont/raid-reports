package eu.ntdev.raidreport.model.user;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity
@Data
public class UserRole implements GrantedAuthority {
    public static final String USER_ROLE_ADMIN = "ADMIN";

    public static final String USER_ROLE_USER = "USER";

    public static final String USER_ROLE_ACTUATOR = "ACTUATOR";

    @Id
    @GeneratedValue
    private Long id;

    private String roleName;


    public UserRole() {
        this.roleName = "";
    }

    public UserRole(String roleName) {
        this.roleName = roleName;
    }

    @Override
    @Transient
    public String getAuthority() {
        return roleName;
    }


}
