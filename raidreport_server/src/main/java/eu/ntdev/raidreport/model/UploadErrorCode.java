package eu.ntdev.raidreport.model;

/**
 * Created by Niklas on 01.07.2017.
 */
public enum UploadErrorCode {
    /**
     * A report comment was too long
     */
    ERR_COMMENT_TOO_LONG,
    /**
     * The raid date was not extractable from the raid report
     */
    ERR_COULD_NOT_EXTRACT_DATE,
    /**
     * The boss name was not extracable from the html
     */
    ERR_COULD_NOT_EXTRACT_BOSS,
    /**
     * Report was empty...for some reason. No bytes available
     */
    FILE_EMPTY,
    /**
     * No uploader was define. Uploader names are arbitrary, but need to be defined
     */
    ERR_UPLOADER_UNKNOWN,
    /**
     * Some unknown error with the report.
     */
    BAD_REPORT,

    /**
     * File with the same user and date already exists
     */
    ERR_DUPLICATE,

    /**
     * File system problems, memory problems, probelmatic problems.
     */
    ERR_UNKNOWN,

    /**
     * The team to which the file was being uploaded did not exit
     */
    ERR_TEAM_NOT_FOUND,

    /**
     * The user was not part of the raid team
     */
    ERR_NOT_IN_TEAM
}
