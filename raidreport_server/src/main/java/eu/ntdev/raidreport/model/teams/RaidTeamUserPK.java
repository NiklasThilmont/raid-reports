package eu.ntdev.raidreport.model.teams;

import lombok.Data;

import java.io.Serializable;

@Data
public class RaidTeamUserPK implements Serializable {
    private Long raidTeam;

    private Long user;
}
