package eu.ntdev.raidreport.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.ntdev.raidreport.model.account.ParticipatingAccount;
import eu.ntdev.raidreport.model.user.User;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;


@Data
@Entity()
@Table(name = "report_file")
public class ReportFile {
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Full path leading to the referenced file.
     */
    @NotNull
    @JsonIgnore
    private String fileName = "";

    /**
     * Full path leading to the base evtc log file.
     */
    @JsonIgnore
    @Nullable
    private String evtcFileName = "";

    /**
     * Part of the report metadata
     */
    @JsonProperty
    @Column(name = "raid_boss")
    private String raidBossName;

    @ManyToOne
    @JoinColumn(name = "raid_boss_id")
    private RaidBoss raidBoss;

    /**
     * No longer the upload date but the raid date.
     */
    private LocalDateTime uploadDate;

    private LocalDateTime killDate;

    /**
     * Optional comment.
     */
    private String comment;

    /**
     * Kill time in miliseconds.
     */
    private Long killTimeInMs;

    private String shareId;

    private boolean shared;

    private boolean success;

    @Column(name = "is_compressed")
    private boolean compressed;

    /**
     * The report was rendered by this piece.
     */
    @ManyToOne
    @JoinColumn(name = "render_target_id", nullable = false)
    @JsonIgnore
    private RaidTeamRenderTarget renderTarget;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "encounter_kill_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private EncounterKillStatus encounterKillStatus;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ParticipatingAccount> participants;

    /**
     * The age of this report, relative to the given date.
     * @return differen in hours
     */
    public Long ageInHours(LocalDateTime date) {
        return ChronoUnit.HOURS.between(this.uploadDate, date);
    }

    public void share() {
        if(!this.shared) {
            this.shareId = UUID.randomUUID().toString();
            this.shared = true;
        }
    }

    public void unShare() {
        this.shared = false;
    }
}
