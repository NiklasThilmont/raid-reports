package eu.ntdev.raidreport.model.teams;

/**
 * Created by Niklas on 14.10.2017.
 */
public enum TeamInviteStatus {
    INVITE_SENT,
    NONE,
    JOINED,
}
