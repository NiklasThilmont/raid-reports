package eu.ntdev.raidreport.service;

import eu.ntdev.raidreport.persistence.RaidTeamRepository;
import eu.ntdev.raidreport.exception.RaidTeamException;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.teams.RaidTeamUser;
import eu.ntdev.raidreport.model.teams.TeamInviteStatus;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.RenderTargetRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Matchers;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * Created by Niklas on 14.10.2017.
 */
@SuppressWarnings("ConstantConditions")
public class RaidTeamServiceTest  {

    private RaidTeamService raidTeamService;

    private User userBob;
    private User userAlice;

    @Before
    public void setUp() throws Exception {
        RaidTeamRepository raidTeamRepository = BDDMockito.mock(RaidTeamRepository.class);
        when(raidTeamRepository.save(Matchers.any(RaidTeam.class))).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            return (RaidTeam) args[0];
        });
        RenderTargetRepository renderTargetRepository = mock(RenderTargetRepository.class);
        raidTeamService = new RaidTeamService(raidTeamRepository, renderTargetRepository);

        userBob = makeUser("Bob", 1L);
        userAlice = makeUser("Alice", 2L);
    }

    private static User makeUser(String name, Long id) {
        User user = new User();
        user.setUsername(name);
        user.setId(id);
        return user;
    }

    @Test
    public void userShouldBeAbleToCreateATeam() {
        String teamName = "RaidTeam1";
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, teamName);
        assertThat(raidTeam.getTeamName()).isEqualTo(teamName);
    }

    @Test
    public void userShouldBeInTeamAfterCreation() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        assertThat(raidTeam.getUsers()).extracting("user").containsExactlyInAnyOrder(userBob);
        assertThat(raidTeam.getUsers()).extracting("raidTeam").containsExactlyInAnyOrder(raidTeam);

        Assertions.assertThat(userBob.getRaidTeams()).extracting("user").containsExactlyInAnyOrder(userBob);
        Assertions.assertThat(userBob.getRaidTeams()).extracting("raidTeam").containsExactlyInAnyOrder(raidTeam);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void ownerShouldBeAbleToInviteOtherUsers() {
        User joining = makeUser("Alice", 2L);
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.inviteUser(userBob, raidTeam, joining);
        assertThat(raidTeam.getUsers()).extracting("user").containsExactlyInAnyOrder(userBob, joining);
        Assertions.assertThat(joining.getRaidTeams()).extracting("raidTeam").containsExactlyInAnyOrder(raidTeam);
        // Get the relevant association, make sure status is in "INVITE"
        // This is important, as a user does have the option of choice to join
        Optional<RaidTeamUser> raidTeamUserOption = raidTeam.getUsers().stream().filter(user -> user.getUser().equals(joining)).findAny();
        assertThat(raidTeamUserOption.get().getTeamInviteStatus()).isEqualTo(TeamInviteStatus.INVITE_SENT);
        assertThat(raidTeamUserOption.get().getUserName()).isEqualTo("Alice");
    }

    @Test
    public void duplicateInvitesShouldHaveNoEffect() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeam.setId(22L);
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);

        assertThat(raidTeam.getUsers().size()).isEqualTo(2);
        assertThat(raidTeam.getUsers()).extracting("user").containsExactlyInAnyOrder(userBob, userAlice);
        Assertions.assertThat(userAlice.getRaidTeams()).extracting("raidTeam").containsExactlyInAnyOrder(raidTeam);
    }

    @Test
    public void ownerHasInviteStatusNone() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        Optional<RaidTeamUser> raidTeamUserOption = raidTeam.getUsers().stream().filter(user -> user.getUser().equals(userBob)).findAny();
        assertThat(raidTeamUserOption.get().getTeamInviteStatus()).isEqualTo(TeamInviteStatus.NONE);
    }

    @Test(expected = RaidTeamException.class)
    public void onlyMembersCanAccessReports() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.retrieveAllReports(raidTeam, userAlice);
    }

    @Test
    public void usersShouldBeAbleToAcceptInvites() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);
        raidTeamService.acceptInvite(userAlice, raidTeam);
        RaidTeamUser raidTeamUser = raidTeam.getUsers().stream().filter(u -> u.getUser().equals(userAlice)).findAny().get();
        assertThat(raidTeamUser.getTeamInviteStatus()).isEqualTo(TeamInviteStatus.JOINED);
    }

    @Test(expected = RaidTeamException.class)
    public void canOnlyAcceptIfInviteSent() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.acceptInvite(userAlice, raidTeam);
    }

    @Test
    public void usersShouldBeAbleToRejectInvites() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);
        raidTeamService.rejectInvite(userAlice, raidTeam);
        Optional<RaidTeamUser> raidTeamUser = raidTeam.getUsers().stream().filter(u -> u.getUser().equals(userAlice)).findAny();
        Optional<RaidTeamUser> raidTeamUserUserSide = userAlice.getRaidTeams().stream().filter(u -> u.getUser().equals(userAlice)).findAny();
        assertThat(raidTeamUserUserSide.isPresent()).isFalse();
        assertThat(raidTeamUser.isPresent()).isFalse();
    }

    @Test
    public void usersShouldBeAbleToLeaveATeam() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);
        raidTeamService.acceptInvite(userAlice, raidTeam);
        raidTeamService.leaveTeam(userAlice, raidTeam);
        assertThat(raidTeam.has(userAlice)).isFalse();
    }

    @Test(expected = RaidTeamException.class)
    public void canOnlyLeaveWhenStatusIsNotPending() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.inviteUser(userBob, raidTeam, userAlice);
        raidTeamService.leaveTeam(userAlice, raidTeam);
    }

    @Test(expected = RaidTeamException.class)
    public void canOnlyLeaveWhenInvited() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.leaveTeam(userAlice, raidTeam);
    }

    @Test(expected = RaidTeamException.class)
    public void founderCanNotLeave() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "");
        raidTeamService.leaveTeam(userBob, raidTeam);
    }

    @Test
    public void teamsCanBeMadePublic() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "team-11");
        raidTeamService.shareTeam(userBob, raidTeam);
        assertThat(raidTeam.isShared()).isTrue();
        assertThat(raidTeam.getShareId()).isNotNull();
        assertThat(raidTeam.getShareId().length()).isEqualTo(36);
    }

    @Test(expected = RaidTeamException.class)
    public void teamsCanOnlyBeSharedByTeamMembers() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "team-11");
        raidTeamService.shareTeam(userAlice, raidTeam);
    }

    @Test
    public void shouldOnlyShareIfNotYetShared() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "team-11");
        raidTeamService.shareTeam(userBob, raidTeam);
        String shareId = raidTeam.getShareId();
        raidTeamService.shareTeam(userBob, raidTeam);
        assertThat(raidTeam.getShareId()).isEqualTo(shareId);
    }

    @Test
    public void shouldRevokeShare() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "team-11");
        raidTeamService.shareTeam(userBob, raidTeam);
        raidTeamService.unShareTeam(userBob, raidTeam);
        assertThat(raidTeam.getShareId()).isNull();
        assertThat(raidTeam.isShared()).isFalse();
    }

    @Test(expected = RaidTeamException.class)
    public void shouldThrowException_onlyMembersCanUnshare() {
        RaidTeam raidTeam = raidTeamService.createNewTeam(userBob, "team-11");
        raidTeamService.shareTeam(userBob, raidTeam);
        raidTeamService.unShareTeam(userAlice, raidTeam);
    }
}