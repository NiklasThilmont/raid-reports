package eu.ntdev.raidreport.service;


import eu.ntdev.raidreport.persistence.RaidTeamRepository;
import eu.ntdev.raidreport.persistence.ReportFileRepository;
import eu.ntdev.raidreport.exception.RaidTeamException;
import eu.ntdev.raidreport.exception.TeamPermissionException;
import eu.ntdev.raidreport.model.teams.RaidTeam;
import eu.ntdev.raidreport.model.teams.RaidTeamUser;
import eu.ntdev.raidreport.model.teams.TeamPermission;
import eu.ntdev.raidreport.model.user.User;
import eu.ntdev.raidreport.persistence.RenderTargetRepository;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Matchers;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RaidTeamPermissionTest {

    private static final Logger LOG = Logger.getLogger(RaidTeamPermissionTest.class);

    private RaidTeamService raidTeamService;

    private RaidTeamReportService raidTeamReportService;

    private User userBob;
    private User userAlice;
    private User userJane;

    private RaidTeam raidTeam;


    private static User makeUser(String name, Long id) {
        User user = new User();
        user.setUsername(name);
        user.setId(id);
        return user;
    }

    @SuppressWarnings("Duplicates")
    @Before
    public void setUp() throws Exception {
        RaidTeamRepository raidTeamRepository = BDDMockito.mock(RaidTeamRepository.class);
        when(raidTeamRepository.save(Matchers.any(RaidTeam.class))).thenAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            return (RaidTeam) args[0];
        });
        RenderTargetRepository renderTargetRepository = mock(RenderTargetRepository.class);



        raidTeamService = new RaidTeamService(raidTeamRepository, renderTargetRepository);
        raidTeamReportService = new RaidTeamReportService(BDDMockito.mock(ReportFileRepository.class), raidTeamRepository, null, null);
        userBob = makeUser("Bob", 1L);
        userAlice = makeUser("Alice", 2L);
        userJane = makeUser("Jane", 3L);
        raidTeam = raidTeamService.createNewTeam(userAlice, "Superteam");
        raidTeam.setId(1L);
        given(raidTeamRepository.findOne(Matchers.any(Long.class))).willReturn(raidTeam);
        given(raidTeamRepository.findOne(null)).willReturn(raidTeam);
    }

    private void inviteBob() {
        raidTeamService.inviteUser(userAlice, raidTeam, userBob);
        raidTeamService.acceptInvite(userBob, raidTeam);
    }

    @Test
    public void shouldHaveAllPermissions_userIsCreator() {
        inviteBob();
        RaidTeamUser raidTeamUser = raidTeam.findForUser(userAlice).orElse(null);
        assertThat(raidTeamUser).isNotNull();
        assertThat(raidTeamUser.getPermissions()).containsExactlyInAnyOrder(TeamPermission.values());
    }

    @Test
    public void shouldGrantAndRevokePermission() {
        inviteBob();
        for (TeamPermission teamPermission : TeamPermission.values()) {
            LOG.info("Testing permission " + teamPermission);
            raidTeamService.grantPermission(userAlice, userBob, raidTeam, teamPermission);
            assertThat(raidTeam.findForUser(userBob).get().getPermissions()).containsExactly(teamPermission);
            raidTeamService.revokePermission(userAlice, userBob, raidTeam, teamPermission);
            assertThat(raidTeam.findForUser(userBob).get().getPermissions()).isEmpty();
        }
    }

    @Test
    public void shouldDenyPermissionGrant_missiesPermission() {
        inviteBob();
        // Oh no, bob does not have enough permissions to do that
        try {
            raidTeamService.grantPermission(userBob, userAlice, raidTeam, TeamPermission.MANAGE_PERMISSION);
            fail("Expected " + TeamPermissionException.class);
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.MANAGE_PERMISSION);
        }
    }

    @Test
    public void shouldDenyPermissionRevoke_missesPermission() {
        inviteBob();
        // Oh no, bob does not have enough permissions to do that
        try {
            raidTeamService.revokePermission(userBob, userAlice, raidTeam, TeamPermission.MANAGE_PERMISSION);
            fail("Expected " + TeamPermissionException.class);
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.MANAGE_PERMISSION);
        }
    }

    @Test(expected = RaidTeamException.class)
    public void shouldDenyPermissionGrant_notInTeam() {
        raidTeamService.grantPermission(userBob, userAlice, raidTeam, TeamPermission.MANAGE_PERMISSION);
    }

    @Test(expected = RaidTeamException.class)
    public void shouldDenyPermissionRevoke_notInTeam() {
        raidTeamService.revokePermission(userBob, userAlice, raidTeam, TeamPermission.MANAGE_PERMISSION);
    }

    @Test
    public void shouldDenyInvite_noPermission() {
        raidTeamService.inviteUser(userAlice, raidTeam, userBob);
        raidTeamService.acceptInvite(userBob, raidTeam);
        try {
            // Bob is in team, but can't invite due to a lack of permissions
            raidTeamService.inviteUser(userBob, raidTeam, userJane);
            fail("Expected exception " + RaidTeamException.class.getName());
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.INVITE);
        }
    }

    @Test
    public void shouldDenyDelete_noPermission() {
        raidTeamService.inviteUser(userAlice, raidTeam, userBob);
        raidTeamService.acceptInvite(userBob, raidTeam);
        try {
            raidTeamReportService.deleteReportFromTeam(raidTeam.getId(), null, userBob);
            fail("Expected exception " + RaidTeamException.class.getName());
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.DELETE_REPORT);
        }
    }

    @Test
    public void shouldDenyUpload_noPermission() {
        raidTeamService.inviteUser(userAlice, raidTeam, userBob);
        raidTeamService.acceptInvite(userBob, raidTeam);
        try {
            raidTeamReportService.uploadToTeam(raidTeam, new ByteArrayInputStream(new byte[2]), "", userBob);
            fail("Expected exception " + RaidTeamException.class.getName());
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.UPLOAD);
        }
    }

    @Test
    public void shouldDenyRevoke_noOneWitManagePermissionsLeft() {
        try {
            raidTeamService.revokePermission(userAlice, userAlice, raidTeam, TeamPermission.MANAGE_PERMISSION);
            fail("Expected " + RaidTeamException.class.getName());
        } catch (TeamPermissionException e) {
            Assertions.assertThat(e.getMissingPermission()).isEqualTo(TeamPermission.MANAGE_PERMISSION);
            assertThat(e.getError()).isEqualTo("ERR_REVOKE_DENIED_NOONE_LEFT");
        }
    }
}
