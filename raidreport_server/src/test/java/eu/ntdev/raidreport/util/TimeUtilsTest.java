package eu.ntdev.raidreport.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Niklas on 02.07.2017.
 */
public class TimeUtilsTest {
    @Test
    public void extractMiliseconds() throws Exception {
        String minString = "18";
        String secString = "12.3";
        Integer expectedMs = 18 * 60 * 1000;
        expectedMs += 12 * 1000;
        expectedMs += 3 * 100;
        assertThat(TimeUtils.extractMiliseconds(minString, secString)).isEqualTo(expectedMs);

        String minString2 = "18";
        String secString2 = "12";
        Integer expectedMs2 = 18 * 60 * 1000;
        expectedMs2 += 12 * 1000;
        assertThat(TimeUtils.extractMiliseconds(minString2, secString2)).isEqualTo(expectedMs2);

        String minString3 = "-1";
        String secString3 = "12";
        Integer expectedMs3 = 0;
        assertThat(TimeUtils.extractMiliseconds(minString3, secString3)).isEqualTo(expectedMs3);

    }

    @Test
    public void extractMiliseconds1() throws Exception {
        String msString = "12.3";
        Integer expected = 12 * 1000 + 3 * 100;
        assertThat(TimeUtils.extractMiliseconds(msString)).isEqualTo(expected);

        String msString2 = "12";
        Integer expected2 = 12 * 1000;
        assertThat(TimeUtils.extractMiliseconds(msString2)).isEqualTo(expected2);

        String msString3 = "0";
        Integer expected3 = 0;
        assertThat(TimeUtils.extractMiliseconds(msString3)).isEqualTo(expected3);

        String msString4 = "12.333";
        Integer expected4 = 12 * 1000 + 333;
        assertThat(TimeUtils.extractMiliseconds(msString4)).isEqualTo(expected4);

        String msString5 = "12.3333";
        Integer expected5 = 12 * 1000 + 333;
        assertThat(TimeUtils.extractMiliseconds(msString5)).isEqualTo(expected5);

        String msString6 = "";
        Integer expected6 = 0;
        assertThat(TimeUtils.extractMiliseconds(msString6)).isEqualTo(expected6);

        String msString7 = "-1";
        Integer expected7 = 0;
        assertThat(TimeUtils.extractMiliseconds(msString7)).isEqualTo(expected7);
    }

}