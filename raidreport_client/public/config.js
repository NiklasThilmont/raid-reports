const FILE_UPLOAD_API = 'http://localhost:8080/reports/upload';
const GET_ALL_REPORT_FILES_API = 'http://localhost:8080/reports/';
const GET_AUTHORITIES_API = 'http://localhost:8080/users/';
const GET_INFO_API = 'http://localhost:8080/info';
const USER_API_BASE = "http://localhost:8080/users";
const RAID_TEAM_API_BASE = "http://localhost:8080/team";
const SHARE_API_BASE = "http://localhost:8080/share";
const ADMIN_API_BASE = "http://localhost:8080/admin";
const ICON_LOCATION = "";


/*const FILE_UPLOAD_API = 'https://nt-dev.eu/api/reports/upload';
const GET_ALL_REPORT_FILES_API = 'https://nt-dev.eu/api/reports/';
const GET_AUTHORITIES_API = 'https://nt-dev.eu/api/users/';
const GET_INFO_API = 'https://nt-dev.eu/api/info';
const USER_API_BASE = "https://nt-dev.eu/api/users";
const RAID_TEAM_API_BASE = "https://nt-dev.eu/api/team";
const SHARE_API_BASE = "https://nt-dev.eu/api/share";
const ICON_LOCATION = "/raidreport/";
const ADMIN_API_BASE = "https://nt-dev.eu/api/admin";*/
