import {ReportFileLeaf} from '../model/ReportFileTree';
import {DateUtils} from './DateUtils';
import {isNullOrUndefined} from 'util';
import {RaidBossCode} from '../model/RaidBossCode';
import {IParticipant} from '../model/team/Participant';
import {User} from "../model/user/User";
import {RaidTeamUser} from "../model/team/RaidTeamUser";
import {ComparatorBuilder} from 'ts-comparator';
export class Comparators {

    public static MakeReportFileLeafDateComparator(asc: boolean) {
        if(asc) {
            return (r1: ReportFileLeaf, r2: ReportFileLeaf) => Comparators.ReportFileLeafDateComparator(r1, r2);
        } else {
            return (r1: ReportFileLeaf, r2: ReportFileLeaf) => Comparators.ReportFileLeafDateComparator(r2, r1);
        }
    }

    public static MakeReportFileLeafBossComparator(asc: boolean) {
        if(asc) {
            return (r1: ReportFileLeaf, r2: ReportFileLeaf) => Comparators.ReportFileLeafBossComparator(r1, r2);
        } else {
            return (r1: ReportFileLeaf, r2: ReportFileLeaf) => Comparators.ReportFileLeafBossComparator(r2, r1);
        }
    }

    public static compareBoss(r1: ReportFileLeaf, r2: ReportFileLeaf) {
        if (r1.reportFile.boss() == null || r1.reportFile.boss().name == null) {
            return -1;
        }
        if (r2.reportFile.boss() == null || r2.reportFile.boss().name == null) {
            return 1;
        }
        return r1.reportFile.boss().name.localeCompare(r2.reportFile.boss().name);
    }

    public static compareParticipants(p1: IParticipant, p2: IParticipant) {
        return p1.className.localeCompare(p2.className);
    }

    public static ReportFileLeafDateComparator(r1: ReportFileLeaf, r2: ReportFileLeaf) {
        return DateUtils.compare(r1.reportFile.killDate(), r2.reportFile.killDate());
    }

    public static ReportFileLeafBossComparator(r1: ReportFileLeaf, r2: ReportFileLeaf) {
        if(isNullOrUndefined(r1.reportFile.boss().order)) return -1;
        if(isNullOrUndefined(r2.reportFile.boss().order)) return -1;
        return r1.reportFile.boss().order - r2.reportFile.boss().order;

    }

    public static compareNumbers(n1: number, n2: number) {
        if(n1 > n2) return 1;
        if(n1 === n2) return 0;
        return -1;
    }

    public static compareUsersByName(u1: User, u2: User) {
        if(!u1 || !u1.name()) {
            return -1;
        }
        if(!u2 || !u2.name()) {
            return 1;
        }
        return u1.name().localeCompare(u2.name());
    }

    public static compareRaidTeamUsersByJoinStatus(u1: RaidTeamUser, u2: RaidTeamUser) {
        if(!u1 && !u2) {
            return 0;
        }
        if(!u1) {
            return -1;
        }
        if(!u2) {
            return 1;
        }
        let indexStatus1 = RaidTeamUser.INVITE_STATES.indexOf(u1.teamInviteStatus());
        let indexStatus2 = RaidTeamUser.INVITE_STATES.indexOf(u2.teamInviteStatus());
        return Comparators.compareNumbers(indexStatus1, indexStatus2);
    }

}