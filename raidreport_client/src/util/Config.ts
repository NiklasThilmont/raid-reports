import {CookieAttributes} from 'js-cookie';
import {DateUtils} from './DateUtils';
declare const GET_INFO_API: string;
declare const USER_API_BASE: string;
declare const RAID_TEAM_API_BASE: string;
declare const SHARE_API_BASE: string;
declare const ADMIN_API_BASE: string;
declare const NEWS_API_BASE: string;

export class RouteConfig {
    public static readonly ROUTE_SHARED_TEAM_BASE = "/raidreport/public/team/";
    public static readonly ROUTE_DASHBOARD = "/raidreport";
    public static readonly ROUTE_SHARED_TEAM = RouteConfig.ROUTE_SHARED_TEAM_BASE + ":shareId";
    public static readonly ROUTE_RESET_PASSWORD = "/raidreport/password/reset/:resetId";
    public static readonly ROUTE_MANAGE_OWN_ACCOUNT = "/raidreport/me/account";
    public static readonly ROUTE_TEAM_OVERVIEW = "/raidreport/team/:id/overview";
    public static readonly ROUTE_TEAM_PERFORMANCE = "/raidreport/team/:teamId/performance"
    public static readonly ROUTE_MANAGE_BOSSES = "/raidreport/manage/bosses";
}

export class Config {
    public static readonly validateLoginApiString = USER_API_BASE + "/me/authorities";
    public static readonly GetInfoApiString = GET_INFO_API;
    public static readonly GetAllUsersApiString = USER_API_BASE;
    public static readonly GetMyRaidTeamsString = RAID_TEAM_API_BASE + "/myteams";
    public static readonly RaidTeamBaseString = RAID_TEAM_API_BASE;
    public static readonly GetUsersForNameString = USER_API_BASE + "/name";

    public static readonly USERNAME_COOKIE_NAME = 'report_upload_username';
    public static readonly PASSWORD_COOKIE_NAME = 'report_upload_password';

    public static readonly DEFAULT_SORT_FIELD = 'wing_boss';

    public static readonly MAX_COMMENT_CHARACTERS = 200;


    public static makeDeleteUserString(id: number) {
        return USER_API_BASE + "/" + id;
    }

    public static makeEnableUserString(id: number) {
        return USER_API_BASE + "/" + id + "/active";
    }

    public static makeGetAllReportsString(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/report";
    }

    public static makeFileUploadString(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/report";
    }

    public static makeDeleteReportString(teamId: number, reportId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/report/" + reportId;
    }

    public static makeInviteUserToRoomString(teamId: number, userToInviteId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/users/" + userToInviteId;
    }

    public static makeInviteString(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/invite"
    }

    public static makeGetFileString(teamOrShareId: number| string, reportId: number, isShared: boolean, boss: string, date: Date) {
        let dateString = new Intl.DateTimeFormat('en-US').format(date).split("/").join("_").toLowerCase();
        let filename = "/" + boss + "_" + dateString + ".html";
        if(isShared) {
            return SHARE_API_BASE + "/team/" + teamOrShareId + "/report/" + reportId + filename;
        } else {
            return RAID_TEAM_API_BASE + "/" + teamOrShareId + "/report/" + reportId + filename;
        }

    }

    public static makeLeaveTeamString(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/me";
    }

    public static makeShareTeamString(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/shared";
    }

    public static makePermissionString(teamId: number, userId: number, permission: string) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/" + userId + "/permission/" + permission;
    }

    public static makeGetSharedTeamString(shareId: string) {
        return SHARE_API_BASE + "/team/" + shareId;
    }

    public static makeShareUrl(shareId: string) {
        return window.location.origin + RouteConfig.ROUTE_SHARED_TEAM_BASE + shareId;
    }

    public static makeReportShareUrl(shareId: string) {
        return SHARE_API_BASE + "/report/" + shareId;
    }

    public static makeShareReportString(teamId: number, reportId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/report/" + reportId + "/shared";
    }

    public static makeResetPasswordUrl(userId: number) {
        return ADMIN_API_BASE + "/management/user/" + userId + "/password"
    }

    public static makeChangePasswordAPIUrl() {
        return USER_API_BASE + "/me/password";
    }

    public static makeChangePasswordUrl(resetId: string) {
        return window.location.origin + RouteConfig.ROUTE_RESET_PASSWORD.replace(":resetId", resetId);
    }

    public static makeGetAPIKeyUrl() {
        return USER_API_BASE + "/me/key";
    }

    public static makeSaveAPIKeyUrl() {
        return USER_API_BASE + "/me/key";
    }

    public static makeGetPerformanceAPIUrl(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/analysis";
    }

    public static bossAPIUrl() {
        return ADMIN_API_BASE + "/management/raid/bosses";
    }

    public static makeGetRenderTargetsURL() {
        return SHARE_API_BASE + "/rendering/targets";
    }

    public static makePatchRenderTargetURL(teamId: number) {
        return RAID_TEAM_API_BASE + "/" + teamId + "/rendering/target";
    }

    public static makeGetNewsPostURL() {
        return NEWS_API_BASE;
    }

}