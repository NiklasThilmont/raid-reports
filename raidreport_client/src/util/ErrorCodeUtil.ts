import {Config} from './Config';
export class ErrorCodeUtil {

    public static codeToText(code: string) {
        switch (code) {
            case "ERR_COMMENT_TOO_LONG":
                return "Comment too long. Maximum is " + Config.MAX_COMMENT_CHARACTERS + " characters.";
            case "ERR_COULD_NOT_EXTRACT_DATE":
                return "Could not extract date from filename. Expected pattern: 'yyyyMMdd-HHmmss_bossname.html'";
            case "ERR_COULD_NOT_EXTRACT_BOSS":
                return "Could not extract boss from HTML file.";
            case "FILE_EMPTY":
                return "Uploaded file was empty.";
            case "ERR_UPLOADER_UNKNOWN":
                return "Uploader name was unknown.";
            case "BAD_REPORT":
                return "File was malformed.";
            case "ERR_DUPLICATE":
                return "Report already exists.";
            default:
                return "";
        }
    }
}