import {ReportFile} from '../model/ReportFile';
import {isNullOrUndefined} from 'util';
export class StringUtils {

    public static convertKillStatusToLocalizedString(killStatus: string) {
        if(killStatus == "SUCCESSFUL") {
            return "succeeded ";
        } else if(killStatus == "FAILURE") {
            return "failed ";
        } else {
            return "";
        }
    }

    public static compareString(val1: string, val2: string) {
        return val1.localeCompare(val2);
    }

    public static extractKillStatString(reportFile: ReportFile) {
        let status = isNullOrUndefined(reportFile.encounterKillStatus()) ? "Unknown" : reportFile.encounterKillStatus();
        let time = isNullOrUndefined(reportFile.killTimeMs()) ? "unknown" : StringUtils.millisecondsToTimeString(reportFile.killTimeMs());
        return status + " in " + time;
    }

    public static millisecondsToTimeString(ms: number) {
        let milliseconds = ms % 1000;
        let seconds = Math.floor((ms / 1000) % 60);
        let minutes = Math.floor((ms / (60 * 1000)) % 60);
        return minutes + ":" + seconds;
    }
}