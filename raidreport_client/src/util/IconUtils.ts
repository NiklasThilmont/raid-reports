
declare const ICON_LOCATION: string;

export namespace IconUtils {
    const classToIconMap = {
        "DRUID": "icon_druid_48px.png",
        "SOULBEAST": "icon_soulbeast_48px.png",
        "RANGER": "icon_ranger_48px.png",

        "NECROMANCER":"icon_necro_48px.png",
        "REAPER": "icon_reaper_48px.png",
        "SCOURGE": "icon_scourge_48px.png",

        "WARRIOR": "icon_warrior_48px.png",
        "BERSERKER": "icon_berserker_48px.png",
        "SPELLBREAKER": "icon_spellbreaker_48px.png",

        "MESMER": "icon_mesmer_48px.png",
        "CHRONOMANCER":  "icon_chrono_48px.png",
        "MIRAGE": "icon_mirage_48px.png",

        "ELEMENTALIST": "icon_ele_48px.png",
        "TEMPEST": "icon_tempest_48px.png",
        "WEAVER": "icon_weaver_48px.png",

        "REVENANT":"icon_revenant_48px.png",
        "HERALD": "icon_herald_48px.png",
        "RENEGADE": "icon_renegade_48px.png",

        "GUARDIAN": "icon_guardian_48px.png",
        "DRAGONHUNTER": "icon_dragonhunter_48px.png",
        "FIREBRAND": "icon_firebrand_48px.png",

        "THIEF": "icon_thief_48px.png",
        "DAREDEVIL": "icon_daredevil_48px.png",
        "DEADEYE": "icon_deadeye_48px.png",

        "ENGINEER": "icon_engi_48px.png",
        "HOLOSMITH": "icon_holosmith_48px.png",
        "SCRAPPER":  "icon_scrapper_48px.png",

    };

    export function mapClassToIcon(className: string) {
        let iconLink =  classToIconMap[className];
        if(iconLink) {
            return ICON_LOCATION + "/classIcons/" + iconLink;
        } else {
            console.info("Unmapped classname " + className);
            return window.location.origin + "/raidreport/classIcons/unknown_icon_48px.png"
        }
    }
}