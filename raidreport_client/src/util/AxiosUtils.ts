import {AxiosError} from 'axios';
import {isNullOrUndefined} from 'util';

export class AxiosUtils {
    public static handleAxiosError = (error: AxiosError) => {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.error("An axios error occurred - data", error.response.data);
            console.error("An axios error occurred - status", error.response.status);
            console.error("An axios error occurred - headers", error.response.headers);
        } else {
            // Something happened in setting up the request that triggered an Error
            console.error('Error', error.message);
        }
        console.error(error.config);
        console.error("An axios error occurred", error);
    };

    public static tryGetErrorData = (error: AxiosError) => {
        if(!isNullOrUndefined(error) &&
            !isNullOrUndefined(error.response) &&
            !isNullOrUndefined(error.response.data)) {
            return error.response.data;
        }
        return null;
    }

}