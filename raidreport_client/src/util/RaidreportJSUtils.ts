export namespace RaidreportJSUtils {
    export function nothing() {}
}

export function buildMap(obj: Object) {
    let map = new Map();
    Object.keys(obj).forEach(key => {
        map.set(key, obj[key]);
    });
    return map;
}