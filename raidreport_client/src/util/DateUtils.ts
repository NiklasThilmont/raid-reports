import DateTimeFormatOptions = Intl.DateTimeFormatOptions;
export class DateUtils {
    private static readonly locale: string = navigator.language;
    private static readonly formatDayMonthOpens: DateTimeFormatOptions = {
        day: "2-digit",
        month: "long",
        weekday: "long"
    };
    private static readonly formatBuildDateOptions: DateTimeFormatOptions = {
        year: "numeric",
        day: "2-digit",
        month: "2-digit",
        hour: "2-digit",
        minute: "2-digit"
    };

    private static readonly formatDayAndMonth: Intl.DateTimeFormat = new Intl.DateTimeFormat(DateUtils.locale, DateUtils.formatDayMonthOpens)
    private static readonly formatBuildDate: Intl.DateTimeFormat = new Intl.DateTimeFormat(DateUtils.locale, DateUtils.formatBuildDateOptions)



    public static formatToDayAndMonth(date: Date) {
        if (date == null) {
            return "";
        }
        return DateUtils.formatDayAndMonth.format(date);
    }

    public static formatToBuildDate(date: Date) {
        if (date == null) {
            return "";
        }
        return DateUtils.formatBuildDate.format(date);
    }

    public static sameDay(d1: Date, d2: Date) {
        if (d1 == null) {
            return false;
        }
        if (d2 == null) {
            return false;
        }
        return d1.toDateString() == d2.toDateString();
    }

    public static differenceInDays(first: Date, second: Date) {
        return Math.round((second.getTime() - first.getTime())/(1000*60*60*24));
    }

    public static compare(d1: Date, d2: Date) {
        if(d1 == null) {
            return -1;
        }
        if(d2 == null) {
            return -1;
        }
        return d1.getTime() - d2.getTime()
    }
}