import {RaidTeamStore} from '../../model/team/RaidTeamStore';
import {AbstractAction} from '../app/AppActions';
import {isNullOrUndefined} from 'util';
import {ActionType} from '../ActionType';
import {
    ReplaceRaidTeamAction, ReplaceRaidTeamsAction, ReplaceRenderTargetsAction, SetCreateTeamStatusAction,
    SetPerformanceDataAction
} from './RaidTeamActionCreator';
import {RaidTeam} from '../../model/team/RaidTeam';
import * as Immutable from 'immutable';
import {BooleanValueAction, NumberValueAction} from '../user/UserActions';

export namespace RaidTeamReducer {
    export function reduce(store: RaidTeamStore, action: AbstractAction) {
        if(isNullOrUndefined(store)) {
            return RaidTeamStore.empty();
        }
        switch (action.type) {
            case ActionType.ReplaceRaidTeams: {
                const act = action as ReplaceRaidTeamsAction;
                const raidTeams = act.teams.map(RaidTeam.of);
                return store.raidTeams(Immutable.List<RaidTeam>(raidTeams))
            }
            case ActionType.ReplaceRaidTeam: {
                const act = action as ReplaceRaidTeamAction;
                const raidTeam = RaidTeam.of(act.team);
                let raidTeams = store.raidTeams();
                raidTeams = Immutable.List<RaidTeam>(raidTeams.map(team => {
                    if(team.id() == raidTeam.id()) {
                        return raidTeam;
                    }
                    return team;
                }));
                return store.raidTeams(raidTeams);
            }
            case ActionType.SetCurrentTeam: {
                const act = action as NumberValueAction;
                return store.currentTeamId(act.value);
            }
            case ActionType.SetManagementDialogOpen: {
                const act = action as BooleanValueAction;
                return store.manageDialogOpen(act.value);
            }
            case ActionType.SetCreateTeamStatus: {
                const act = action as SetCreateTeamStatusAction;
                return store.createTeamStatus(act.status);
            }
            case ActionType.SetPerformanceData: {
                const act = action as SetPerformanceDataAction;
                return store.performanceData(act.performanceData);
            }
            case ActionType.ReplaceRenderTargets: {
                const act = action as ReplaceRenderTargetsAction;
                return store.availableRenderTargets(act.renderTargets);
            }
        }
        return store;
    }
}