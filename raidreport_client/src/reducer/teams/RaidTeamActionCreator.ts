import * as redux from 'redux';
import {AppState} from '../../model/AppState';
import axios, {AxiosRequestConfig, AxiosError} from 'axios';
import {Config} from '../../util/Config';
import {ActionType} from '../ActionType';
import {AbstractAction} from '../app/AppActions';
import {IRaidTeam, RaidTeam} from '../../model/team/RaidTeam';
import {AxiosUtils} from '../../util/AxiosUtils';
import {BooleanValueAction, NumberValueAction} from '../user/UserActions';
import {AppActionCreator} from '../app/AppActionCreator';
import {UserActionCreator} from '../user/UserActionCreator';
import {CreateTeamStatus} from '../../model/team/CreateTeamStatus';
import {isNullOrUndefined} from 'util';
import {ReportFile} from '../../model/ReportFile';
import {ITeamPerformanceData} from '../../model/team/performance/TeamPerformanceData';
import {IRenderTarget} from '../../model/team/RenderTarget';

export interface ReplaceRaidTeamsAction extends AbstractAction{
    type: ActionType.ReplaceRaidTeams;
    teams: Array<IRaidTeam>;
}

export interface SetCreateTeamStatusAction extends AbstractAction {
    status: CreateTeamStatus;
}

export interface ReplaceRaidTeamAction extends AbstractAction {
    team: IRaidTeam;
}

export interface SetPerformanceDataAction extends AbstractAction {
    performanceData: ITeamPerformanceData;
}

export interface ReplaceRenderTargetsAction extends AbstractAction {
    renderTargets: Array<IRenderTarget>;
}

export namespace RaidTeamActionCreator {

    import AsyncRetrieveAllUsers = UserActionCreator.AsyncRetrieveAllUsers;
    export function ReplaceRaidTeams(teams: Array<IRaidTeam>): ReplaceRaidTeamsAction {
        return {
            type: ActionType.ReplaceRaidTeams,
            teams: teams
        };
    }

    export function SetCurrentTeam(id: number): NumberValueAction {
        return {
            type: ActionType.SetCurrentTeam,
            value: id
        };
    }

    export function SetManagementDialogOpen(open: boolean): BooleanValueAction {
        return {
            type: ActionType.SetManagementDialogOpen,
            value: open
        };
    }

    export function SetCreateTeamStatus(status: CreateTeamStatus): SetCreateTeamStatusAction {
        return {
            type: ActionType.SetCreateTeamStatus,
            status: status
        };
    }

    export function ReplaceRaidTeam(team: IRaidTeam): ReplaceRaidTeamAction {
        return {
            type: ActionType.ReplaceRaidTeam,
            team: team
        }
    }

    export function SetPerformanceData(data: ITeamPerformanceData): SetPerformanceDataAction {
        return {
            type: ActionType.SetPerformanceData,
            performanceData: data
        }
    }

    export function ReplaceRenderTargets(targets: Array<IRenderTarget>): ReplaceRenderTargetsAction {
        return {
            type: ActionType.ReplaceRenderTargets,
            renderTargets: targets
        }
    }

    export function AsyncSelectTeam(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            console.debug('Selecting Team id: ', teamId);
            dispatch(SetCurrentTeam(teamId));
            dispatch(AppActionCreator.AsyncRequestAllReportFiles(teamId));
        };
    }

    export function AsyncLoadAllTeams() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.get(Config.GetMyRaidTeamsString, config)
                .then(response => dispatch(ReplaceRaidTeams(response.data)))
                .catch(error => AxiosUtils.handleAxiosError(error));
        };
    }

    export function AsyncAcceptInvite(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.patch(Config.makeInviteString(teamId), null, config)
                .then(response => {
                    dispatch(AsyncLoadAllTeams());
                    AppActionCreator.showSuccess("Invite successfully accepted.");
                })
                .catch((error: AxiosError) => {
                    AxiosUtils.handleAxiosError(error);
                    AppActionCreator.showError("Could not accept invite.");
                });
        };
    }

    export function AsyncRejectInvite(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeInviteString(teamId), config)
                .then(response => {
                    dispatch(AsyncLoadAllTeams());
                    AppActionCreator.showSuccess("Invite successfully accepted.");
                })
                .catch((error: AxiosError) => {
                    AxiosUtils.handleAxiosError(error);
                    AppActionCreator.showError("Could not accept invite.");
                });
        };
    }

    export function AsyncCreateTeam(teamName: string) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            let data = {
                teamName: teamName
            };
            dispatch(SetCreateTeamStatus(CreateTeamStatus.CREATION_PENDING));
            axios.post(Config.RaidTeamBaseString, data, config)
                .then(response => {
                    dispatch(SetCreateTeamStatus(CreateTeamStatus.CREATION_SUCCESS));
                    dispatch(AsyncLoadAllTeams());
                })
                .catch(error => {
                    dispatch(SetCreateTeamStatus(CreateTeamStatus.CREATION_FAILED));
                    AxiosUtils.handleAxiosError(error);
                });
        };
    }

    export function AsyncLeaveTeam(teamId: number, history: any) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeLeaveTeamString(teamId), config).then(response => {
                AppActionCreator.showSuccess("Team successfully left.");
                dispatch(RaidTeamActionCreator.AsyncLoadAllTeams());
                history.push("/raidreport");
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                let errorData = AxiosUtils.tryGetErrorData(error);
                let text = "Could not leave team: ";
                if(errorData != null) {
                    text +=  errorData.error;
                } else if(!isNullOrUndefined(error)) {
                    text += error.code;
                } else {
                    text += "Unknown";
                }
                AppActionCreator.showError(text);
            })
        }
    }

    export function AsyncShareTeam(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.patch(Config.makeShareTeamString(teamId), null, config).then((response) => {
                dispatch(RaidTeamActionCreator.AsyncLoadAllTeams());
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                let errorData = AxiosUtils.tryGetErrorData(error);
                let text = "Could not share team: ";
                if(errorData != null) {
                    text +=  errorData.error;
                } else if(!isNullOrUndefined(error)) {
                    text += error.code;
                } else {
                    text += "Unknown";
                }
                AppActionCreator.showError(text);
            })
        }
    }

    export function AsyncRevokeShareTeam(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeShareTeamString(teamId), config).then((response) => {
                dispatch(RaidTeamActionCreator.AsyncLoadAllTeams());
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                let errorData = AxiosUtils.tryGetErrorData(error);
                let text = "Could not unshare team: ";
                if(errorData != null) {
                    text +=  errorData.error;
                } else if(!isNullOrUndefined(error)) {
                    text += error.code;
                } else {
                    text += "Unknown";
                }
                AppActionCreator.showError(text);
            })
        }
    }

    export function AsyncRevokePermission(teamId: number, userId: number, permission: string) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makePermissionString(teamId, userId, permission), config).then(response => {
                dispatch(RaidTeamActionCreator.ReplaceRaidTeam(response.data));
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                if(error && error.response && error.response.data) {
                    AppActionCreator.showError("Could not revoke permission: " + error.response.data.error);
                } else {
                    AppActionCreator.showError("Could not revoke permission due to unknown reason");
                }

            })
        }
    }

    export function AsyncGrantPermission(teamId: number, userId: number, permission: string) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.post(Config.makePermissionString(teamId, userId, permission), null, config).then(response => {
                dispatch(RaidTeamActionCreator.ReplaceRaidTeam(response.data));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not grant permission: " + permission);
            })
        }
    }

    export function AsyncShareReport(teamId: number, reportId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.post(Config.makeShareReportString(teamId, reportId), null, config).then(response => {
                let data: IRaidTeam = response.data;
                dispatch(RaidTeamActionCreator.ReplaceRaidTeam(data));
                dispatch(AppActionCreator.ReplaceReportFiles(data.reports.map(ReportFile.fromAPI)));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not share report: " + reportId);
            })
        }
    }

    export function AsyncUnShareReport(teamId: number, reportId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeShareReportString(teamId, reportId), config).then(response => {
                let data: IRaidTeam = response.data;
                dispatch(RaidTeamActionCreator.ReplaceRaidTeam(data));
                dispatch(AppActionCreator.ReplaceReportFiles(data.reports.map(ReportFile.fromAPI)));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not share report: " + reportId);
            })
        }
    }

    export function AsyncFetchPerformanceData(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config = getState().appStore.makeAuthConfig();
            axios.post(Config.makeGetPerformanceAPIUrl(teamId), null, config).then(response => {
                dispatch(SetPerformanceData(response.data));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not retrieve performance data.");
            })
        }
    }

    export function AsyncFetchRenderTargets() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            axios.get(Config.makeGetRenderTargetsURL()).then(response => {
                dispatch(ReplaceRenderTargets(response.data));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not retrieve render targets.");
            })
        }
    }

    export function AsyncChangeRenderTarget(teamId: number, renderTarget: IRenderTarget) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config = getState().appStore.makeAuthConfig();
            axios.patch(Config.makePatchRenderTargetURL(teamId), renderTarget, config).then(response => {
                dispatch(ReplaceRaidTeam(response.data));
                AppActionCreator.showSuccess("Render target changed to " + renderTarget.name);
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not change render target:" + (!!error.response ? error.response.data : 'Unknown Error'));
            })
        }
    }
}