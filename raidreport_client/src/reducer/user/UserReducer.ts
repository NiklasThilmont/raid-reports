import {UserStore} from '../../model/user/UserStore';
import {AbstractAction, ChangeStringValueAction} from '../app/AppActions';
import {isNullOrUndefined} from 'util';
import {ActionType} from '../ActionType';
import * as Immutable from 'immutable';
import {User} from '../../model/user/User';
import {
    NumberValueAction, ReplaceUsersAction, SetRegisterRejectedReasonAction,
    SetRegisterUserStatusAction
} from './UserActions';

export namespace UserReducer {
    export function reduce(store: UserStore, action: AbstractAction) {
        console.debug("UserReducer called with action:", action);
        if(isNullOrUndefined(store)) {
            return UserStore.empty();
        }

        switch (action.type) {
            case ActionType.ReplaceUsers: {
                let act: ReplaceUsersAction = action as ReplaceUsersAction;
                store = store.users(Immutable.List<User>(act.users.map(User.of)));
                return store;
            }
            case ActionType.SetRegisterRejectedReason: {
                let act: SetRegisterRejectedReasonAction = action as SetRegisterRejectedReasonAction;
                store = store.registerRejectReason(act.reason);
                return store;
            }
            case ActionType.SetRegisterUserStatus: {
                let act: SetRegisterUserStatusAction = action as SetRegisterUserStatusAction;
                store = store.registerUserStatus(act.status);
                return store;
            }
            case ActionType.RemoveUser: {
                let act: NumberValueAction = action as NumberValueAction;
                let users = store.users();
                let index = users.findIndex(user => user.id() === act.value);
                users = users.delete(index);
                return store.users(users);
            }
            case ActionType.UnlockUser: {
                let act: NumberValueAction = action as NumberValueAction;
                let users = store.users();
                let index = users.findIndex(user => user.id() === act.value);
                let user = users.get(index);
                user = user.enabled(true);
                users = users.set(index, user);
                return store.users(users);
            }
            case ActionType.SetAPIKey: {
                let act: ChangeStringValueAction = action as ChangeStringValueAction;
                return store.apiKey(act.value);
            }
        }

        return store;
    }
}