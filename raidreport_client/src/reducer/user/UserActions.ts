import {AbstractAction} from '../app/AppActions';
import {IUser} from '../../model/user/User';
import {ActionType} from '../ActionType';
import {RegisterUserStatus} from '../../model/user/RegisterUserStatus';
export interface ReplaceUsersAction extends AbstractAction {
    type: ActionType.ReplaceUsers;
    users: Array<IUser>;
}

export interface NumberValueAction extends AbstractAction {
    value: number;
}

export interface BooleanValueAction extends AbstractAction {
    value: boolean;
}

export interface SetRegisterUserStatusAction extends AbstractAction {
    type: ActionType.SetRegisterUserStatus;
    status: RegisterUserStatus;
}

export interface SetRegisterRejectedReasonAction extends AbstractAction {
    type: ActionType.SetRegisterRejectedReason;
    reason: string;
}