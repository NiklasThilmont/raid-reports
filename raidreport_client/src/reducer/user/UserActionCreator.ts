import * as redux from 'redux';
import {AppState} from '../../model/AppState';
import axios, {AxiosError, AxiosRequestConfig} from 'axios';
import {Config} from '../../util/Config';
import {AxiosUtils} from '../../util/AxiosUtils';
import {IUser} from '../../model/user/User';
import {
    NumberValueAction,
    ReplaceUsersAction,
    SetRegisterRejectedReasonAction,
    SetRegisterUserStatusAction
} from './UserActions';
import {ActionType} from '../ActionType';
import {RegisterUserStatus} from '../../model/user/RegisterUserStatus';
import {AppActionCreator} from '../app/AppActionCreator';
import {ChangeStringValueAction} from '../app/AppActions';

export namespace UserActionCreator {
    export function ReplaceUsers(users: Array<IUser>): ReplaceUsersAction {
        return {
            type: ActionType.ReplaceUsers,
            users: users
        };
    }

    function RemoveUser(userId: number): NumberValueAction {
        return {
            type: ActionType.RemoveUser,
            value: userId
        }
    }

    function UnlockUser(userId: number): NumberValueAction {
        return {
            value: userId,
            type: ActionType.UnlockUser
        }
    }

    export function SetRegisterUserStatus(registerUserStatus: RegisterUserStatus): SetRegisterUserStatusAction {
        return {
            type: ActionType.SetRegisterUserStatus,
            status: registerUserStatus
        };
    }

    export function SetRegisterRejectReason(reason: string): SetRegisterRejectedReasonAction {
        return {
            type: ActionType.SetRegisterRejectedReason,
            reason: reason
        }
    }


    export function SetAPIKey(key: string): ChangeStringValueAction {
        return {
            type: ActionType.SetAPIKey,
            value: key
        }
    }

    export function AsyncRetrieveAllUsers() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            console.debug('Sending request to retrieve users with config:', config);
            axios.get(Config.GetAllUsersApiString, config)
                .then(response => dispatch(ReplaceUsers(response.data)))
                .catch(error => AxiosUtils.handleAxiosError(error));
        };
    }

    export function AsyncResetCreation() {
        return function (dispatch: redux.Dispatch<AppState>) {
            dispatch(SetRegisterUserStatus(RegisterUserStatus.DIALOG_CLOSED));
            dispatch(SetRegisterRejectReason(null));
            dispatch(AppActionCreator.ChangePassword(""));
            dispatch(AppActionCreator.ChangeUsername(""));
        }
    }

    export function AsyncCreateAccount() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let body = {
                username: getState().appStore.username(),
                password: getState().appStore.password()
            };
            dispatch(SetRegisterUserStatus(RegisterUserStatus.CREATION_PENDING));
            axios.post(Config.GetAllUsersApiString + "/", body).then(response => {
                dispatch(SetRegisterUserStatus(RegisterUserStatus.CREATION_FINISHED));
            }).catch((error: AxiosError) => {
                dispatch(SetRegisterUserStatus(RegisterUserStatus.CREATION_FINISHED));
                dispatch(SetRegisterRejectReason(error.response.data.userError));
            });
        };
    }

    export function AsyncDeleteUser(userId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeDeleteUserString(userId), config).then(response => {
                AppActionCreator.showSuccess("User deleted.");
                dispatch(RemoveUser(userId));
            }).catch((error: AxiosError) => {
                AppActionCreator.showError("User could not be deleted. Reason: " + error.code)
                AxiosUtils.handleAxiosError(error);
            })
        }
    }

    export function AsyncUnlockUser(userId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            console.debug("Request config for unlock user", config);
            axios.patch(Config.makeEnableUserString(userId), null, config).then(response => {
                AppActionCreator.showSuccess("User Successfully activate.");
                dispatch(UnlockUser(userId));
            }).catch((error: AxiosError) => {
                AppActionCreator.showError("User could not be activated. Reason: " + error.code)
                AxiosUtils.handleAxiosError(error);
            })
        }
    }

    export function AsyncRetrieveKey() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = getState().appStore.makeAuthConfig();
            axios.get(Config.makeGetAPIKeyUrl(),  config).then(response => {
                dispatch(SetAPIKey(response.data.key))
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
            })
        }
    }

    export function AsyncSaveAPIKey() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = getState().appStore.makeAuthConfig();
            let data = {
                key: getState().users.apiKey()
            };
            axios.post(Config.makeSaveAPIKeyUrl(), data, config).then(response => {
                dispatch(SetAPIKey(response.data.key));
                AppActionCreator.showSuccess("Key Successfully saved.");
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not store key.");
            })
        }
    }

    export function AsyncDeleteAPIKey() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = getState().appStore.makeAuthConfig();
            axios.delete(Config.makeSaveAPIKeyUrl(), config).then(response => {
                dispatch(SetAPIKey(""));
                AppActionCreator.showSuccess("Key Successfully deleted.");
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not delete key");
            })
        }
    }
}