import {AppStore} from '../../model/AppStore';
import {
    AbstractAction,
    AddFileToQueueAction,
    BeginFileUploadAction,
    ChangeStringValueAction,
    ReplaceReportFilesAction, SetCurrentNodeAction,
    SetFileTreeRootAction,
    SortReportFilesAction,
    SucceedLoginAction, UpdateBossAction,
    UpdateFileUploadStatusAction
} from './AppActions';
import * as Immutable from 'immutable';
import {isNullOrUndefined} from 'util';
import {ReportFile} from '../../model/ReportFile';
import {ActionType} from '../ActionType';
import {Config} from '../../util/Config';
import * as Cookies from 'js-cookie';
import {UploadingFile} from '../../model/UploadingFile';
import {FileUploadStatus} from '../../model/FileUploadStatus';
import {QueuebleFile} from '../../model/QueueableFile';
import {ReportFileLeaf, ReportFileTree} from '../../model/ReportFileTree';
import {AppActionCreator} from './AppActionCreator';
import {match} from 'react-router';
import {RaidBoss} from '../../model/RaidBoss';
export class AppReducer {

    public static ReplaceReportFiles(store: AppStore, action: ReplaceReportFilesAction): AppStore {
        let files = Immutable.List<ReportFile>(action.files);
        let root = ReportFileTree.fromFileList(files);
        let sortAction = AppActionCreator.SortReportFiles(true, store.fileTreeSortField());
        let res = AppReducer.SortReportFiles(store.reportFiles(files).fileTreeRoot(root), sortAction);
        let currentNode = res.currentNode();
        if(currentNode && currentNode.type === "leaf") {
            let reportId = (currentNode as ReportFileLeaf).reportFile.id();
            let matchingReport = action.files.find((value, index, obj) => value.id() === reportId);
            if(matchingReport) {
                (currentNode as ReportFileLeaf).reportFile = matchingReport;
                res = res.currentNode(currentNode);
            } else {
                res = res.currentNode(null);
            }
        }
        return res;
    }

    public static ChangeUsername(store: AppStore, action: ChangeStringValueAction): AppStore {
        return store.username(action.value).loginErrorText(null);
    }

    public static ChangePassword(store: AppStore, action: ChangeStringValueAction): AppStore {
        return store.password(action.value).loginErrorText(null);
    }

    public static LogOutUser(store: AppStore): AppStore {
        localStorage.removeItem(Config.PASSWORD_COOKIE_NAME);
        localStorage.removeItem(Config.USERNAME_COOKIE_NAME);
        return AppStore.empty();
    }

    public static UpdateFileUploadStatus(store: AppStore, action: UpdateFileUploadStatusAction): AppStore {
        let uFile = store.uploadingFile().uploadStatus(action.newStatus);
        return store.uploadingFile(uFile);
    }

    public static BeginFileUpload(store: AppStore, action: BeginFileUploadAction): AppStore {
        let uFile = UploadingFile.of(action.file);
        return store.uploadingFile(uFile).fileUploadStatus(FileUploadStatus.UPLOADING);
    }

    public static AddFileToQueue(store: AppStore, action: AddFileToQueueAction): AppStore {
        let queueableFile = QueuebleFile.of(action.file);
        return store.filesToUpload(store.filesToUpload().push(queueableFile));
    }

    public static SetFileTreeRoot(store: AppStore, action: SetFileTreeRootAction): AppStore {
        return store.fileTreeRoot(action.root);
    }

    public static SortReportFiles(store: AppStore, action: SortReportFilesAction): AppStore {
        let root = store.fileTreeRoot().flatCopy();
        // This sorts all the children (raid days)
        root.sortChildrenByDate();
        if(action.field === "wing_boss") {
            root.sortFileLeafsByBoss(action.ascending);
        } else if(action.field === "date") {
           root.sortFileLeafsByKillTimePoint(action.ascending);
        } else if(action.field === "name") {
            root.sortFileLeafsByName(action.ascending);
        }
        return store.fileTreeRoot(root).fileTreeSortField(action.field);
    }

    public static SucceedLogin(store: AppStore, action: SucceedLoginAction): AppStore {
        return store.userAuthorities(Immutable.List<string>(action.roles)).isLoggedIn(true);
    }


    public static Reduce(store: AppStore, action: AbstractAction): AppStore {
        console.debug("Reducer called with action type " + ActionType[action.type]);
        if(isNullOrUndefined(store)) return AppStore.empty();
        switch (action.type) {
            case ActionType.ReplaceReportFiles:
                return AppReducer.ReplaceReportFiles(store, action as ReplaceReportFilesAction);
            case ActionType.ChangePassword:
                return AppReducer.ChangePassword(store, action as ChangeStringValueAction);
            case ActionType.ChangeUsername:
                return AppReducer.ChangeUsername(store, action as ChangeStringValueAction);
            case ActionType.FailLogin:
                return store.isLoggedIn(false).loginErrorText((<ChangeStringValueAction>action).value);
            case ActionType.SucceedLogin:
                return AppReducer.SucceedLogin(store, action as SucceedLoginAction);
            case ActionType.LogOut:
                return AppReducer.LogOutUser(store);
            case ActionType.BeginFileUpload:
                return AppReducer.BeginFileUpload(store, action as BeginFileUploadAction);
            case ActionType.UpdateFileUploadStatus:
                return AppReducer.UpdateFileUploadStatus(store, action as UpdateFileUploadStatusAction);
            case ActionType.EndFileUpload:
                return store.fileUploadStatus(FileUploadStatus.NONE).uploadingFile(null);
            case ActionType.AddFileToQueue:
                return AppReducer.AddFileToQueue(store, action as AddFileToQueueAction);
            case ActionType.RemoveFirstFromQueue:
                return store.filesToUpload(store.filesToUpload().shift());
            case ActionType.SortReportFiles:
                return AppReducer.SortReportFiles(store, action as SortReportFilesAction);
            case ActionType.SetFileTreeRoot:
                return AppReducer.SetFileTreeRoot(store, action as SetFileTreeRootAction);
            case ActionType.SetCurrentNode: {
                let act: SetCurrentNodeAction = action as SetCurrentNodeAction;
                return store.currentNode(act.currentNode);
            }
            case ActionType.UpdateBoss: {
                let act: UpdateBossAction = action as UpdateBossAction;
                let bosses = store.bosses();
                bosses = store.bosses().set(act.boss.id(), act.boss);
                return store.bosses(bosses);
            }
            case ActionType.ClearAllBosses: {
                return store.bosses(Immutable.Map<number, RaidBoss>());
            }
            default:
                return store;
        }
    }
}