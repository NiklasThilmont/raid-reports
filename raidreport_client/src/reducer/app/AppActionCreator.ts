import * as redux from 'redux';
import {AppState} from '../../model/AppState';
import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';
import {Config} from '../../util/Config';
import {AxiosUtils} from '../../util/AxiosUtils';
import {APIReportFile, ReportFile} from '../../model/ReportFile';
import {
    AbstractAction, AddFileToQueueAction,
    BeginFileUploadAction,
    ChangeStringValueAction,
    ReplaceReportFilesAction, SetCurrentNodeAction, SetFileTreeRootAction, SortReportFilesAction, SucceedLoginAction,
    UpdateBossAction,
    UpdateFileUploadStatusAction
} from './AppActions';
import {ActionType} from '../ActionType';
import * as Cookies from 'js-cookie';
import {ErrorCodeUtil} from '../../util/ErrorCodeUtil';
import {isNull, isNullOrUndefined} from 'util';
import {FileUploadStatus} from '../../model/FileUploadStatus';
import {ReportFileTree, TreeNode} from '../../model/ReportFileTree';
import {RaidTeamActionCreator} from '../teams/RaidTeamActionCreator';
import {UserActionCreator} from '../user/UserActionCreator';
import {IRaidBoss, RaidBoss} from '../../model/RaidBoss';





export class AppActionCreator {


    /**
     * Used to dipslay messages.
     * @type {any}
     */
    public static APP_MESSAGE_CONTAINER: any = null;


    public static showError = (text: string) => {
        AppActionCreator.APP_MESSAGE_CONTAINER.show(text, {
            time: 0,
            type: 'error',
        })
    };

    public static showSuccess = (text: string) => {
        AppActionCreator.APP_MESSAGE_CONTAINER.show(text, {
            time: 10000,
            type: 'success',
        })
    };

    public static ReplaceReportFiles(files: Array<ReportFile>): ReplaceReportFilesAction {
        return {
            type: ActionType.ReplaceReportFiles,
            files: files
        }
    }

    public static ChangeUsername(username: string): ChangeStringValueAction {
        return {
            type: ActionType.ChangeUsername,
            value: username
        }
    }

    public static ChangePassword(username: string): ChangeStringValueAction {
        return {
            type: ActionType.ChangePassword,
            value: username
        }
    }

    public static FailLogin(message?: string): ChangeStringValueAction {
        return {
            type: ActionType.FailLogin,
            value: message
        }
    }

    public static SetCurrentNode(currentNode: TreeNode): SetCurrentNodeAction {
        return {
            type: ActionType.SetCurrentNode,
            currentNode: currentNode
        }
    }

    /**
     * Invokes clearing of all login info. Includes username, password, stored cookies and the login status.
     * @returns {{type: ActionType}}
     * @constructor
     */
    private static LogOutUser(): AbstractAction {
        return {
            type: ActionType.LogOut

        }
    }

    public static SucceedLogin(roles: Array<string>): SucceedLoginAction {
        return {
            type: ActionType.SucceedLogin,
            roles: roles
        }
    }

    public static BeginFileUpload(file: File): BeginFileUploadAction {
        return {
            type: ActionType.BeginFileUpload,
            file: file
        }
    }

    public static EndFileUpload(): AbstractAction {
        return {
            type: ActionType.EndFileUpload
        }
    }

    public static UpdateFileUploadStatus(newStatus: number): UpdateFileUploadStatusAction {
        return {
            type: ActionType.UpdateFileUploadStatus,
            newStatus: newStatus
        }
    }

    public static AddFileToQueue(file: File): AddFileToQueueAction {
        return {
            type: ActionType.AddFileToQueue,
            file: file
        }
    }

    public static RemoveFirstFileFromQueue(): AbstractAction {
        return {
            type: ActionType.RemoveFirstFromQueue
        }
    }

    public static SortReportFiles(ascending: boolean, field: "name" | "date" | "wing_boss"): SortReportFilesAction {
        return {
            type: ActionType.SortReportFiles,
            ascending: ascending,
            field: field
        }
    }

    public static SetFileTreeRoot(root: ReportFileTree): SetFileTreeRootAction {
        return {
            type: ActionType.SetFileTreeRoot,
            root: root
        }
    }

    public static UpdateBoss(boss: RaidBoss): UpdateBossAction {
        return {
            type: ActionType.UpdateBoss,
            boss: boss
        }
    }

    public static ClearAllBosses(): AbstractAction {
        return {
            type: ActionType.ClearAllBosses
        }
    }

    public static AsyncLogOutUser() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            dispatch(AppActionCreator.LogOutUser());
            dispatch(RaidTeamActionCreator.SetCurrentTeam(null));
            dispatch(RaidTeamActionCreator.ReplaceRaidTeams([]));
            dispatch(UserActionCreator.ReplaceUsers([]));
        }
    }

    public static AsyncDeleteReportFile(teamId: number, reportId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.delete(Config.makeDeleteReportString(teamId, reportId), config).then((response: AxiosResponse) => {
                dispatch(AppActionCreator.AsyncRequestAllReportFiles(teamId));
                AppActionCreator.showSuccess("File deleted.")
            }).catch((err: AxiosError) => {
                AxiosUtils.handleAxiosError(err);
                if(err && err.response && err.response.data) {
                    AppActionCreator.showError("Could not delete file: " + err.response.data.error)
                } else {
                    AppActionCreator.showError("Could not delete file.")
                }
            });
        }
    }

    public static AsyncRequestAllReportFiles(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: getState().appStore.username(),
                    password: getState().appStore.password()
                }
            };
            axios.get(Config.makeGetAllReportsString(teamId), config).then((response: AxiosResponse) => {
                let apiFiles: Array<APIReportFile> = response.data;
                let files = apiFiles.map(apiFile => ReportFile.fromAPI(apiFile));
                dispatch(AppActionCreator.ReplaceReportFiles(files));
            }).catch(AxiosUtils.handleAxiosError);
        }
    }

    public static AsyncLoginUser(username: string, password: string, store: boolean) {
        return function (dispatch: redux.Dispatch<AppState>) {
            let config: AxiosRequestConfig = {
                auth: {
                    username: username,
                    password: password
                }
            };
            axios.get(Config.validateLoginApiString, config).then((response: AxiosResponse) => {
                console.info('Login success');
                console.debug("login_response", response);
                dispatch(AppActionCreator.SucceedLogin(response.data));
                dispatch(AppActionCreator.ChangeUsername(username));
                dispatch(AppActionCreator.ChangePassword(password));
                dispatch(RaidTeamActionCreator.AsyncLoadAllTeams());
                if(store) {
                    localStorage.setItem(Config.USERNAME_COOKIE_NAME, username);
                    localStorage.setItem(Config.PASSWORD_COOKIE_NAME, password);
                }
            }).catch((error: AxiosError) => {
                AxiosUtils.handleAxiosError(error);
                localStorage.removeItem(Config.USERNAME_COOKIE_NAME);
                localStorage.removeItem(Config.PASSWORD_COOKIE_NAME);
                let msg: string = "Login failed";
                if(error && error.response) {
                    if(error.response.status === 401) {
                        msg += ": Invalid credentials";
                    } else if(error.response.status === 403) {
                        msg += ": Account inactive.";
                    } else if(error.response.status === 500) {
                        msg += ": Server error.";
                    } else {
                        msg += ": " + error.response.status;
                    }
                }
                dispatch(AppActionCreator.FailLogin(msg));
            });
        }
    }

    public static AsyncSucceedFileUpload(teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            dispatch(AppActionCreator.EndFileUpload());
            if(getState().appStore.filesToUpload().count() > 0) {
                let fileToUpload = getState().appStore.filesToUpload().first();
                dispatch(AppActionCreator.AsyncUploadFile(fileToUpload.file(), teamId));
                dispatch(AppActionCreator.RemoveFirstFileFromQueue());
            }
        }
    }

    public static AsyncFailFileUpload(error: AxiosError) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            if (error.response) {
                if(!isNullOrUndefined(error.response.data) && !isNullOrUndefined(error.response.data.errorCode)) {
                    let text = ErrorCodeUtil.codeToText(error.response.data.errorCode);
                    AppActionCreator.showError(text);
                }
                AppActionCreator.showError(error.response.status + ": " + error);
            } else {
                // Something happened in setting up the request that triggered an Error
                AppActionCreator.showError(JSON.stringify(error.message));
            }
            dispatch(AppActionCreator.EndFileUpload());
        }
    }

    public static AsyncUploadFile(file: File, teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            dispatch(AppActionCreator.BeginFileUpload(file));
            let username = getState().appStore.username();
            let password = getState().appStore.password();
            console.info('Uploading file');
            let data = new FormData();
            data.append('file', file, file.name);
            data.append('user', username);
            let config: AxiosRequestConfig = {
                headers: {'content-type': 'multipart/form-data'},
                auth: {
                    username: username,
                    password: password
                },
                onUploadProgress: (event) => {
                    dispatch(AppActionCreator.UpdateFileUploadStatus((event.loaded * 100) / event.total))
                }
            };
            axios.post(Config.makeFileUploadString(teamId), data, config).then((response: AxiosResponse) => {
                dispatch(AppActionCreator.AsyncSucceedFileUpload(teamId));
                dispatch(AppActionCreator.AsyncRequestAllReportFiles(teamId));
                AppActionCreator.showSuccess("File Upload Successful");
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                dispatch(AppActionCreator.AsyncFailFileUpload(error));
            });
        }
    }

    public static AsyncAddFileToQueue(file: File, teamId: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            console.log("Adding file to queue.");
            console.log("File upload status: ", FileUploadStatus[getState().appStore.fileUploadStatus()]);
            if(getState().appStore.fileUploadStatus() == FileUploadStatus.UPLOADING) {
                dispatch(AppActionCreator.AddFileToQueue(file));
            } else {
                dispatch(AppActionCreator.AsyncUploadFile(file, teamId));
            }

        }
    }

    public static AsyncRetrieveAllBosses() {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config = getState().appStore.makeAuthConfig();
            axios.get(Config.bossAPIUrl(), config).then(response => {
                dispatch(AppActionCreator.ClearAllBosses());
                let data: Array<IRaidBoss> = response.data;
                data.forEach(value => {
                    dispatch(AppActionCreator.UpdateBoss(RaidBoss.fromAPI(value)));
                })
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not retrieve bosses.");
            })
        }
    }

    public static AsyncDeleteBoss(id: number) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config = getState().appStore.makeAuthConfig();
            axios.delete(Config.bossAPIUrl() + "/" + id,  config).then(response => {
                dispatch(AppActionCreator.AsyncRetrieveAllBosses());
                AppActionCreator.showSuccess("Boss successfully deleted.");
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
                dispatch(AppActionCreator.AsyncRetrieveAllBosses());
                AppActionCreator.showError("Could not delete boss.");
            })
        }
    }

    public static AsyncSaveBoss(boss: RaidBoss) {
        return function (dispatch: redux.Dispatch<AppState>, getState: () => AppState) {
            let config = getState().appStore.makeAuthConfig();
            axios.patch(Config.bossAPIUrl(), boss.toApi(), config).then(response => {
                dispatch(AppActionCreator.AsyncRetrieveAllBosses());
                AppActionCreator.showSuccess("Boss successfully updated.");
            }).catch(error => {
                dispatch(AppActionCreator.AsyncRetrieveAllBosses());
                AxiosUtils.handleAxiosError(error);
                AppActionCreator.showError("Could not update boss.");
            })
        }
    }
}