import {ActionType} from '../ActionType';
import {ReportFile} from '../../model/ReportFile';
import {ReportFileTree, TreeNode} from '../../model/ReportFileTree';
import {RaidBoss} from '../../model/RaidBoss';

/**
 * All actions that are going to be dispatches to the redux store are collected is in this file.
 * Each action must extend the AbstractAction interface, and as such, have a type.
 *
 * Always place new actions for the {@link AppReducer} here.
 */
//

export interface AbstractAction {
    type: ActionType;
}

export interface ReplaceReportFilesAction extends AbstractAction{
    type: ActionType.ReplaceReportFiles,
    files: Array<ReportFile>
}

export interface ChangeStringValueAction extends AbstractAction{
    value: string;
}

export interface BeginFileUploadAction extends AbstractAction {
    type: ActionType.BeginFileUpload;
    file: File;
}

export interface UpdateFileUploadStatusAction extends AbstractAction {
    type: ActionType.UpdateFileUploadStatus;
    newStatus: number;
}

export interface AddFileToQueueAction extends AbstractAction {
    type: ActionType.AddFileToQueue;
    file: File;
}

export interface SortReportFilesAction extends AbstractAction {
    type: ActionType.SortReportFiles;
    ascending: boolean;
    field: 'date' | 'name' | 'wing_boss';
}

export interface SetCurrentNodeAction extends AbstractAction {
    currentNode: TreeNode;
}

/**
 * Replaces the tree root in the AppStore with the given, new root element.
 */
export interface SetFileTreeRootAction extends AbstractAction {
    type: ActionType.SetFileTreeRoot;
    root: ReportFileTree
}

export interface SucceedLoginAction extends AbstractAction {
    type: ActionType.SucceedLogin;
    roles: Array<string>;
}

export interface UpdateBossAction extends AbstractAction {
    boss: RaidBoss;
}
