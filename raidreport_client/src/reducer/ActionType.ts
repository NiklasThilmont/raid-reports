export enum ActionType {
    ReplaceReportFiles,
    SetCurrentNode,
    ChangeUsername,
    ChangePassword,
    FailLogin,
    SucceedLogin,
    LogOut,
    UpdateBoss,
    ClearAllBosses,

    /* File upload */
    BeginFileUpload,
    UpdateFileUploadStatus,
    EndFileUpload,
    AddFileToQueue,
    RemoveFirstFromQueue,
    /* File Sorting */
    SortReportFiles,
    SetFileTreeRoot,
    /* User Management */
    ReplaceUsers,
    SetRegisterUserStatus,
    SetRegisterRejectedReason,
    RemoveUser,
    UnlockUser,
    /* Team Management */
    ReplaceRaidTeams,
    ReplaceRaidTeam,
    SetCurrentTeam,
    SetManagementDialogOpen,
    SetCreateTeamStatus,
    SetInviteUserDialogStatus,
    ReplaceRenderTargets,
    /* Public/Share content */
    ReplaceSharedTeam,
    SetAPIKey,
    SetPerformanceData

}