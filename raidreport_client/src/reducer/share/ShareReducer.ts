import {AbstractAction} from '../app/AppActions';
import {PublicStore} from '../../model/share/PublicStore';
import {isNullOrUndefined} from 'util';
import {RaidTeam} from '../../model/team/RaidTeam';
import {ActionType} from '../ActionType';
import {ReplaceSharedTeamAction} from './ShareActionCreator';


export namespace ShareReducer {
    export function reduce(store: PublicStore, action: AbstractAction): PublicStore {
        if(isNullOrUndefined(store)) {
            return PublicStore.empty();
        }
        switch (action.type) {
            case ActionType.ReplaceSharedTeam: {
                let act: ReplaceSharedTeamAction = action as ReplaceSharedTeamAction;
                return store.sharedTeam(act.team);
            }
        }
        return store;
    }
}