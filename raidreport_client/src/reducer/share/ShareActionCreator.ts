import {RaidTeam} from '../../model/team/RaidTeam';
import {AbstractAction} from '../app/AppActions';
import {ActionType} from '../ActionType';
import {AppState} from '../../model/AppState';
import * as redux from 'redux';
import axios, {AxiosRequestConfig, AxiosError} from 'axios';
import {Config} from '../../util/Config';
import {AxiosUtils} from '../../util/AxiosUtils';
import {APIReportFile, ReportFile} from '../../model/ReportFile';
import {AppActionCreator} from '../app/AppActionCreator';

export interface ReplaceSharedTeamAction extends AbstractAction {
    team: RaidTeam;
}

export namespace ShareActionCreator {

    export function ReplaceSharedTeam(team: RaidTeam): ReplaceSharedTeamAction {
        return {
            type: ActionType.ReplaceSharedTeam,
            team: team
        };
    }


    export function AsyncLoadSharedTeam(shareId: string) {
        return function (dispatch: redux.Dispatch<AppState>) {
            axios.get(Config.makeGetSharedTeamString(shareId)).then(response => {
                dispatch(ReplaceSharedTeam(RaidTeam.of(response.data)));
                let reportFiles: Array<APIReportFile> = response.data.reports;
                dispatch(AppActionCreator.ReplaceReportFiles(reportFiles.map(ReportFile.fromAPI)));
            }).catch(error => {
                AxiosUtils.handleAxiosError(error);
            });
        };
    }
}

