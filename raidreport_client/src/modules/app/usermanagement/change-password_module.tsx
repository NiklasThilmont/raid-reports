import * as React from 'react';
import {RouteComponentProps, withRouter} from "react-router";
import {Dialog, FlatButton, TextField} from "material-ui";
import axios, {AxiosRequestConfig} from "axios";
import {Config, RouteConfig} from "../../../util/Config";
import {AxiosUtils} from "../../../util/AxiosUtils";


interface ChangePasswordDialogLocalProps extends RouteComponentProps<any>{

}

interface ChangePasswordDialogLocalState {
    status: ChangePasswordStatus;
    password: string;
    confirmedPassword: string;
    passError: string | null;
}

enum ChangePasswordStatus {
    OPEN,
    PENDING,
    SUCCESS,
    REJECT,
}

class ChangePasswordDialogModule extends React.Component<ChangePasswordDialogLocalProps, ChangePasswordDialogLocalState> {

    constructor(props: ChangePasswordDialogLocalProps) {
        super(props);
        this.state = ChangePasswordDialogModule.makeState();
    }

    private static makeState(): ChangePasswordDialogLocalState {
        return {
            confirmedPassword: "",
            password: "",
            status: ChangePasswordStatus.OPEN,
            passError: null
        }
    }

    private determinePassError = (p1: string | null, p2: string | null) => {
        let error;
        if(p1 !== p2) {
            error = "Passwords do not match.";
        } else {
            error = null;
        }
        this.setState({
            passError: error
        })
    };

    private changeNormalPassword = (event: any, val: string) => {
        this.setState({
            password: val
        });
        this.determinePassError(val, this.state.confirmedPassword);
    };

    private changeConfirmPassword = (event: any, val: string) => {
        this.setState({
            confirmedPassword: val
        });
        this.determinePassError(val, this.state.password);
    };

    private handleChangePassword = () => {
        let request = {
            "password": this.state.password,
            "resetId": this.props.match.params.resetId
        };
        axios.patch(Config.makeChangePasswordAPIUrl(), request).then(response => {
            this.setState({
                status: ChangePasswordStatus.SUCCESS
            })
        }).catch(error => {
            AxiosUtils.handleAxiosError(error);
            this.setState({
                status: ChangePasswordStatus.REJECT
            })
        })
    };

    private navigateToStart = () => {
        this.setState( ChangePasswordDialogModule.makeState());
        this.props.history.push(RouteConfig.ROUTE_DASHBOARD);
    };


    private continuePasswordChange = () => {
        let next: ChangePasswordStatus;
        switch (this.state.status) {
            case ChangePasswordStatus.OPEN:
                this.handleChangePassword();
                break;
            case ChangePasswordStatus.REJECT:
            case ChangePasswordStatus.SUCCESS:
                this.navigateToStart();
                break;
        }
    };


    private renderContent = () => {
        switch (this.state.status) {
            case ChangePasswordStatus.OPEN:
                return this.renderOpen();
            case ChangePasswordStatus.PENDING:
                return this.renderPending();
            case ChangePasswordStatus.REJECT:
                return this.renderReject();
            case ChangePasswordStatus.SUCCESS:
                return this.renderSuccess();
        }
    };

    private renderOpen = () => {
        return <div>
            <div>
                <TextField floatingLabelText={"Enter Password"}
                           value={this.state.password}
                           type="password"
                           onChange={this.changeNormalPassword}/>

            </div>
            <div>
                <TextField floatingLabelText={"Confirm Password"}
                           value={this.state.confirmedPassword}
                           type="password"
                           errorText={this.state.passError}
                           onChange={this.changeConfirmPassword}/>
            </div>
        </div>
    };

    private renderReject = () => {
        return <div>
            Could not reject password. Either your reset link expired or an error occurred. Please contact an admin.
        </div>
    };

    private renderPending = () => {
        return <div>
            Changing password...
        </div>
    };

    private renderSuccess = () => {
        return <div>
            Password successfully changed.
        </div>
    };

    render() {
        return (<div>
            {this.props.match.params.resetId}
            <Dialog open={true}
                    title="Change Your Password"
                    actions={[<FlatButton
                        disabled={this.state.status === ChangePasswordStatus.PENDING}
                        onClick={this.continuePasswordChange}
                        label="Continue"
                    />, <FlatButton
                        label="Abort"
                    />]}
            >
                {this.renderContent()}
            </Dialog>
        </div>);
    }
}

/**
 * @see ChangePasswordDialogModule
 * @author nt
 * @since 24.12.2017
 */
export const ChangePasswordDialog = withRouter(ChangePasswordDialogModule);