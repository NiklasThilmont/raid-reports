import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {RegisterUserStatus} from '../../../model/user/RegisterUserStatus';
import {
    Dialog, FlatButton, Step, StepContent, StepLabel, Stepper, TextField, StepButton,
    RaisedButton, FontIcon
} from 'material-ui';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import {UserActionCreator} from '../../../reducer/user/UserActionCreator';
import {isNullOrUndefined} from 'util';

interface RegisterUserDialogProps {
    registerUserStatus: RegisterUserStatus;
    registerRejectReason: string;
    username: string;
    password: string;
}

interface RegisterUserDialogLocalProps {

}

interface RegisterUserDialogLocalState {

}

interface RegisterUserDialogDispatch {
    changeUsername(username: string): void;
    changePassword(password: string): void;
    invokeCreation(): void;
    closeAndReset(): void;
}

class RegisterUserDialogModule extends React.Component<RegisterUserDialogProps & RegisterUserDialogLocalProps & RegisterUserDialogDispatch, RegisterUserDialogLocalState> {

    constructor(props: RegisterUserDialogProps & RegisterUserDialogLocalProps & RegisterUserDialogDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: RegisterUserDialogLocalProps): RegisterUserDialogProps {
        return {
            registerUserStatus: state.users.registerUserStatus(),
            username: state.appStore.username(),
            password: state.appStore.password(),
            registerRejectReason: state.users.registerRejectReason()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RegisterUserDialogDispatch {
        return {
            changeUsername: username => dispatch(AppActionCreator.ChangeUsername(username)),
            changePassword: password => dispatch(AppActionCreator.ChangePassword(password)),
            invokeCreation: () => dispatch(UserActionCreator.AsyncCreateAccount()),
            closeAndReset: () => dispatch(UserActionCreator.AsyncResetCreation())
        };
    }

    render() {
        console.debug("Register reject reason:", this.props.registerRejectReason);
        return (<Dialog
            open={this.props.registerUserStatus !== RegisterUserStatus.DIALOG_CLOSED}
            actions={[<FlatButton label="Close"
                                  icon={<FontIcon className="material-icons">
                                      close
                                  </FontIcon>}
                                  onClick={this.props.closeAndReset}
                                  />]}
        >
            <Stepper activeStep={this.props.registerUserStatus} orientation="vertical">
                <Step>
                    <StepLabel>User Information</StepLabel>
                    <StepContent>
                        <TextField
                            floatingLabelText="Username"
                            value={this.props.username}
                            onChange={(e,v) => this.props.changeUsername(v)}
                        />
                        <br/>
                        <TextField
                            floatingLabelText="Password"
                            type="password"
                            value={this.props.password}
                            onChange={(e,v) => this.props.changePassword(v)}
                        />
                        <br/>
                        <RaisedButton
                            primary={true}
                            label="Create"
                            onClick={this.props.invokeCreation}
                        />
                    </StepContent>
                </Step>
                <Step>
                    <StepLabel>Registration Pending</StepLabel>
                    <StepContent>
                        <p>Registration is currently pending</p>
                    </StepContent>
                </Step>
                <Step>
                    <StepLabel>Finished</StepLabel>
                    <StepContent>
                        {isNullOrUndefined(this.props.registerRejectReason) ? "Success. Login is possible after your account is unlocked." : "Register rejected: " + this.props.registerRejectReason}
                        <br/>
                        <RaisedButton
                            primary={true}
                            label="OK"
                            onClick={this.props.closeAndReset}
                        />
                    </StepContent>
                </Step>
            </Stepper>
        </Dialog>);
    }
}

/**
 * @see RegisterUserDialogModule
 * @author Niklas
 * @since 14.10.2017
 */
export const RegisterUserDialog: React.ComponentClass<RegisterUserDialogLocalProps> = connect(RegisterUserDialogModule.mapStateToProps, RegisterUserDialogModule.mapDispatchToProps)(RegisterUserDialogModule);