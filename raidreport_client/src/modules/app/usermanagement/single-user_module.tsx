import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {User} from '../../../model/user/User';
import {RaisedButton} from 'material-ui';
import {UserActionCreator} from '../../../reducer/user/UserActionCreator';
import {ConfirmDialog} from '../general/confirm-dialog_module';
import {ResetPasswordDialog} from "./reset-password-dialog_module";

const Toggle = require('react-toggle').default;
require("react-toggle/style.css");

interface SingleUserProps {
    username: string,
    password: string
}

interface SingleUserLocalProps {
    user: User;
}

interface SingleUserLocalState {
    confirmOpen: boolean;
}

interface SingleUserDispatch {
    deleteUser(id: number): void;
    unlockUser(id: number): void;
}

class SingleUserModule extends React.Component<SingleUserProps & SingleUserLocalProps & SingleUserDispatch, SingleUserLocalState> {

    constructor(props: SingleUserProps & SingleUserLocalProps & SingleUserDispatch) {
        super(props);
        this.state = {
            confirmOpen: false
        }
    }

    static mapStateToProps(state: AppState, localProps: SingleUserLocalProps): SingleUserProps {
        return {
            username: state.appStore.username(),
            password: state.appStore.password(),
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SingleUserDispatch {
        return {
            deleteUser: id => dispatch(UserActionCreator.AsyncDeleteUser(id)),
            unlockUser: id => dispatch(UserActionCreator.AsyncUnlockUser(id))
        }
    }

    private handleUserUnlock = () => {
        if(!this.props.user.enabled()) {
            this.props.unlockUser(this.props.user.id());
        }
    };

    private closeConfirm = () => {
        this.setState({
            confirmOpen: false
        })
    };

    private openConfirm = () => {
        this.setState({
            confirmOpen: true
        })
    };

    render() {
        return (<span>
                <ConfirmDialog
                    declineLabel="Delete"
                    msg={"Do you really want to delete the user " + this.props.user.name() + "?"}
                    open={this.state.confirmOpen}
                    onAccept={() => {
                        this.closeConfirm();
                        this.props.deleteUser(this.props.user.id());
                    }}
                    onDecline={this.closeConfirm}
                />
                <span style={{margin: "16px"}}>
                       {this.props.user.name()}
                </span>
                <span style={{margin: "16px"}}>
                      <Toggle
                          onChange={this.handleUserUnlock}
                          checked={this.props.user.enabled()}
                      />
                </span>
               <span style={{margin: "16px"}}>
                    <RaisedButton
                        label="Delete User"
                        secondary={true}
                        onClick={this.openConfirm}
                    />
                </span>
                <span style={{margin: "16px"}}>
                    <ResetPasswordDialog
                        user={this.props.user}
                        adminPassword={this.props.password}
                        adminUsername={this.props.username}
                    />
                </span>
            </span>);
    }
}

/**
 * @see SingleUserModule
 * @author Niklas
 * @since 14.10.2017
 */
export const SingleUser: React.ComponentClass<SingleUserLocalProps> = connect(SingleUserModule.mapStateToProps, SingleUserModule.mapDispatchToProps)(SingleUserModule);