import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {User} from '../../../model/user/User';
import * as Immutable from 'immutable';
import {UserActionCreator} from '../../../reducer/user/UserActionCreator';
import {ListItem, Paper} from 'material-ui';
import {SingleUser} from './single-user_module';
import {Comparators} from "../../../util/Comparators";

interface UserListProps {
    users: Array<User>;
}

interface UserListLocalProps {

}

interface UserListLocalState {

}

interface UserListDispatch {
    loadAllUsers(): void;
}

class UserListModule extends React.Component<UserListProps & UserListLocalProps & UserListDispatch, UserListLocalState> {

    static mapStateToProps(state: AppState, localProps: UserListLocalProps): UserListProps {
        return {
            users: state.users.users().toArray().sort(Comparators.compareUsersByName)
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): UserListDispatch {
        return {
            loadAllUsers: () => dispatch(UserActionCreator.AsyncRetrieveAllUsers())
        }
    }

    public componentDidMount() {
        this.props.loadAllUsers();
    }

    private static mapUser = (user: User) => {
        return <div key={user.id()} style={{padding: "2px"}}><SingleUser user={user}/></div>
    };

    render() {
        return (
        <div style={{margin: "16px"}}>
            <div className="General-Container">
                {this.props.users.map(UserListModule.mapUser)}
            </div>
        </div>);
    }
}

/**
 * @see UserListModule
 * @author Niklas
 * @since 14.10.2017
 */
export const UserList: React.ComponentClass<UserListLocalProps> = connect(UserListModule.mapStateToProps, UserListModule.mapDispatchToProps)(UserListModule);