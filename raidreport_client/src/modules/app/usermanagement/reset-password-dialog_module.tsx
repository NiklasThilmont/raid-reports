import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from "../../../model/AppState";
import {Dialog, FlatButton, RaisedButton, TextField} from "material-ui";
import {User} from "../../../model/user/User";
import {ConfirmDialog} from "../general/confirm-dialog_module";
import {RaidreportJSUtils} from "../../../util/RaidreportJSUtils";
import nothing = RaidreportJSUtils.nothing;
import axios from "axios";
import {Config} from "../../../util/Config";
import {AxiosUtils} from "../../../util/AxiosUtils";


interface ResetPasswordDialogLocalProps {
    adminUsername: string;
    adminPassword: string;
    user: User;
}

interface ResetPasswordDialogLocalState {
    open: boolean;
    confirmOpen: boolean;
    resetFailed: boolean;
    resetLink: string;
}


class ResetPasswordDialogModule extends React.Component<ResetPasswordDialogLocalProps, ResetPasswordDialogLocalState> {

    private constructor(props: ResetPasswordDialogLocalProps) {
        super(props);
        this.state = this.initState();
    }


    private renderConfirmMessage = () => {
        return "Would you really like to reset " + this.props.user.name() + "'s password?";
    };

    private initState = () => {
        return {
            open: false,
            confirmOpen: false,
            resetLink: "",
            resetFailed: false
        }
    };

    private startReset = () => {
        this.setState({
            confirmOpen: true
        })
    };

    private handleAcceptReset = () => {
        let credentials = {
            username: this.props.adminUsername,
            password: this.props.adminPassword
        };
        axios.delete(Config.makeResetPasswordUrl(this.props.user.id()), {auth: credentials}).then(response => {
            this.setState({
                open: true,
                confirmOpen: false,
                resetLink: Config.makeChangePasswordUrl(response.data)
            })
        }).catch(error => {
            AxiosUtils.handleAxiosError(error);
            this.setState({
                open: true,
                confirmOpen: false,
                resetFailed: true
            })
        });
    };

    private finishReset = () => {
        this.setState(this.initState());
    };

    private handleDeclineReset = () => {
        this.setState({
            confirmOpen: false
        })
    };

    private renderContent = () => {
        if(this.state.resetFailed) {
            return <div>Could not reset password.</div>
        } else {
            return <div>
                The password for {this.props.user.name()} has been reset. By using the following link, a
                new password may be set:
                <TextField
                    floatingLabelText="Reset Link"
                    value={this.state.resetLink}
                    onChange={nothing}
                    fullWidth={true}
                />
            </div>
        }
    };

    render() {
        return (
        <span>
            <RaisedButton label="Reset Password"
                          primary={true}
                          onClick={this.startReset}
            />
            <ConfirmDialog open={this.state.confirmOpen}
                           msg={this.renderConfirmMessage()}
                           declineLabel="Reset"
                           onAccept={this.handleAcceptReset}
                           onDecline={this.handleDeclineReset}
            />
            <Dialog open={this.state.open}
                    title="Reset Link"
                    actions={[<FlatButton
                        label="OK"
                        onClick={this.finishReset}
                    />]}
            >
                {this.renderContent()}
            </Dialog>
        </span>);
    }
}

/**
 * @see ResetPasswordDialogModule
 * @author nt
 * @since 24.12.2017
 */
export const ResetPasswordDialog = ResetPasswordDialogModule;