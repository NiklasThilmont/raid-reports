import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../model/AppState';
import {AppActionCreator} from '../../reducer/app/AppActionCreator';
import {Checkbox, IconButton, Popover, RaisedButton, TextField, TouchTapEvent} from 'material-ui';
import {UserActionCreator} from '../../reducer/user/UserActionCreator';
import {RegisterUserStatus} from '../../model/user/RegisterUserStatus';
import {RegisterUserDialog} from './usermanagement/register-user-dialog_module';

interface LoginButtonProps {
    username: string;
    password: string;
    errorText: string;
}

interface LoginButtonLocalProps {

}

interface LoginButtonLocalState {
    anchor:  any;
    open: boolean;
    create: boolean;
    rememberLogin: boolean;

}

interface LoginButtonDispatch {
    changeUsername(username: string): void;
    changePassword(password: string): void;
    logInUser(username: string, password: string, storeInCookies: boolean): void;
    startUserCreation(): void;
}

class LoginButtonModule extends React.Component<LoginButtonProps & LoginButtonLocalProps & LoginButtonDispatch, LoginButtonLocalState> {

    constructor(props: LoginButtonProps & LoginButtonLocalProps & LoginButtonDispatch) {
        super(props);
        this.state = {
            anchor: null,
            open: false,
            create: false,
            rememberLogin: true
        };
    }

    static mapStateToProps(state: AppState, localProps: LoginButtonLocalProps): LoginButtonProps {
        return {
            username: state.appStore.username(),
            password: state.appStore.password(),
            errorText: state.appStore.loginErrorText()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): LoginButtonDispatch {
        return {
            changeUsername: username => dispatch(AppActionCreator.ChangeUsername(username)),
            changePassword: password => dispatch(AppActionCreator.ChangePassword(password)),
            logInUser: (username, password, storeInCookies) => dispatch(AppActionCreator.AsyncLoginUser(username, password, storeInCookies)),
            startUserCreation: () => dispatch(UserActionCreator.SetRegisterUserStatus(RegisterUserStatus.DIALOG_OPEN))
        };
    }


    private handleOpenButtonClick = (event: TouchTapEvent) => {
        this.setState({
            anchor: event.currentTarget,
            open: !this.state.open
        });
    };

    private getLoginOpenerLabel = () => {
        if(this.state.open) return 'Stop';
        return 'Login';
    };

    private invokeLogin = () => {
       this.props.logInUser(this.props.username, this.props.password, this.state.rememberLogin);
    };


    private closeAndReset = () => {
        this.setState({
            anchor: null,
            create: false,
            rememberLogin: true,
            open: false,
        });
    };

    private invokeCreation = () => {
        this.closeAndReset();
        this.props.startUserCreation();
    };

    private handleLoginOrCreation = () => {
        if(this.state.create === false) {
            this.invokeLogin();
        } else {
            this.invokeCreation();
        }
    };

    private handlePasswordButtonKeyPress = (event: React.KeyboardEvent<{}>) => {
        if(event.key.toLocaleLowerCase() == 'enter') {
            this.handleLoginOrCreation();
        }
    };



    render() {
        return <div>
            <IconButton
                onTouchTap={this.handleOpenButtonClick}
                iconClassName="material-icons"
            >
                input
            </IconButton>
            <RegisterUserDialog/>
            <Popover
                open={this.state.open}
                anchorEl={this.state.anchor}
                anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
                onRequestClose={this.closeAndReset}
                style={{paddingLeft: '20px', paddingRight: '20px', paddingTop: '20px', paddingBottom:'20px'}}

            >
                <h3>Login</h3>
                <TextField
                    floatingLabelText="Username"
                    value={this.props.username}
                    onChange={(e,v) => this.props.changeUsername(v)}
                />
                <br/>
                <TextField
                    floatingLabelText="Password"
                    type="password"
                    value={this.props.password}
                    onChange={(e,v) => this.props.changePassword(v)}
                    errorText={this.props.errorText}
                    onKeyPress={this.handlePasswordButtonKeyPress}
                />
                <br/>
                <Checkbox
                    label="Remember Me"
                    checked={this.state.rememberLogin}
                    onCheck={(event, checked) => {this.setState({rememberLogin: checked});}}
                />
                <br/>
                <Checkbox
                    label="Create"
                    checked={this.state.create}
                    onCheck={(event, checked) => {this.setState({create: checked});}}
                />
                <RaisedButton
                    style={{float:'right'}}
                    primary={true}
                    label={this.state.create ? "Create Account" : "Login"}
                    onClick={this.handleLoginOrCreation}
                />
            </Popover>
        </div>;
    }
}

/**
 * @see LoginButtonModule
 * @author Niklas
 * @since 01.07.2017
 */
export const LoginButton: React.ComponentClass<LoginButtonLocalProps> = connect(LoginButtonModule.mapStateToProps, LoginButtonModule.mapDispatchToProps)(LoginButtonModule);