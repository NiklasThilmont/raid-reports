import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {BuildInfo} from '../../../model/BuildInfo';
import axios, {AxiosError, AxiosResponse} from 'axios';
import {isNullOrUndefined} from 'util';
import {Config} from '../../../util/Config';
import {AxiosUtils} from '../../../util/AxiosUtils';
import {DateUtils} from '../../../util/DateUtils';

interface AppInfoProps {

}

interface AppInfoLocalProps {

}

interface AppInfoLocalState {
    buildInfo: BuildInfo;
}

interface AppInfoDispatch {

}

class AppInfoModule extends React.Component<AppInfoProps & AppInfoLocalProps & AppInfoDispatch, AppInfoLocalState> {

    constructor(props: AppInfoProps & AppInfoLocalProps & AppInfoDispatch) {
        super(props);
        this.state = {
            buildInfo: null,
        };
    }

    static mapStateToProps(state: AppState, localProps: AppInfoLocalProps): AppInfoProps {
        return {}
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): AppInfoDispatch {
        return {}
    }


    public componentDidMount() {
        axios.get(Config.GetInfoApiString).then((response: AxiosResponse) => {
            this.setState({
                buildInfo: response.data.build
            });
        }).catch((error: AxiosError) => {
            AxiosUtils.handleAxiosError(error);
        });
    }


    private makeVersionString = () => {
        if(!isNullOrUndefined(this.state.buildInfo)) {
            let buildInfo = this.state.buildInfo;
            if(!isNullOrUndefined(buildInfo.version) && !isNullOrUndefined(buildInfo.name) && !isNullOrUndefined(buildInfo.time)) {
                let date = new Date(Date.parse(buildInfo.time));
                return 'API: ' + buildInfo.name + ' version ' + buildInfo.version + ' built at ' + DateUtils.formatToBuildDate(date);
            } else {
                return 'Unknown Build';
            }
        }
        return 'Unknown Build';
    };


    render() {
        return (<div>
            <div className="General-Container Margined-Button">
                <h3>Raid-Report</h3>
                <p>
                    Raid-Report is based around the idea of optimizing raid performance by analyzing the data
                    gathered through tools like <a target="_blank" href="https://www.deltaconnected.com/arcdps/">Arc DPS</a>.
                    <br/>
                    The data analysis is performed by already existing tools, such as <a target="_blank" href="https://www.raidheroes.tk">Raid Heroes </a>
                     or  <a target="_blank" href="https://github.com/baaron4/GW2-Elite-Insights-Parser/">Elite-Insights</a>, which do an excellent job at analyzing and displaying the raid performance.
                    <br/>
                    As a result, this app aims to integrate Raid Heroes into a team environment, and has the follwing features:
                </p>
                <ul>
                    <li><strong>Map your teams.</strong> Recreate your existing raid teams in r/team/performance, manage access and permissions</li>
                    <li><strong>Upload Arc DPS reports</strong> and have them converted by Raid Heroes for you</li>
                    <li><strong>Share</strong> single reports or whole teams with the public</li>
                </ul>
                <p>
                    {"If you find a bug or want to request a feature, please use the issue tracker at "}
                    <a href="https://bitbucket.org/NiklasThilmont/raid-reports/issues?status=new&status=open">Bitbucket</a>
                </p>
            </div>
            <div className="General-Container Margined-Button">
                <div style={{color: '#8e8e8e', textAlign: 'center'}}>
                    Made by Niklas Thilmont / niklas.1587 @ GW2
                    <br/>
                    <span style={{fontS0ize: '12px'}}>{this.makeVersionString()}</span>
                </div>
            </div>
        </div>);
    }
}

/**
 * @see AppInfoModule
 * @author Niklas
 * @since 21.10.2017
 */
export const AppInfo: React.ComponentClass<AppInfoLocalProps> = connect(AppInfoModule.mapStateToProps, AppInfoModule.mapDispatchToProps)(AppInfoModule);