import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {ShareActionCreator} from '../../../../reducer/share/ShareActionCreator';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {isNullOrUndefined} from 'util';
import {CircularProgress} from 'material-ui';
import {SingleTeamReportList} from '../../teams/singleteam/single-team-reportlist_module';
import {SharedTeamReportList} from './shared-team-reportlist_module';

interface SharedTeamProps {
    sharedTeam: RaidTeam;
}

interface SharedTeamLocalProps {

}

interface SharedTeamLocalState {

}

interface SharedTeamDispatch {
    loadSharedTeam(shareId: string): void;
}

interface SharedTeamRouterProps {
    match: {params: {shareId: string}}
}

class SharedTeamModule extends React.Component<SharedTeamProps & SharedTeamLocalProps & SharedTeamDispatch & SharedTeamRouterProps, SharedTeamLocalState> {

    constructor(props: SharedTeamProps & SharedTeamLocalProps & SharedTeamDispatch & SharedTeamRouterProps) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: SharedTeamLocalProps): SharedTeamProps {
        return {
            sharedTeam: state.share.sharedTeam()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SharedTeamDispatch {
        return {
            loadSharedTeam: shareId => dispatch(ShareActionCreator.AsyncLoadSharedTeam(shareId))
        }
    }

    public componentDidReceiveProps(oldProps: SharedTeamProps & SharedTeamLocalProps & SharedTeamDispatch & SharedTeamRouterProps) {
        if(this.props.match.params.shareId != oldProps.match.params.shareId) {
            this.props.loadSharedTeam(this.props.match.params.shareId);
        }
    }

    public componentDidMount() {
        this.props.loadSharedTeam(this.props.match.params.shareId);
    }

    render() {
        console.debug("Shared team:", this.props.sharedTeam);
        if(isNullOrUndefined(this.props.sharedTeam)) {
            return <div className="General-Container">No team selected or found</div>;
        }
        return(
        <div style={{padding: '48px'}}>
            <div className="General-Container-Less-Padding">
                <div className="Aligner">
                    <div className="Aligner-Center">
                        <span style={{fontSize: '24px'}}>Team {this.props.sharedTeam.teamName()}</span>
                    </div>
                </div>
            </div>
            <div style={{marginTop: "16px"}}>
                <SharedTeamReportList
                    shareId={this.props.match.params.shareId}
                />
            </div>
        </div>);
    }
}

/**
 * @see SharedTeamModule
 * @author Niklas
 * @since 27.10.2017
 */
export const SharedTeam: React.ComponentClass<SharedTeamLocalProps> = connect(SharedTeamModule.mapStateToProps, SharedTeamModule.mapDispatchToProps)(SharedTeamModule);