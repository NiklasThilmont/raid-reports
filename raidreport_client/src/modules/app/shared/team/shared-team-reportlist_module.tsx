import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {FontIcon, Paper, RaisedButton} from 'material-ui';
import {ReportListTree} from '../../upload/tree/report-list-tree_module';
import {SelectedNodeInfo} from '../../upload/tree/selected-node-info_module';
import {ReportListSorter} from '../../upload/report-list-sorter_module';
import {ShareActionCreator} from '../../../../reducer/share/ShareActionCreator';

interface SharedTeamReportListProps {

}

interface SharedTeamReportListLocalProps {
    shareId: string;
}

interface SharedTeamReportListLocalState {

}

interface SharedTeamReportListDispatch {
    reloadSharedTeam(shareId: string): void;
}

class SharedTeamReportListModule extends React.Component<SharedTeamReportListProps & SharedTeamReportListLocalProps & SharedTeamReportListDispatch, SharedTeamReportListLocalState> {

    constructor(props: SharedTeamReportListProps & SharedTeamReportListLocalProps & SharedTeamReportListDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: SharedTeamReportListLocalProps): SharedTeamReportListProps {
        return {
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SharedTeamReportListDispatch {
        return {
            reloadSharedTeam: shareId => dispatch(ShareActionCreator.AsyncLoadSharedTeam(shareId))
        }
    }

    render() {
        return (<div>
            <div className="row">
                <div className="col-md-9" style={{marginBottom: "16px"}}>
                    <ReportListTree
                        isShared={true}
                        currentTeamId={this.props.shareId}
                        showDeleteButton={true}
                        onRefreshReports={() => this.props.reloadSharedTeam(this.props.shareId)}
                    />
                </div>
                <div className="col-md-3">
                    <Paper style={{backgroundColor: '#21252B', color: 'white', padding: '30px', borderRadius: '20px'}}>
                        <SelectedNodeInfo
                            isShared={true}
                            showDeleteButton={false}
                            currentTeamId={this.props.shareId}
                        />
                    </Paper>
                    <div style={{width: '100%', marginTop: '20px'}}>
                        <ReportListSorter/>
                    </div>
                </div>
            </div>
        </div>);
    }
}

/**
 * @see SharedTeamReportListModule
 * @author Niklas
 * @since 27.10.2017
 */
export const SharedTeamReportList: React.ComponentClass<SharedTeamReportListLocalProps> = connect(SharedTeamReportListModule.mapStateToProps, SharedTeamReportListModule.mapDispatchToProps)(SharedTeamReportListModule);