import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {Dialog, TextField, FlatButton, FontIcon, IconButton, RaisedButton} from 'material-ui';
import {CreateTeamStatus} from '../../../model/team/CreateTeamStatus';
import {RaidTeamActionCreator} from '../../../reducer/teams/RaidTeamActionCreator';
import SetCreateTeamStatus = RaidTeamActionCreator.SetCreateTeamStatus;

interface RaidTeamCreateDialogProps {
    createTeamStatus: CreateTeamStatus;
}

interface RaidTeamCreateDialogLocalProps {

}

interface RaidTeamCreateDialogLocalState {
    name: string;
}

interface RaidTeamCreateDialogDispatch {
    close(): void;
    createTeam(name: string): void;
}

class RaidTeamCreateDialogModule extends React.Component<RaidTeamCreateDialogProps & RaidTeamCreateDialogLocalProps & RaidTeamCreateDialogDispatch, RaidTeamCreateDialogLocalState> {

    constructor(props: RaidTeamCreateDialogProps & RaidTeamCreateDialogLocalProps & RaidTeamCreateDialogDispatch) {
        super(props);
        this.state = {
            name: ''
        };
    }

    static mapStateToProps(state: AppState, localProps: RaidTeamCreateDialogLocalProps): RaidTeamCreateDialogProps {
        return {
            createTeamStatus: state.teams.createTeamStatus()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RaidTeamCreateDialogDispatch {
        return {
            close: () => dispatch(SetCreateTeamStatus(CreateTeamStatus.CLOSED)),
            createTeam: name => dispatch(RaidTeamActionCreator.AsyncCreateTeam(name))
        };
    }

    private readonly actions = [
        <FlatButton
            label="Close"
            icon={<FontIcon className="material-icons" onClick={this.props.close}>close</FontIcon>}
            onClick={() => this.props.close()}
        />
    ];

    private renderContent = () => {
        if(this.props.createTeamStatus === CreateTeamStatus.CREATION_PENDING) {
            return <div>Creation Pending</div>;
        } else if(this.props.createTeamStatus === CreateTeamStatus.CREATION_SUCCESS) {
            return<div>
                <div className="Aligner Full-Width ">
                    <FontIcon className="material-icons">done</FontIcon>
                </div>
                <div className="Aligner Full-Width ">
                    <h4>Creation Successful</h4>
                </div>
            </div>
        } else if(this.props.createTeamStatus === CreateTeamStatus.CREATION_FAILED) {
            return <div>
                <div className="Aligner Full-Width ">
                    <FontIcon className="material-icons">error</FontIcon>
                </div>
                <div className="Aligner Full-Width ">
                    <h4>Creation Failed</h4>
                </div>
            </div>;
        } else {
            return <div>
                <TextField
                    floatingLabelText="Team Name"
                    value={this.state.name}
                    onChange={(e, v) => this.setState({name: v})}
                />
                <FlatButton
                    label="Create Team"
                    icon={<FontIcon className="material-icons">create</FontIcon>}
                    onClick={() => this.props.createTeam(this.state.name)}
                />
                <br/>
            </div>
        }
    };

    render() {
        return (<Dialog
            open={this.props.createTeamStatus !== CreateTeamStatus.CLOSED}
            onRequestClose={this.props.close}
            title="Create Raid Team"
            autoScrollBodyContent={true}
            actions={this.actions}
        >
            {this.renderContent()}
        </Dialog>);
    }
}

/**
 * @see RaidTeamCreateDialogModule
 * @author Niklas
 * @since 15.10.2017
 */
export const RaidTeamCreateDialog: React.ComponentClass<RaidTeamCreateDialogLocalProps> = connect(RaidTeamCreateDialogModule.mapStateToProps, RaidTeamCreateDialogModule.mapDispatchToProps)(RaidTeamCreateDialogModule);