import * as React from 'react';
import {IRenderTarget} from '../../../../model/team/RenderTarget';
import {Dialog, FlatButton, FontIcon, RadioButton, RadioButtonGroup} from 'material-ui';
import {InfoIcon} from '../../../icons/MaterialIcons';

interface ManageRenderTargetDialogProps {
    open: boolean;
    availableRenderTargets: Array<IRenderTarget>;
    initialRenderTarget: IRenderTarget;
    teamName?: string;
    onExit(): void;
    onSave(newRenderTarget: IRenderTarget): void;
}

interface ManageRenderTargetDialogLocalState {
    currentTarget: IRenderTarget;
}

export class ManageRenderTargetDialogModule extends React.Component<ManageRenderTargetDialogProps , ManageRenderTargetDialogLocalState> {

    constructor(props: ManageRenderTargetDialogProps) {
        super(props);
        this.state = {
            currentTarget: props.initialRenderTarget
        };
    }

    private onSelectChange = (event: any, renderTargetName: any) => {
        const renderTarget = this.props.availableRenderTargets.find(target => target.name === renderTargetName);
        if (renderTarget !== this.state.currentTarget) {
            this.setState({
                currentTarget: renderTarget
            })
        }
    };

    private handleExit = () => {
        console.log(this.props.initialRenderTarget);
        this.setState({
            currentTarget: this.props.initialRenderTarget
        });
        this.props.onExit();
    };

    private mapRadioButton = (target: IRenderTarget) => {
        return <RadioButton
                key={target.name}
                value={target.name}
                label={target.name}
            >
            </RadioButton>
    };

    render() {
        return (<Dialog
            open={this.props.open}
            onRequestClose={this.handleExit}
            actions={[
                <FlatButton label="Close" onClick={this.handleExit} icon={<FontIcon className="material-icons">close</FontIcon>}/>,
                <FlatButton label="Save" onClick={() => this.props.onSave(this.state.currentTarget)} icon={<FontIcon className="material-icons">save</FontIcon>}/>,
            ]}
        >
            <h3>Render Target Management for {this.props.teamName}</h3>
            {
                this.props.availableRenderTargets.length == 0
                    ?
                    <span>Loading Targets...</span>
                    :
                <RadioButtonGroup
                    name="render_targets"
                    valueSelected={this.state.currentTarget.name}
                    onChange={this.onSelectChange}
                >
                    {this.props.availableRenderTargets.map(this.mapRadioButton)}
                </RadioButtonGroup>
            }
            <br/>
            <span>Change of render target will only affect future imports!</span><br/>
            <span>The old render target is stored on a per-report base. Subsequent reimports will always use the old rendering engine.</span>
        </Dialog>);
    }
}