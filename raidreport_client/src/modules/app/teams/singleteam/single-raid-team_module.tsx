import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {AppState} from '../../../../model/AppState';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
import {isNullOrUndefined} from 'util';
import * as Immutable from 'immutable';
import {RaidTeamMenu} from './raid-team-menu_modules';
import {ManageRaidTeamDialog} from './managa-raid-team-dialog_module';
import {SingleTeamReportList} from './single-team-reportlist_module';
import {IconButton} from 'material-ui';
import {Link} from 'react-router-dom';
import {Config, RouteConfig} from '../../../../util/Config';

interface SingleRaidTeamProps {
    currentTeam: RaidTeam;
    raidTeams: Immutable.List<RaidTeam>;
}

interface SingleRaidTeamLocalProps {

}

interface SingleRaidTeamLocalState {

}

interface SingleRaidTeamDispatch {
    selectTeam(id: number): void;
}

interface SingleRaidTeamRouterProps {
    match: {params: {id: number}}
}

class SingleRaidTeamModule extends React.Component<SingleRaidTeamProps & SingleRaidTeamLocalProps & SingleRaidTeamDispatch & SingleRaidTeamRouterProps, SingleRaidTeamLocalState> {


    static mapStateToProps(state: AppState, localProps: SingleRaidTeamLocalProps & SingleRaidTeamRouterProps): SingleRaidTeamProps {
        return {
            currentTeam: state.teams.getTeamById(state.teams.currentTeamId()),
            raidTeams: state.teams.raidTeams()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SingleRaidTeamDispatch {
        return {
            selectTeam: (id) => dispatch(RaidTeamActionCreator.AsyncSelectTeam(id))
        };
    }

    public componentDidMount() {
        this.props.selectTeam(this.props.match.params.id);
    }

    render() {
        if(isNullOrUndefined(this.props.currentTeam)) {
            return <div className="General-Container">No team selected or found</div>;
        }
        return (
        <div className="Team-Data-Container">
            <div className="General-Container-Less-Padding">
                <div className="Aligner">
                    <div className="Aligner-Right">
                        <Link to={RouteConfig.ROUTE_TEAM_PERFORMANCE.replace(":teamId", this.props.match.params.id.toString())}>
                            <IconButton iconClassName="material-icons"
                                        tooltip="Switch to Performance Review"
                                        tooltipPosition="bottom-right"
                            >
                                insert_chart
                            </IconButton>
                        </Link>
                    </div>
                    <div className="Aligner-Center">
                        <span style={{fontSize: '24px'}}>Team {this.props.currentTeam.teamName()}</span>
                    </div>
                    <div className="Aligner-Right">
                        <RaidTeamMenu/>
                    </div>
                </div>
                <ManageRaidTeamDialog/>
            </div>
            <div style={{marginTop: "16px"}}>
                <SingleTeamReportList/>
            </div>
        </div>);
    }
}

/**
 * @see RaidTeamReportsModule
 * @author Niklas
 * @since 15.10.2017
 */
export const RaidTeamReports: React.ComponentClass<SingleRaidTeamLocalProps> = connect(SingleRaidTeamModule.mapStateToProps, SingleRaidTeamModule.mapDispatchToProps)(SingleRaidTeamModule);