import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {Dialog, FlatButton, FontIcon, List, ListItem, Paper, Subheader, TextField} from 'material-ui';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
import {Comparators} from "../../../../util/Comparators";
import {RaidTeamUser} from "../../../../model/team/RaidTeamUser";

interface ManageRaidTeamDialogProps {
    open: boolean;
    raidTeam: RaidTeam;
}

interface ManageRaidTeamDialogLocalProps {

}

interface ManageRaidTeamDialogLocalState {
    name: string;
}

interface ManageRaidTeamDialogDispatch {
    closeDialog(): void;
}

class ManageRaidTeamDialogModule extends React.Component<ManageRaidTeamDialogProps & ManageRaidTeamDialogLocalProps & ManageRaidTeamDialogDispatch, ManageRaidTeamDialogLocalState> {

    constructor(props: ManageRaidTeamDialogProps & ManageRaidTeamDialogLocalProps & ManageRaidTeamDialogDispatch) {
        super(props);
        this.state = {
            name: ""
        }
    }

    static mapStateToProps(state: AppState, localProps: ManageRaidTeamDialogLocalProps): ManageRaidTeamDialogProps {
        return {
            raidTeam: state.teams.getTeamById(state.teams.currentTeamId()),
            open: state.teams.manageDialogOpen()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ManageRaidTeamDialogDispatch {
        return {
            closeDialog: () => dispatch(RaidTeamActionCreator.SetManagementDialogOpen(false))
        }
    }

    private static mapStatusToText(status: String) {
        if(status === RaidTeamUser.INVITE_STATUS_NONE) {
            return " created this Team.";
        }
        if(status === RaidTeamUser.INVITE_STATUS_JOINED) {
            return " is a Member.";
        }
        if(status === RaidTeamUser.INVITE_STATUS_INVITE_SENT) {
            return " has been invited.";
        }
        return " has unhandled " + status;
    }

    private static mapRaidTeamUser(raidTeamUser: RaidTeamUser) {
        return <ListItem key={raidTeamUser.user().id()}>{raidTeamUser.user().name() + ManageRaidTeamDialogModule.mapStatusToText(raidTeamUser.teamInviteStatus())}</ListItem>;
    }

    render() {
        return (<Dialog
            open={this.props.open}
            onRequestClose={this.props.closeDialog}
            actions={[<FlatButton
                label="Close"
                onClick={this.props.closeDialog}
                icon={<FontIcon className="material-icons"
                >
                    close
                </FontIcon>}/>]}
        >
            <h3>Team Management for {this.props.raidTeam.teamName()}</h3>
            <Subheader>Team Members</Subheader>
            <Paper style={{maxHeight: "500px", overflow: 'auto'}}>
                <List>
                    {this.props.raidTeam.users()
                        .sort(Comparators.compareRaidTeamUsersByJoinStatus)
                        .map(ManageRaidTeamDialogModule.mapRaidTeamUser)}
                </List>
            </Paper>

        </Dialog>);
    }
}

/**
 * @see ManageRaidTeamDialogModule
 * @author Niklas
 * @since 15.10.2017
 */
export const ManageRaidTeamDialog: React.ComponentClass<ManageRaidTeamDialogLocalProps> = connect(ManageRaidTeamDialogModule.mapStateToProps, ManageRaidTeamDialogModule.mapDispatchToProps)(ManageRaidTeamDialogModule);