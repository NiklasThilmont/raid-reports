import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {IParticipant} from '../../../../model/team/Participant';
import {ReportFileLeaf} from '../../../../model/ReportFileTree';
import {IconUtils} from '../../../../util/IconUtils';

interface RaidParticipantsInfoProps {
    raidParticipants: Array<IParticipant>;
}

interface RaidParticipantsInfoLocalProps {

}

interface RaidParticipantsInfoLocalState {

}

interface RaidParticipantsInfoDispatch {

}

class RaidParticipantsInfoModule extends React.Component<RaidParticipantsInfoProps & RaidParticipantsInfoLocalProps & RaidParticipantsInfoDispatch, RaidParticipantsInfoLocalState> {

    constructor(props: RaidParticipantsInfoProps & RaidParticipantsInfoLocalProps & RaidParticipantsInfoDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: RaidParticipantsInfoLocalProps): RaidParticipantsInfoProps {
        let leaf = state.appStore.currentNode();
        return {
            raidParticipants: leaf.type === 'leaf' ? (leaf as ReportFileLeaf).reportFile.participants() : []
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RaidParticipantsInfoDispatch {
        return {}
    }

    private renderParticipant = (participant: IParticipant) => {
        return <div key={participant.id} >
            <img src={IconUtils.mapClassToIcon(participant.className)}
                 className="Class-Icon-In-Info"
                 title={participant.className}
                 width={24}
                 height={24}
            />
            {participant.playerAccount.accountName}
        </div>
    };

    render() {
        return (<div>
                <div style={{marginLeft: "8px"}}>
                    {this.props.raidParticipants.map(this.renderParticipant)}
                </div>
            </div>);
    }
}

/**
 * @see RaidParticipantsInfoModule
 * @author Niklas
 * @since 31.10.2017
 */
export const RaidParticipantsInfo: React.ComponentClass<RaidParticipantsInfoLocalProps> = connect(RaidParticipantsInfoModule.mapStateToProps, RaidParticipantsInfoModule.mapDispatchToProps)(RaidParticipantsInfoModule);