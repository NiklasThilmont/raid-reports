import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {Dialog, FlatButton, Step, StepLabel, Stepper, TextField, StepContent, List, ListItem} from 'material-ui';
import {IUser} from '../../../../model/user/User';
import axios, {AxiosRequestConfig} from 'axios';
import {Config} from '../../../../util/Config';
import {userInfo} from 'os';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';

interface InviteUserDialogProps {
    username: string;
    password: string;
    teamId: number;
}

interface InviteUserDialogLocalProps {
    open: boolean;
    onRequestClose(): void;
}

interface InviteUserDialogLocalState {
    name: string;
    status: InviteUserDialogStatus;
    users: Array<IUser>;
}

interface InviteUserDialogDispatch {
    reloadAllRaidTeams(): void;
}

enum InviteUserDialogStatus {
    INITIAL,
    DISPLAY_CANDIDATES,
    PENDING,
    INVITE_SUCCESSFUL,
    INVITE_FAILED
}

class InviteUserDialogModule extends React.Component<InviteUserDialogProps & InviteUserDialogLocalProps & InviteUserDialogDispatch, InviteUserDialogLocalState> {

    constructor(props: InviteUserDialogProps & InviteUserDialogLocalProps & InviteUserDialogDispatch) {
        super(props);
        this.state = {
            name: '',
            status: InviteUserDialogStatus.INITIAL,
            users: []
        };
    }

    static mapStateToProps(state: AppState, localProps: InviteUserDialogLocalProps): InviteUserDialogProps {
        return {
            username: state.appStore.username(),
            password: state.appStore.password(),
            teamId: state.teams.currentTeamId()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): InviteUserDialogDispatch {
        return {
            reloadAllRaidTeams: () => dispatch(RaidTeamActionCreator.AsyncLoadAllTeams())
        };
    }

    private mapStatusToStepperIndex = () => {
        switch (this.state.status) {
            case InviteUserDialogStatus.INITIAL:
                return 0;
            case InviteUserDialogStatus.DISPLAY_CANDIDATES:
                return 1;
            case InviteUserDialogStatus.PENDING:
            case InviteUserDialogStatus.INVITE_SUCCESSFUL:
            case InviteUserDialogStatus.INVITE_FAILED:
                return 2;

        }
    };

    private loadPossibleUsers = () => {
        let config: AxiosRequestConfig = {
            auth: {
                username: this.props.username,
                password: this.props.password
            },
            params: {
                username: this.state.name
            }
        };
        axios.get(Config.GetUsersForNameString, config).then(response => {
            this.setState({
                users: response.data,
                status: InviteUserDialogStatus.DISPLAY_CANDIDATES
            })
        }).catch(error => {
            this.setState({
                status: InviteUserDialogStatus.INVITE_FAILED
            })
        })
    };

    private inviteUser = (userId: number) => {
        let config: AxiosRequestConfig = {
            auth: {
                username: this.props.username,
                password: this.props.password
            }
        };
        axios.patch(Config.makeInviteUserToRoomString(this.props.teamId, userId), {}, config).then(response => {
            this.props.reloadAllRaidTeams();
            this.setState({
                status: InviteUserDialogStatus.INVITE_SUCCESSFUL
            })
        }).catch(error => {
            this.setState({
                status: InviteUserDialogStatus.INVITE_FAILED
            })
        })
    };

    private renderStepThree = () => {
        if(this.state.status === InviteUserDialogStatus.INVITE_SUCCESSFUL) {
            return "User successfully invited";
        } else if(this.state.status === InviteUserDialogStatus.INVITE_FAILED) {
            return "User could not be invited. Please report the issue to a developer."
        } else {
            return "Invite pending...";
        }
    };

    private handleClose = () => {
        this.setState({
            name: '',
            status: InviteUserDialogStatus.INITIAL,
            users: []
        });
        this.props.onRequestClose();
    };

    render() {
        return (<Dialog
            open={this.props.open}
            onRequestClose={this.handleClose}
        >
            <Stepper
                orientation="vertical"
                activeStep={this.mapStatusToStepperIndex()}
            >
                <Step>
                    <StepLabel>Search User</StepLabel>
                    <StepContent>
                        <p>
                            Please provide a user you want to add.
                        </p>
                        <TextField
                            floatingLabelText="Username"
                            value={this.state.name}
                            onChange={(e, v) => this.setState({name: v})}
                        />
                        <FlatButton
                            label="Load Users"
                            onClick={this.loadPossibleUsers}
                        />
                    </StepContent>
                </Step>
                <Step>
                    <StepLabel>Select User</StepLabel>
                    <StepContent>
                        <List>
                            {this.state.users.map(user => <ListItem
                                key={user.id}
                                onClick={() => this.inviteUser(user.id)}
                            >
                                {user.username}
                            </ListItem>)}
                        </List>
                    </StepContent>
                </Step>
                <Step>
                    <StepLabel>Done</StepLabel>
                    <StepContent>
                        <p>
                            {this.renderStepThree()}
                        </p>
                    </StepContent>
                </Step>
            </Stepper>
        </Dialog>);
    }
}

/**
 * @see InviteUserDialogModule
 * @author Niklas
 * @since 15.10.2017
 */
export const InviteUserDialog: React.ComponentClass<InviteUserDialogLocalProps> = connect(InviteUserDialogModule.mapStateToProps, InviteUserDialogModule.mapDispatchToProps)(InviteUserDialogModule);