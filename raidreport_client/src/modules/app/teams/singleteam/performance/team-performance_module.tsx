import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../../model/AppState';
import {RouteComponentProps} from 'react-router';
import {Link} from 'react-router-dom';
import {RouteConfig} from '../../../../../util/Config';
import {IconButton, List, ListItem} from 'material-ui';
import {RaidTeam} from '../../../../../model/team/RaidTeam';
import {RaidTeamActionCreator} from '../../../../../reducer/teams/RaidTeamActionCreator';
import {RaidTeamMenu} from '../raid-team-menu_modules';
import {ITeamPerformanceData} from '../../../../../model/team/performance/TeamPerformanceData';
import {buildMap} from '../../../../../util/RaidreportJSUtils';

interface TeamPerformanceProps {
    currentTeam: RaidTeam;
    participants: Map<string, number>;
}

interface TeamPerformanceLocalProps {

}

interface TeamPerformanceLocalState {

}

interface TeamPerformanceDispatch {
    selectTeam(id: number): void;
    fetchPerformanceData(teamId: number): void;
}

class TeamPerformanceModule extends React.Component<TeamPerformanceProps & TeamPerformanceLocalProps & TeamPerformanceDispatch & RouteComponentProps<any>, TeamPerformanceLocalState> {

    constructor(props: TeamPerformanceProps & TeamPerformanceLocalProps & TeamPerformanceDispatch  & RouteComponentProps<any>) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: TeamPerformanceLocalProps & RouteComponentProps<any>): TeamPerformanceProps {
        let data: ITeamPerformanceData = state.teams.performanceData();
        let participants = new Map();
        if(data) {
            participants = new Map(buildMap(data.participationPerPlayer));
        }
        console.log(data);
        return {
            currentTeam: state.teams.getTeamById(state.teams.currentTeamId()),
            participants: participants
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): TeamPerformanceDispatch {
        return {
            selectTeam: (id) => dispatch(RaidTeamActionCreator.AsyncSelectTeam(id)),
            fetchPerformanceData: teamId => dispatch(RaidTeamActionCreator.AsyncFetchPerformanceData(teamId))
        };
    }

    public componentDidMount() {
        this.props.selectTeam(this.props.match.params.teamId);
        this.props.fetchPerformanceData(this.props.match.params.teamId);
    }

    private mapParticipant = (participationCount: number, name: string) => {
        return <ListItem key={name}>
            <span>{name}: {participationCount}</span>
        </ListItem>
    };

    private renderParticipants = () => {
        let elements: Array<JSX.Element> = [];
        this.props.participants.forEach((participationCount, name) => {
            elements.push(this.mapParticipant(participationCount, name))
        });
        return elements;
    };

    render() {
        if(!this.props.currentTeam) {
            return <div className="General-Container">No team selected or found</div>;
        }
        return (
            <div className="Team-Data-Container">
                <div className="General-Container-Less-Padding">
                    <div className="Aligner">
                        <div className="Aligner-Right">
                            <Link to={RouteConfig.ROUTE_TEAM_OVERVIEW.replace(':id', this.props.match.params.teamId.toString())}>
                                <IconButton iconClassName="material-icons"
                                            tooltip="Switch to Team"
                                            tooltipPosition="bottom-right"
                                >
                                    people
                                </IconButton>
                            </Link>
                        </div>
                        <div className="Aligner-Center">
                            <span style={{fontSize: '24px'}}>Team {this.props.currentTeam.teamName()}</span>
                        </div>
                        <div className="Aligner-Right">
                            <RaidTeamMenu/>
                        </div>
                    </div>
                </div>
                <br/>
                <div className="General-Container">
                    <h4>Participating Players in this Raid Team</h4>
                    <p>
                        Lists all players that have participated in this team, ordered descending by the
                        number of successful kills they have attended in this team. <br/>
                        Does not exclude external members.
                    </p>
                    <List>
                        {this.renderParticipants()}
                    </List>
                </div>
            </div>);
    }
}

/**
 * @see TeamPerformanceModule
 * @author Niklas
 * @since 31.12.2017
 */
export const TeamPerformance: React.ComponentClass<TeamPerformanceLocalProps> = connect(TeamPerformanceModule.mapStateToProps, TeamPerformanceModule.mapDispatchToProps)(TeamPerformanceModule);