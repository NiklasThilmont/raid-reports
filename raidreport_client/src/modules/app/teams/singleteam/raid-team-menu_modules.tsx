import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {Divider, IconButton, IconMenu, MenuItem} from 'material-ui';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
import {InviteUserDialog} from './invite-user-dialog_module';
import {ExitToAppIcon, GroupAddIcon, GroupIcon, LockIcon, SettingsIcon, ShareIcon} from '../../../icons/MaterialIcons';
import {withRouter} from 'react-router';
import {History} from 'history';
import {ConfirmDialog} from '../../general/confirm-dialog_module';
import {ShareTeamDialog} from '../dialogs/share-team-dialog_module';
import {TeamPermissionsDialog} from '../dialogs/team-permissions-dialog_module';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {ManageRenderTargetDialogModule} from './manage-render-target-dialog_module';
import {IRenderTarget} from '../../../../model/team/RenderTarget';

const renderTargets: Array<IRenderTarget> = [
    {
        id: 2,
        name: "Elite Insights",
        iconLocation: ""
    },
    {
        id: 1,
        name: "Raid Heroes",
        iconLocation: ""
    }
];

interface RaidTeamMenuProps {
    currentTeam: RaidTeam;
    availableRenderTargets: Array<IRenderTarget>;
    currentUserHasInvitePermission: boolean;
    currentUserHasManagePermission: boolean;
}

interface RaidTeamMenuLocalProps {

}

interface RaidTeamMenuLocalState {
    inviteUserDialogOpen: boolean;
    confirmOpen: boolean;
    shareOpen: boolean;
    permissionOpen: boolean;
    renderTargetOpen: boolean;
}

interface RaidTeamMenuDispatch {
    fetchRenderTargets(): void;
    openManagementDialog(): void;
    leaveTeam(teamId: number, history: History): void;
    changeRenderTargetFor(teamId: number, newTarget: IRenderTarget): void;
}

interface OtherProps {
    history: History;
}

class RaidTeamMenuModule
    extends React.Component<RaidTeamMenuProps & RaidTeamMenuLocalProps & RaidTeamMenuDispatch & OtherProps, RaidTeamMenuLocalState>
{

    constructor(props: RaidTeamMenuProps & RaidTeamMenuLocalProps & RaidTeamMenuDispatch & OtherProps) {
        super(props);
        this.state = {
            inviteUserDialogOpen: false,
            confirmOpen: false,
            shareOpen: false,
            permissionOpen: false,
            renderTargetOpen: false
        };
    }

    static mapStateToProps(state: AppState, localProps: RaidTeamMenuLocalProps): RaidTeamMenuProps {
        let currentTeam = state.teams.getTeamById(state.teams.currentTeamId());
        return {
            currentTeam: currentTeam,
            currentUserHasInvitePermission: state.teams.hasPermissionInCurrentTeam(state.appStore.username(), "INVITE"),
            currentUserHasManagePermission: state.teams.hasPermissionInCurrentTeam(state.appStore.username(), "MANAGE_PERMISSION"),
            availableRenderTargets: state.teams.availableRenderTargets()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RaidTeamMenuDispatch {
        return {
            openManagementDialog: () => dispatch(RaidTeamActionCreator.SetManagementDialogOpen(true)),
            leaveTeam: (teamId, history2) => dispatch(RaidTeamActionCreator.AsyncLeaveTeam(teamId, history2)),
            fetchRenderTargets: () => dispatch(RaidTeamActionCreator.AsyncFetchRenderTargets()),
            changeRenderTargetFor: (teamId, newTarget) => dispatch(RaidTeamActionCreator.AsyncChangeRenderTarget(teamId, newTarget))
        };
    }

    private closeConfirm = () => {
        this.setState({
            confirmOpen: false
        })
    };

    private openConfirm = () => {
        this.setState({
            confirmOpen: true
        })
    };

    private openPermissions = () => {
        this.setPermissionOpen(true);
    };

    private closePermissions = () => {
        this.setPermissionOpen(false);
    };

    private setShareOpen = (open: boolean) => {
        this.setState({
            shareOpen: open
        })
    };

    private setPermissionOpen = (open: boolean) => {
        this.setState({
            permissionOpen: open
        })
    };

    private setRenderTargetOpen = (open: boolean) => {
        if (open) {
            this.props.fetchRenderTargets();
        }
        this.setState({
            renderTargetOpen: open
        })
    };

    private handleRenderTargetSave = (newTarget: IRenderTarget) => {
        this.setRenderTargetOpen(false);
        this.props.changeRenderTargetFor(this.props.currentTeam.id(), newTarget);
    };


    render() {
        return ( <div>
            <ConfirmDialog
                declineLabel="Leave"
                open={this.state.confirmOpen}
                msg="Do you really want to leave your team?"
                onAccept={() => {
                    this.closeConfirm();
                    this.props.leaveTeam(this.props.currentTeam.id(), this.props.history);
                }}
                onDecline={this.closeConfirm}
            />
            <ShareTeamDialog
                open={this.state.shareOpen}
                onClose={() => this.setShareOpen(false)}
            />
            <ManageRenderTargetDialogModule open={this.state.renderTargetOpen}
                                            availableRenderTargets={this.props.availableRenderTargets}
                                            initialRenderTarget={this.props.currentTeam.renderTarget()}
                                            onExit={() => this.setRenderTargetOpen(false)}
                                            onSave={this.handleRenderTargetSave}
            />
            <InviteUserDialog
                open={this.state.inviteUserDialogOpen}
                onRequestClose={() => this.setState({inviteUserDialogOpen: false})}
            />
            <TeamPermissionsDialog
                open={this.state.permissionOpen}
                onRequestClose={this.closePermissions}
            />
            <IconMenu
                iconButtonElement={<IconButton iconClassName="material-icons">menu</IconButton>}
                >
                    <MenuItem
                        primaryText="Users"
                        leftIcon={<GroupIcon className="Icon-In-Menu"/>}
                        onClick={this.props.openManagementDialog}
                    />
                {
                    this.props.currentUserHasInvitePermission ?
                        <MenuItem
                            primaryText="Invite User"
                            leftIcon={<GroupAddIcon className="Icon-In-Menu"/>}
                            onClick={() => this.setState({inviteUserDialogOpen: true})}
                        />
                        : null
                }
                    <MenuItem
                        primaryText="Share"
                        leftIcon={<ShareIcon className="Icon-In-Menu"/>}
                        onClick={() => this.setShareOpen(true)}
                    />
                {
                    this.props.currentUserHasManagePermission ?
                        <MenuItem
                            primaryText="Render Settings"
                            leftIcon={<SettingsIcon className="Icon-In-Menu"/>}
                            onClick={() => this.setRenderTargetOpen(true)}
                        />
                        : null
                }
                    <MenuItem
                        primaryText="Permissions"
                        leftIcon={<LockIcon className="Icon-In-Menu"/>}
                        onClick={this.openPermissions}
                    />
                    <Divider/>
                    <MenuItem
                        primaryText="Leave Team"
                        leftIcon={<ExitToAppIcon className="Icon-In-Menu"/>}
                        onClick={this.openConfirm}
                    />
            </IconMenu>
        </div>);
    }
}

/**
 * @see RaidTeamMenuModule
 * @author Niklas
 * @since 15.10.2017
 */
export const RaidTeamMenu: React.ComponentClass<RaidTeamMenuLocalProps> = withRouter(connect(RaidTeamMenuModule.mapStateToProps, RaidTeamMenuModule.mapDispatchToProps)(RaidTeamMenuModule) as any);