import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {FontIcon, Paper, RaisedButton} from 'material-ui';
import runOnlyPendingTimers = jest.runOnlyPendingTimers;
import {AppState} from '../../../../model/AppState';
import {FileUploadStatus} from '../../../../model/FileUploadStatus';
import {AppActionCreator} from '../../../../reducer/app/AppActionCreator';
import {ReportListTree} from '../../upload/tree/report-list-tree_module';
import {SelectedNodeInfo} from '../../upload/tree/selected-node-info_module';
import {ReportListSorter} from '../../upload/report-list-sorter_module';
import {ReportUploader} from '../../upload/report-uploader_module';
import {FileUploadQueue} from '../../upload/file-upload-queue_module';

interface SingleTeamReportListProps {
    currentTeamId: number;
    queueHasFiles: boolean;
    fileIsUploading: boolean;
    currentUserHasUploadPermission: boolean;
}

interface SingleTeamReportListLocalProps {

}

interface SingleTeamReportListLocalState {

}

interface SingleTeamReportListDispatch {
    onRequestUpdate(teamId: number): void;
}

class SingleTeamReportListModule extends React.Component<SingleTeamReportListProps & SingleTeamReportListLocalProps & SingleTeamReportListDispatch, SingleTeamReportListLocalState> {

    constructor(props: SingleTeamReportListProps & SingleTeamReportListLocalProps & SingleTeamReportListDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: SingleTeamReportListLocalProps): SingleTeamReportListProps {
        return {
            queueHasFiles: state.appStore.filesToUpload().count() > 0,
            fileIsUploading: state.appStore.fileUploadStatus() == FileUploadStatus.UPLOADING,
            currentTeamId: state.teams.currentTeamId(),
            currentUserHasUploadPermission: state.teams.hasPermissionInCurrentTeam(state.appStore.username(), "UPLOAD")
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SingleTeamReportListDispatch {
        return {
            onRequestUpdate: (teamId) => dispatch(AppActionCreator.AsyncRequestAllReportFiles(teamId))
        }
    }

    public componentDidMount() {
        this.props.onRequestUpdate(this.props.currentTeamId);
    }


    render() {
        return (<div>
            <div className="row">
                <div className="col-md-2">
                    <Paper style={{backgroundColor: '#21252B', color: 'white', padding: '30px', borderRadius: '20px'}}>
                        <SelectedNodeInfo
                            isShared={false}
                            showDeleteButton={true}
                            currentTeamId={this.props.currentTeamId}
                        />
                    </Paper>
                </div>
                <div className="col-md-7" style={{marginBottom: "16px"}}>
                    <ReportListTree
                        isShared={false}
                        showShareButton={true}
                        currentTeamId={this.props.currentTeamId}
                        showDeleteButton={true}
                        onRefreshReports={() => this.props.onRequestUpdate(this.props.currentTeamId)}
                    />
                </div>
                <div className="col-md-3" >
                    <div style={{width: '100%', marginTop: '20px'}}>
                        <ReportListSorter/>
                    </div>
                    <div className="Aligner" >
                        {
                            this.props.currentUserHasUploadPermission ? <ReportUploader/> : null
                        }
                    </div>
                    <div className="Aligner">
                        {
                            this.props.queueHasFiles || this.props.fileIsUploading ? <FileUploadQueue/> : null
                        }
                    </div>
                </div>
            </div>
        </div>);
    }
}

/**
 * @see SingleTeamReportListModule
 * @author Niklas
 * @since 27.10.2017
 */
export const SingleTeamReportList: React.ComponentClass<SingleTeamReportListLocalProps> = connect(SingleTeamReportListModule.mapStateToProps, SingleTeamReportListModule.mapDispatchToProps)(SingleTeamReportListModule);