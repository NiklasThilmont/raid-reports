import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {Dialog, FontIcon, RaisedButton, TextField} from 'material-ui';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
import {Config} from '../../../../util/Config';
import {ShareIcon} from '../../../icons/MaterialIcons';

interface ShareTeamDialogProps {
    raidTeam: RaidTeam;
}

interface ShareTeamDialogLocalProps {
    open: boolean;
    onClose(): void;
}

interface ShareTeamDialogLocalState {

}

interface ShareTeamDialogDispatch {
    shareTeam(teamId: number): void;
    revokeShareTeam(teamId: number): void;
}

class ShareTeamDialogModule extends React.Component<ShareTeamDialogProps & ShareTeamDialogLocalProps & ShareTeamDialogDispatch, ShareTeamDialogLocalState> {

    constructor(props: ShareTeamDialogProps & ShareTeamDialogLocalProps & ShareTeamDialogDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: ShareTeamDialogLocalProps): ShareTeamDialogProps {
        return {
            raidTeam: state.teams.getTeamById(state.teams.currentTeamId())
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ShareTeamDialogDispatch {
        return {
            shareTeam: teamId => dispatch(RaidTeamActionCreator.AsyncShareTeam(teamId)),
            revokeShareTeam: teamId => dispatch(RaidTeamActionCreator.AsyncRevokeShareTeam(teamId))
        }
    }

    private handleShareTeam = () => {
        this.props.shareTeam(this.props.raidTeam.id());
    };

    private handleRevokeShareTeam = () => {
        this.props.revokeShareTeam(this.props.raidTeam.id());
    };

    private renderShared = () => {
        console.debug("raidTeam", this.props.raidTeam);
        return <div>
            <div className="Aligner">
                Your team is <span style={{color: "#97ffbc"}}>&nbsp;shared</span><br/><br/>
            </div>
            <div className="Aligner">
                When your team is shared, anyone with the following link can view your uploaded reports.<br/><br/>
            </div>
            <div className="Aligner">
                <TextField
                    floatingLabelText="Share URL"
                    fullWidth={true}
                    value={Config.makeShareUrl(this.props.raidTeam.shareId())}
                />
            </div>
            <div className="Aligner">
                <RaisedButton
                    secondary={true}
                    label="Revoke Share"
                    onClick={this.handleRevokeShareTeam}
                />
            </div>
        </div>
    };

    private renderNotShared = () => {
        return <div>
            <div className="Aligner">
                Share your team<br/><br/>
            </div>
            <div className="Aligner">
                When your team is shared, anyone with the following link can view your uploaded reports.<br/><br/>
            </div>
            <div className="Aligner">
                <RaisedButton
                    primary={true}
                    label="Share"
                    onClick={this.handleShareTeam}
                    icon={<FontIcon className={"material-icons"}>share</FontIcon>}
                />
            </div>
        </div>
    };

    render() {
        return (<Dialog open={this.props.open} onRequestClose={this.props.onClose}>
            {this.props.raidTeam.shared() ? this.renderShared() : this.renderNotShared()}
        </Dialog>);
    }
}

/**
 * @see ShareTeamDialogModule
 * @author Niklas
 * @since 26.10.2017
 */
export const ShareTeamDialog: React.ComponentClass<ShareTeamDialogLocalProps> = connect(ShareTeamDialogModule.mapStateToProps, ShareTeamDialogModule.mapDispatchToProps)(ShareTeamDialogModule);