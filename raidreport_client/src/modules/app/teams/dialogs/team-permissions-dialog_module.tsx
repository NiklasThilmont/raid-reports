import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {Dialog, Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui';
import {RaidTeamUser} from '../../../../model/team/RaidTeamUser';
import {RaidTeam} from '../../../../model/team/RaidTeam';
import {User} from '../../../../model/user/User';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
const Toggle = require('react-toggle').default;
require("react-toggle/style.css");

interface TeamPermissionsDialogProps {
    raidTeam: RaidTeam;
    loggedInUserName: string;
    loggedInRaidUser: RaidTeamUser;
}

interface TeamPermissionsDialogLocalProps {
    open: boolean;
    onRequestClose(): void;
}

interface TeamPermissionsDialogLocalState {

}

interface TeamPermissionsDialogDispatch {
    grantPermission(teamId: number, userId: number, permission: string): void;
    revokePermission(teamId: number, userId: number, permission: string): void;
}

class TeamPermissionsDialogModule extends React.Component<TeamPermissionsDialogProps & TeamPermissionsDialogLocalProps & TeamPermissionsDialogDispatch, TeamPermissionsDialogLocalState> {

    constructor(props: TeamPermissionsDialogProps & TeamPermissionsDialogLocalProps & TeamPermissionsDialogDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: TeamPermissionsDialogLocalProps): TeamPermissionsDialogProps {
        let raidTeam = state.teams.getTeamById(state.teams.currentTeamId());
        let loggedInUserName = state.appStore.username();
        return {
            raidTeam: raidTeam,
            loggedInUserName: loggedInUserName,
            loggedInRaidUser: raidTeam.getUser(loggedInUserName)
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): TeamPermissionsDialogDispatch {
        return {
            revokePermission: (teamId, userId, permission) => dispatch(RaidTeamActionCreator.AsyncRevokePermission(teamId, userId, permission)),
            grantPermission: (teamId, userId, permission) => dispatch(RaidTeamActionCreator.AsyncGrantPermission(teamId, userId, permission))
        };
    }

    private makeRevokeOrGrantHandler(permission: string, userId: number, teamId: number) {
        return (e: any) => {
            if(e.target.checked) {
                this.props.grantPermission(teamId, userId, permission)
            } else {
                this.props.revokePermission(teamId, userId, permission)
            }
        }
    }

    private permissionToggle = (user: RaidTeamUser, permission: string, hasManagePermission: boolean) => {
        return <Toggle
            checked={user.hasPermission(permission)}
            disabled={!hasManagePermission}
            onChange={this.makeRevokeOrGrantHandler(permission, user.user().id(), this.props.raidTeam.id())}
        />;
    };

    private mapUser = (user: RaidTeamUser) => {
        let hasManagePermission = false;
        if(this.props.loggedInRaidUser.hasPermission("MANAGE_PERMISSION")) {
            hasManagePermission = true;
        }
        return <TableRow key={user.user().name()}>
            <TableRowColumn>{user.user().name()}</TableRowColumn>
            <TableRowColumn>{this.permissionToggle(user, "INVITE", hasManagePermission)}</TableRowColumn>
            <TableRowColumn>{this.permissionToggle(user, "KICK", hasManagePermission)}</TableRowColumn>
            <TableRowColumn>{this.permissionToggle(user, "UPLOAD", hasManagePermission)}</TableRowColumn>
            <TableRowColumn>{this.permissionToggle(user, "DELETE_REPORT", hasManagePermission)}</TableRowColumn>
            <TableRowColumn>{this.permissionToggle(user, "MANAGE_PERMISSION", hasManagePermission)}</TableRowColumn>
        </TableRow>
    };

    render() {
        return (<Dialog open={this.props.open} onRequestClose={this.props.onRequestClose}>
            <Table
                selectable={false}
            >
                <TableHeader
                    displaySelectAll={false}
                    adjustForCheckbox={false}
                >
                    <TableRow>
                        <TableHeaderColumn>User</TableHeaderColumn>
                        <TableHeaderColumn>INVITE</TableHeaderColumn>
                        <TableHeaderColumn>KICK</TableHeaderColumn>
                        <TableHeaderColumn>UPLOAD</TableHeaderColumn>
                        <TableHeaderColumn>DELETE_REPORT</TableHeaderColumn>
                        <TableHeaderColumn>MANAGE_PERMISSION</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody
                    displayRowCheckbox={false}
                >
                    {this.props.raidTeam.users().map(this.mapUser)}
                </TableBody>
            </Table>
        </Dialog>);
    }
}

/**
 * @see TeamPermissionsDialogModule
 * @author Niklas
 * @since 28.10.2017
 */
export const TeamPermissionsDialog: React.ComponentClass<TeamPermissionsDialogLocalProps> = connect(TeamPermissionsDialogModule.mapStateToProps, TeamPermissionsDialogModule.mapDispatchToProps)(TeamPermissionsDialogModule);