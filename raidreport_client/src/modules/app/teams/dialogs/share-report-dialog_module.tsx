import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {ReportFile} from '../../../../model/ReportFile';
import {ReportFileLeaf} from '../../../../model/ReportFileTree';
import {RaidTeamActionCreator} from '../../../../reducer/teams/RaidTeamActionCreator';
import {Dialog, FontIcon, RaisedButton, TextField} from 'material-ui';
import {Config} from '../../../../util/Config';
import {RaidTeam} from '../../../../model/team/RaidTeam';

interface ShareReportDialogProps {
    reportFile: ReportFile;
    currenTeam: RaidTeam;
}

interface ShareReportDialogLocalProps {
    open: boolean;
    onClose(): void;
}

interface ShareReportDialogLocalState {

}

interface ShareReportDialogDispatch {
    shareReport(reportId: number, currentTeamId: number): void;
    unShareReport(reportId: number, currentTeamId: number): void;
}

class ShareReportDialogModule extends React.Component<ShareReportDialogProps & ShareReportDialogLocalProps & ShareReportDialogDispatch, ShareReportDialogLocalState> {

    constructor(props: ShareReportDialogProps & ShareReportDialogLocalProps & ShareReportDialogDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: ShareReportDialogLocalProps): ShareReportDialogProps {
        let reportFile = null;
        if(state.appStore.currentNode() && state.appStore.currentNode().type === "leaf") {
            reportFile = (state.appStore.currentNode() as ReportFileLeaf).reportFile;
        }
        return {
            reportFile: reportFile,
            currenTeam: state.teams.getTeamById(state.teams.currentTeamId())
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ShareReportDialogDispatch {
        return {
            shareReport: (reportId, currentTeamId) => dispatch(RaidTeamActionCreator.AsyncShareReport(currentTeamId, reportId)),
            unShareReport: (reportId, currentTeamId) => dispatch(RaidTeamActionCreator.AsyncUnShareReport(currentTeamId, reportId))
        }
    }



    private handleShareTeam = () => {
        this.props.shareReport(this.props.reportFile.id(), this.props.currenTeam.id());
    };

    private handleRevokeShareTeam = () => {
        this.props.unShareReport(this.props.reportFile.id(), this.props.currenTeam.id());
    };

    private renderShared = () => {
        return <div>
            <div className="Aligner">
                This report is <span style={{color: "#97ffbc"}}>&nbsp;shared</span><br/><br/>
            </div>
            <div className="Aligner">
                When your report is shared, anyone with the following link can view it.<br/><br/>
            </div>
            <div className="Aligner">
                <TextField
                    floatingLabelText="Share URL"
                    fullWidth={true}
                    value={Config.makeReportShareUrl(this.props.reportFile.shareId())}
                />
            </div>
            <div className="Aligner">
                <RaisedButton
                    secondary={true}
                    label="Revoke Share"
                    onClick={this.handleRevokeShareTeam}
                />
            </div>
        </div>
    };

    private renderNotShared = () => {
        return <div>
            <div className="Aligner">
                Share your team<br/><br/>
            </div>
            <div className="Aligner">
                When your report is shared, anyone with the following link can view it.<br/><br/>
            </div>
            <div className="Aligner">
                <RaisedButton
                    primary={true}
                    label="Share"
                    onClick={this.handleShareTeam}
                    icon={<FontIcon className={"material-icons"}>share</FontIcon>}
                />
            </div>
        </div>
    };

    render() {
        console.debug("reportFile", this.props.reportFile);
        if(!this.props.reportFile) {
            return <div/>
        }
        return (<Dialog open={this.props.open} onRequestClose={this.props.onClose}>
            {this.props.reportFile.shared() ? this.renderShared() : this.renderNotShared()}
        </Dialog>);
    }
}

/**
 * @see ShareReportDialogModule
 * @author Niklas
 * @since 05.11.2017
 */
export const ShareReportDialog: React.ComponentClass<ShareReportDialogLocalProps> = connect(ShareReportDialogModule.mapStateToProps, ShareReportDialogModule.mapDispatchToProps)(ShareReportDialogModule);