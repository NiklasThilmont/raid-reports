import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {RaidTeam} from '../../../model/team/RaidTeam';
import {RaidTeamActionCreator} from '../../../reducer/teams/RaidTeamActionCreator';
import {FontIcon, RaisedButton} from 'material-ui';
import {Link} from 'react-router-dom';
import {CreateTeamStatus} from '../../../model/team/CreateTeamStatus';
import {RaidTeamCreateDialog} from './raid-team-create-dialog_module';
import {isNullOrUndefined} from 'util';
import {RaidTeamUser} from '../../../model/team/RaidTeamUser';
import {RouteConfig} from '../../../util/Config';
import {NewsFeedModule} from '../news/newsfeed-module';

interface RaidTeamDashboardProps {
    raidTeams: Array<RaidTeam>;
    loggedIn: boolean;
    loggedInUserName: string;
}

interface RaidTeamDashboardLocalProps {

}

interface RaidTeamDashboardLocalState {

}

interface RaidTeamDashboardDispatch {
    replaceAllRaidTeams(): void;
    selectTeam(id: number): void;
    openTeamCreation(): void;
    acceptInvite(teamId: number): void;
    declineInvite(teamId: number): void;
}

class RaidTeamDashboardModule extends React.Component<RaidTeamDashboardProps & RaidTeamDashboardLocalProps & RaidTeamDashboardDispatch, RaidTeamDashboardLocalState> {

    constructor(props: RaidTeamDashboardProps & RaidTeamDashboardLocalProps & RaidTeamDashboardDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: RaidTeamDashboardLocalProps): RaidTeamDashboardProps {
        return {
            raidTeams: state.teams.raidTeams().toArray(),
            loggedIn: state.appStore.isLoggedIn(),
            loggedInUserName: state.appStore.username()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RaidTeamDashboardDispatch {
        return {
            replaceAllRaidTeams: () => dispatch(RaidTeamActionCreator.AsyncLoadAllTeams()),
            selectTeam: id => dispatch(RaidTeamActionCreator.AsyncSelectTeam(id)),
            openTeamCreation: () => dispatch(RaidTeamActionCreator.SetCreateTeamStatus(CreateTeamStatus.DIALOG_OPEN)),
            acceptInvite: teamId => dispatch(RaidTeamActionCreator.AsyncAcceptInvite(teamId)),
            declineInvite: teamId => dispatch(RaidTeamActionCreator.AsyncRejectInvite(teamId))
        }
    }

    private mapSingleTeam = (raidTeam: RaidTeam) => {
        const raidTeamUser = raidTeam.getUser(this.props.loggedInUserName);
        const isInvited = !isNullOrUndefined(raidTeamUser) && raidTeamUser.teamInviteStatus() === RaidTeamUser.INVITE_STATUS_INVITE_SENT;
        return <div key={raidTeam.id()} className="col-md-4">
            <div className="General-Container Team-Dashboard-Element">
                {isInvited ? <h4 className="Invite-Pending-Header">Invite Pending</h4> : null}
                <Link to={RouteConfig.ROUTE_TEAM_OVERVIEW.replace(":id", raidTeam.id().toString())} className={isInvited ? "Disabled-link" : ""}>
                    <div className="Pointer" onClick={() => this.props.selectTeam(raidTeam.id())}>
                        <h1>Raid Team</h1>
                        <h3>{raidTeam.teamName()}</h3>
                    </div>
                </Link>
                {isInvited ?
                    <div>
                        <RaisedButton
                            className="Margined-Button"
                            label="Accept Invite"
                            primary={true}
                            onClick={() => this.props.acceptInvite(raidTeam.id())}
                        />
                        <RaisedButton
                            className="Margined-Button"
                            label="Decline Invite"
                            secondary={true}
                            onClick={() => this.props.declineInvite(raidTeam.id())}
                        />
                    </div>
                     : null}
            </div>
        </div>
    };

    render() {
        if(!this.props.loggedIn) {
            return <NewsFeedModule/>
        }
        return (<div >

            <div className="clearfix">
                {this.props.raidTeams.map(this.mapSingleTeam)}
            </div>
            <div className="clearfix">
                <div className="col-md-3">
                    <div className="General-Container Team-Dashboard-Element">
                        <RaidTeamCreateDialog/>
                        <RaisedButton
                            primary={true}
                            label="Create a Raid Team"
                            icon={<FontIcon className="material-icons">add</FontIcon>}
                            onClick={this.props.openTeamCreation}
                        />
                    </div>
                </div>
            </div>
        </div>);
    }
}

/**
 * @see RaidTeamDashboardModule
 * @author Niklas
 * @since 15.10.2017
 */
export const RaidTeamDashboard: React.ComponentClass<RaidTeamDashboardLocalProps> = connect(RaidTeamDashboardModule.mapStateToProps, RaidTeamDashboardModule.mapDispatchToProps)(RaidTeamDashboardModule);