
import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {RaidBoss} from '../../../model/RaidBoss';
import {Dialog, List, TextField} from 'material-ui';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import * as Immutable from "immutable";

interface SingleRaidBossDialogProps {
    boss: RaidBoss;

}

interface SingleRaidBossDialogLocalProps {
    bossId: number;
    open: boolean;
    onRequestClose(): void;
}

interface SingleRaidBossDialogLocalState {
    bossIdError: string;
}

interface SingleRaidBossDialogDispatch {
    updateBoss(boss: RaidBoss): void;
}

class SingleRaidBossDialogModule extends React.Component<SingleRaidBossDialogProps & SingleRaidBossDialogLocalProps & SingleRaidBossDialogDispatch, SingleRaidBossDialogLocalState> {

    constructor(props: SingleRaidBossDialogProps & SingleRaidBossDialogLocalProps & SingleRaidBossDialogDispatch) {
        super(props);
        this.state = {
            bossIdError: null
        }

    }

    static mapStateToProps(state: AppState, localProps: SingleRaidBossDialogLocalProps): SingleRaidBossDialogProps {
        return {
            boss: state.appStore.bosses().get(localProps.bossId)
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SingleRaidBossDialogDispatch {
        return {
            updateBoss: boss => dispatch(AppActionCreator.UpdateBoss(boss))
        }
    }

    private handleBossNameChange = (event: any, value: string) => {
        this.props.updateBoss(this.props.boss.name(value));
    };

    private areValidBossIds = (ids: Array<number>) => {
        return ids.every(Number.isInteger);
    };

    private handleBossIdChange = (event: any, value: string) => {
        let ids = value.split(",");
        let idsAsNumber = ids.map(idString => Number.parseInt(idString));
        this.props.updateBoss(this.props.boss.classIds(Immutable.List<number>(idsAsNumber)));
        let errorText = null;
        if(!this.areValidBossIds(idsAsNumber)) {
            errorText = "Boss IDs are invalid!";
        }
        this.setState({
            bossIdError: errorText
        })
    };

    render() {
        if(this.props.boss) {
            return <div>
                <Dialog open={this.props.open}
                        title={"Change Data for " + this.props.boss.name()}
                        onRequestClose={this.props.onRequestClose}>
                    Changing the boss name will require a re-import of all reports.
                    This is currently not handled by the backend. Please use this feature only in communication
                    with a developer.
                    <div className="Full-Width">
                        <TextField value={this.props.boss.name()}
                                   floatingLabelText="Boss Name"
                                   onChange={this.handleBossNameChange}
                        />
                    </div>
                    <div className="Full-Width">
                        <TextField defaultValue={this.props.boss.classIds().join(",")}
                                   floatingLabelText="Boss IDs"
                                   errorText={this.state.bossIdError}
                                   onChange={this.handleBossIdChange}
                        />
                    </div>
                </Dialog>
            </div>
        } else {
            return <div/>
        }

    }
}

/**
 * @see SingleRaidBossDialogModule
 * @author Niklas
 * @since 03.01.2018
 */
export const SingleRaidBossDialog: React.ComponentClass<SingleRaidBossDialogLocalProps> = connect(SingleRaidBossDialogModule.mapStateToProps, SingleRaidBossDialogModule.mapDispatchToProps)(SingleRaidBossDialogModule);