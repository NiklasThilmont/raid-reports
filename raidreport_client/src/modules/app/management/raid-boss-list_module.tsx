import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {List} from 'immutable/dist/immutable-nonambient';
import {RaidBoss} from '../../../model/RaidBoss';
import {SingleRaidBossDialog} from './single-raid-boss-dialog_module';
import {
    IconButton, ListItem, RaisedButton, Table, TableBody, TableHeader, TableHeaderColumn,
    TableRow
} from 'material-ui';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';

interface RaidBossListProps {
    bosses: Array<RaidBoss>;
}

interface RaidBossListLocalProps {
}

interface RaidBossListLocalState {
    currentBossId: number;
}

interface RaidBossListDispatch {
    retrieveAllBosses(): void;
    createRaidBoss(boss: RaidBoss): void;
    saveBoss(boss: RaidBoss): void;
    deleteBoss(bossId: number): void;
}

class RaidBossListModule extends React.Component<RaidBossListProps & RaidBossListLocalProps & RaidBossListDispatch, RaidBossListLocalState> {

    private static COL_SPAN_ID = 56;
    private static COL_SPAN_NAME = 200;
    private static COL_SPAN_IDS = 200;
    private static COL_SPAN_BTN_EDIT = 56;
    private static COL_SPAN_BTN_SAVE = 56;

    constructor(props: RaidBossListProps & RaidBossListLocalProps & RaidBossListDispatch) {
        super(props);
        this.state = {
            currentBossId: null
        }
    }

    static mapStateToProps(state: AppState, localProps: RaidBossListLocalProps): RaidBossListProps {
        return {
            bosses: state.appStore.bosses().toArray()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): RaidBossListDispatch {
        return {
            retrieveAllBosses: () => dispatch(AppActionCreator.AsyncRetrieveAllBosses()),
            saveBoss: boss => dispatch(AppActionCreator.AsyncSaveBoss(boss)),
            deleteBoss: bossId => dispatch(AppActionCreator.AsyncDeleteBoss(bossId)),
            createRaidBoss: boss => dispatch(AppActionCreator.UpdateBoss(boss))
        };
    }

    public componentDidMount() {
        this.props.retrieveAllBosses();
    }

    private closeDialog = () => {
        this.setState({
            currentBossId: null
        })
    };

    private makeOpenBossHandler = (bossId: number) => {
      return () => {
          this.setState({
              currentBossId: bossId
          })
      }
    };

    private makeSaveBossHandler = (boss: RaidBoss) => {
        return () => {
            this.props.saveBoss(boss);
        }
    };

    private makeDeleteBossHandler = (bossId: number) => {
        return () => {
            this.props.deleteBoss(bossId);
        }
    };

    private addBoss = () => {
        let raidBoss = RaidBoss.fromAPI({
            name: "",
            id: -1,
            classIds: [],
            order: 0
        });
        this.props.createRaidBoss(raidBoss);
        this.setState({
            currentBossId: -1
        });
    };

    private mapBoss = (boss: RaidBoss) => {
        return <TableRow key={boss.id()}>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_ID}>{boss.id()}</TableHeaderColumn>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_NAME}>{boss.name()}</TableHeaderColumn>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_IDS}>{boss.classIds().toArray().join(", ")}</TableHeaderColumn>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_EDIT}>
                <IconButton iconClassName="material-icons" onTouchTap={this.makeOpenBossHandler(boss.id())}>edit</IconButton>
            </TableHeaderColumn>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_SAVE}>
                <IconButton iconClassName="material-icons" onTouchTap={this.makeSaveBossHandler(boss)}>save</IconButton>
            </TableHeaderColumn>
            <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_SAVE}>
                <IconButton iconClassName="material-icons" onTouchTap={this.makeDeleteBossHandler(boss.id())}>delete</IconButton>
            </TableHeaderColumn>
        </TableRow>
    };

    private renderBosses = () => {
        return <Table>
                <TableHeader displaySelectAll={false}
                             adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_ID}>ID</TableHeaderColumn>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_NAME}>Name</TableHeaderColumn>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_IDS}>Class-Ids</TableHeaderColumn>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_EDIT}/>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_SAVE}/>
                        <TableHeaderColumn colSpan={RaidBossListModule.COL_SPAN_BTN_SAVE}/>
                    </TableRow>
                </TableHeader>
            <TableBody  displayRowCheckbox={false}>
                {this.props.bosses.map(this.mapBoss)}
            </TableBody>
        </Table>
    };

    render() {
        return (
            <div>
                <div className="General-Container" style={{margin: "8px"}}>
                    <h4>Raid Boss Management </h4>
                    <span>From now on, raid bosses are stored as entities in the database. This allows for a more flexible
                        approach on handling new bosses, and displaying their names. For now, this does not affect old reports,
                        as they store the boss name directly as a field, instead of referencing the boss entity.
                        <br/>
                        As a result, changing of names will not affect any old reports. Until data migration is done, boss
                        names should not be changed either. Notice will be given once that feature is ready to use!
                        <br/>
                        To Add a new boss, press the "add" button. Bosses changes are not automatically saved. To save a boss,
                        please use the "save" button next to the boss row. This is done as a precaution, so that no accidental
                        changes happen.
                    </span>
                    <br/>
                    <RaisedButton className="Margined-Button" primary={true} label="Reset Changes" onClick={this.props.retrieveAllBosses}/>
                    <RaisedButton className="Margined-Button" primary={true} label="Add Boss" onClick={this.addBoss}/>
                    <SingleRaidBossDialog bossId={this.state.currentBossId}
                                          open={this.state.currentBossId != null}
                                          onRequestClose={this.closeDialog}/>
                    <div className="row">
                        <div className="col-md-6">
                            {this.renderBosses()}
                        </div>
                    </div>

                </div>
            </div>
            );
    }
}

/**
 * @see RaidBossListModule
 * @author Niklas
 * @since 03.01.2018
 */
export const RaidBossList: React.ComponentClass<RaidBossListLocalProps> = connect(RaidBossListModule.mapStateToProps, RaidBossListModule.mapDispatchToProps)(RaidBossListModule);