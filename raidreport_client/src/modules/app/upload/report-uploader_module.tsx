import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {isNullOrUndefined} from 'util';
import {FontIcon, MenuItem, Paper, RaisedButton, SelectField, TouchTapEvent} from 'material-ui';
import {Config} from '../../../util/Config';
import {FileUploadStatus} from '../../../model/FileUploadStatus';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import {Simulate} from 'react-dom/test-utils';
import {CSSProperties} from 'react';

const Dropzone = require('react-dropzone').default;



interface ReportUploaderProps {
    completion: number;
    uploadStatus: FileUploadStatus;
    currentTeamId: number;
}

interface ReportUploaderLocalProps {

}

interface ReportUploaderLocalState {
    file: File;
}

interface ReportUploaderDispatch {
    addFileToQueue(file: File, teamId: number): void;
}

class ReportUploaderModule extends React.Component<ReportUploaderProps & ReportUploaderLocalProps & ReportUploaderDispatch, ReportUploaderLocalState> {

    static mapStateToProps(state: AppState, localProps: ReportUploaderLocalProps): ReportUploaderProps {
        return {
            uploadStatus: state.appStore.fileUploadStatus(),
            completion: isNullOrUndefined(state.appStore.uploadingFile()) ? 0 : state.appStore.uploadingFile().uploadStatus(),
            currentTeamId: state.teams.currentTeamId()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ReportUploaderDispatch {
        return {
            addFileToQueue: (file, teamId) => dispatch(AppActionCreator.AsyncAddFileToQueue(file, teamId))
        }
    }



    private msg: any;

    constructor(props: ReportUploaderProps & ReportUploaderLocalProps & ReportUploaderDispatch) {
        super(props);
        this.state = {
            file: null,
        };
    }



    private invokeFileUpload = () => {
        this.props.addFileToQueue(this.state.file, this.props.currentTeamId);
        this.setState({
             file: null,
        })
    };

    private handleFileDrop = (acceptedFiles: Array<File>, rejectedFiles: Array<File>) => {
        if(acceptedFiles.length > 0) {
            this.setState({
                file: acceptedFiles[0]
            });
        }
    };





    render() {return (
            <div className="Report-Uploader">
                <div  style={{width: '100%'}}>
                    <div className="Aligner">
                        <Dropzone
                            onDrop={this.handleFileDrop}
                            multiple={false}
                            accept=".evtc"
                        >
                            <div className="Aligner" style={{height: '100%', textAlign: 'center'}}>
                                {!isNullOrUndefined(this.state.file) ?
                                    <span>
                                        Selected File:<br/>
                                        {this.state.file.name}
                                    </span> :
                                    <div className="Aligner"> <FontIcon className="material-icons" color="white" style={{fontSize: 70}}>file_upload</FontIcon></div>}
                            </div>
                        </Dropzone>
                    </div>

                    <div className="Aligner">
                        <RaisedButton
                            style={{marginTop: '20px'}}
                            label="Upload"
                            secondary={true}
                            disabled={isNullOrUndefined(this.state.file)}
                            onClick={this.invokeFileUpload}
                            icon={<FontIcon className="material-icons" >file_upload</FontIcon>}
                        />
                    </div>
                </div>
            </div>
       )};
}

/**
 * @see ReportUploaderModule
 * @author Niklas
 * @since 02.07.2017
 */
export const ReportUploader: React.ComponentClass<ReportUploaderLocalProps> = connect(ReportUploaderModule.mapStateToProps, ReportUploaderModule.mapDispatchToProps)(ReportUploaderModule);