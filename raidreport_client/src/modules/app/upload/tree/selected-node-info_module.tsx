import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {isNullOrUndefined} from 'util';
import {ReportFileLeaf, TreeNode} from '../../../../model/ReportFileTree';
import {StringUtils} from '../../../../util/StringUtils';
import {FontIcon, RaisedButton} from 'material-ui';
import {Config} from '../../../../util/Config';
import {ConfirmDialog} from '../../general/confirm-dialog_module';
import {AppActionCreator} from '../../../../reducer/app/AppActionCreator';
import {RaidParticipantsInfo} from '../../teams/singleteam/raid-participants-info_module';

interface SelectedNodeInfoProps {
    currentNode: TreeNode;
    currentUserName: string;
    currentUserHasDeletePermission: boolean;
}

interface SelectedNodeInfoLocalProps {
    isShared: boolean;
    currentTeamId?: number | string;
    showDeleteButton?: boolean;
}

interface SelectedNodeInfoLocalState {

}

interface SelectedNodeInfoDispatch {
    setCurrentNode(node: TreeNode): void;
}

class SelectedNodeInfoModule extends React.Component<SelectedNodeInfoProps & SelectedNodeInfoLocalProps & SelectedNodeInfoDispatch, SelectedNodeInfoLocalState> {

    public static defaultProps: Partial<SelectedNodeInfoProps & SelectedNodeInfoLocalProps & SelectedNodeInfoDispatch> = {
        currentTeamId: -1,
        showDeleteButton: false,
    };

    constructor(props: SelectedNodeInfoProps & SelectedNodeInfoLocalProps & SelectedNodeInfoDispatch) {
        super(props);

    }

    static mapStateToProps(state: AppState, localProps: SelectedNodeInfoLocalProps): SelectedNodeInfoProps {
        return {
            currentNode: state.appStore.currentNode(),
            currentUserName: state.appStore.username(),
            currentUserHasDeletePermission: state.teams.hasPermissionInCurrentTeam(state.appStore.username(), "DELETE_REPORT")
        }
    }


    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): SelectedNodeInfoDispatch {
        return {

            setCurrentNode: node => dispatch(AppActionCreator.SetCurrentNode(node))
        }
    }

    render() {
        if(!isNullOrUndefined(this.props.currentNode) && this.props.currentNode.type == 'leaf') {
            let leaf = this.props.currentNode as ReportFileLeaf;
            return(
                <div style={{width: '100%'}}>
                    <div className="ReportInfoElement">
                        Uploader: {leaf.reportFile.uploaderName()}
                    </div>
                    <div className="ReportInfoElement">
                        Boss: {leaf.reportFile.boss().name}
                    </div>
                    <div className="ReportInfoElement">
                        {StringUtils.extractKillStatString(leaf.reportFile)}
                    </div>
                    <div className="ReportInfoElement" style={{marginTop: "16px", marginBottom: "16px"}}>
                        Participants
                    </div>
                    <RaidParticipantsInfo/>
                </div>);
        }
        return <div>No valid file selected</div>;
    }
}

/**
 * @see SelectedNodeInfoModule
 * @author Niklas
 * @since 27.10.2017
 */
export const SelectedNodeInfo: React.ComponentClass<SelectedNodeInfoLocalProps> = connect(SelectedNodeInfoModule.mapStateToProps, SelectedNodeInfoModule.mapDispatchToProps)(SelectedNodeInfoModule);