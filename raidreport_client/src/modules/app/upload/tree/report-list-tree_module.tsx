import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../../model/AppState';
import {ReportFileLeaf, ReportFileTree, TreeNode} from '../../../../model/ReportFileTree';
import {AppActionCreator} from '../../../../reducer/app/AppActionCreator';
import {FontIcon, RaisedButton} from 'material-ui';
import {Config} from '../../../../util/Config';
import {ConfirmDialog} from '../../general/confirm-dialog_module';
import {ShareReportDialog} from '../../teams/dialogs/share-report-dialog_module';
const Treebeard = require('react-treebeard');


interface ReportListTreeProps {
    treeRoot: ReportFileTree;
    currentNode: TreeNode;
    currentUserName: string;
    currentUserHasDeletePermission: boolean;
}

interface ReportListTreeLocalProps {
    isShared: boolean;
    currentTeamId?: number | string;
    showDeleteButton?: boolean;
    showShareButton?: boolean;
    onRefreshReports(): void;
}

interface ReportListTreeLocalState {
    confirmOpen: boolean;
    shareReportOpen: boolean;
}

interface ReportListTreeDispatch {
    setTreeRoot(root: ReportFileTree): void;
    setCurrentNode(node: TreeNode): void;
    deleteReportFile(teamId: number, reportId: number): void;
}

class ReportListTreeModule extends React.Component<ReportListTreeProps & ReportListTreeLocalProps & ReportListTreeDispatch, ReportListTreeLocalState> {

    public static defaultProps: Partial<ReportListTreeProps & ReportListTreeLocalProps & ReportListTreeDispatch> = {
        currentTeamId: -1,
        showDeleteButton: false,
        showShareButton: false
    };

    constructor(props: ReportListTreeProps & ReportListTreeLocalProps & ReportListTreeDispatch) {
        super(props);
        this.state = {
            confirmOpen: false,
            shareReportOpen: false
        }
    }

    static mapStateToProps(state: AppState, localProps: ReportListTreeLocalProps): ReportListTreeProps {
        return {
            treeRoot: state.appStore.fileTreeRoot(),
            currentNode: state.appStore.currentNode(),
            currentUserName: state.appStore.username(),
            currentUserHasDeletePermission: state.teams.hasPermissionInCurrentTeam(state.appStore.username(), "DELETE_REPORT")
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ReportListTreeDispatch {
        return {
            setTreeRoot: root => dispatch(AppActionCreator.SetFileTreeRoot(root)),
            setCurrentNode: node => dispatch(AppActionCreator.SetCurrentNode(node)),
            deleteReportFile: (teamId, reportId) => dispatch(AppActionCreator.AsyncDeleteReportFile(teamId, reportId))
        };
    }

    private readonly treeStyle =  {
        tree: {
            base: {
                listStyle: 'none',
                backgroundColor: '#21252B',
                margin: 0,
                padding: 30,
                borderRadius: '20px',
                color: '#9DA5AB',
                fontFamily: 'robot, sans-serif',
                fontSize: '14px'
            },
            node: {
                base: {
                    position: 'relative'
                },
                link: {
                    cursor: 'pointer',
                    position: 'relative',
                    padding: '0px 5px',
                    display: 'block'
                },
                activeLink: {
                    background: '#31363F'
                },
                toggle: {
                    base: {
                        position: 'relative',
                        display: 'inline-block',
                        verticalAlign: 'top',
                        marginLeft: '-5px',
                        height: '24px',
                        width: '24px'
                    },
                    wrapper: {
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        margin: '-7px 0 0 -7px',
                        height: '14px'
                    },
                    height: 14,
                    width: 14,
                    arrow: {
                        fill: '#9DA5AB',
                        strokeWidth: 0
                    }
                },
                header: {
                    base: {
                        display: 'inline-block',
                        verticalAlign: 'top',
                        color: '#9DA5AB'
                    },
                    connector: {
                        width: '2px',
                        height: '12px',
                        borderLeft: 'solid 2px black',
                        borderBottom: 'solid 2px black',
                        position: 'absolute',
                        top: '0px',
                        left: '-21px'
                    },
                    title: {
                        lineHeight: '24px',
                        verticalAlign: 'middle'
                    }
                },
                subtree: {
                    listStyle: 'none',
                    paddingLeft: '19px'
                },
                loading: {
                    color: '#E2C089'
                }
            }
        }
    };

    private onToggle = (node:any, toggled: any) => {
        let newRoot = this.props.treeRoot;
        if(this.props.currentNode) {
            this.props.currentNode.active = false;
        }
        node.active = true;
        if(node.children){
            node.toggled = toggled;
            newRoot = Object.assign(ReportFileTree.empty(), newRoot);
        }
        this.props.setCurrentNode(node);
        this.props.setTreeRoot(newRoot);
    };


    private deleteSelected = () => {
        if (this.props.currentNode.type == 'leaf') {
            let leaf = this.props.currentNode as ReportFileLeaf;
            this.props.deleteReportFile(this.props.currentTeamId as number, leaf.reportFile.id());
            this.props.setCurrentNode(null);
        }
    };

    private openConfirm = () => {
        this.setState({
            confirmOpen: true
        });
    };

    private closeConfirm = () => {
        this.setState({
            confirmOpen: false
        });
    };


    private shouldShowDelete = () => {
        let owned = false;
        if(this.props.currentNode) {
            owned = this.props.currentNode.isOwnedBy(this.props.currentUserName);
        }
        return this.props.showDeleteButton && (owned || this.props.currentUserHasDeletePermission);
    };



    render() {
        let leaf = null;
        if(this.props.currentNode && this.props.currentNode.type == 'leaf') {
            leaf = this.props.currentNode as ReportFileLeaf;
        }
        return (<div className="General-Container">
            <ConfirmDialog
                declineLabel="Delete"
                open={this.state.confirmOpen}
                msg="Do you really want to delete the report?"
                onAccept={() => {
                    this.closeConfirm();
                    this.deleteSelected();
                }}
                onDecline={this.closeConfirm}
            />
            <ShareReportDialog
                open={this.state.shareReportOpen}
                onClose={() => this.setState({shareReportOpen: false})}
            />
            <div className="row">
                <RaisedButton
                    style={{marginRight: "8px"}}
                    label="Reload"
                    labelPosition="before"
                    primary={true}
                    onClick={this.props.onRefreshReports}
                    icon={<FontIcon className="material-icons">autorenew</FontIcon>}
                />
                {
                    leaf ? <RaisedButton
                        style={{marginRight: "8px"}}
                        containerElement={<a href={Config.makeGetFileString(this.props.currentTeamId, leaf.reportFile.id(),
                            this.props.isShared, leaf.reportFile.boss().name, leaf.reportFile.killDate())} download />}
                        target="_blank"
                        label="download"
                        primary={true}
                        icon={<FontIcon className="material-icons">file_download</FontIcon>}
                    /> : null
                }
                {
                    leaf ?  <RaisedButton
                        style={{marginRight: "8px"}}
                        href={Config.makeGetFileString(this.props.currentTeamId, leaf.reportFile.id(),
                            this.props.isShared, leaf.reportFile.boss().name, leaf.reportFile.killDate())}
                        target="_blank"
                        label="open"
                        primary={true}
                        icon={<FontIcon className="material-icons">open_in_new</FontIcon>}
                    /> : null
                }
                {
                    leaf && this.props.showShareButton ?  <RaisedButton
                        style={{marginRight: "8px"}}
                        label="share"
                        primary={true}
                        onClick={() => this.setState({shareReportOpen: true})}
                        icon={<FontIcon className="material-icons">share</FontIcon>}
                    /> : null
                }
                {
                    leaf && this.shouldShowDelete() ?  <RaisedButton
                        style={{marginRight: "8px"}}
                        label="Delete"
                        secondary={true}
                        onClick={this.openConfirm}
                        icon={<FontIcon className="material-icons">delete</FontIcon>}
                    /> : null
                }
            </div>
            <Treebeard.Treebeard
                data={this.props.treeRoot}
                onToggle={this.onToggle}
                style={this.treeStyle}
            />
        </div>);
    }
}

/**
 * @see ReportListTreeModule
 * @author Niklas
 * @since 27.10.2017
 */
export const ReportListTree: React.ComponentClass<ReportListTreeLocalProps> = connect(ReportListTreeModule.mapStateToProps, ReportListTreeModule.mapDispatchToProps)(ReportListTreeModule);