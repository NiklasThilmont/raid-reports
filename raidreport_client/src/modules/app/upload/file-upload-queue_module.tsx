import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {LinearProgress, List, ListItem, Paper, Subheader} from 'material-ui';
import * as Immutable from 'immutable';
import {QueuebleFile} from '../../../model/QueueableFile';
import {FileUploadStatus} from '../../../model/FileUploadStatus';
import {UploadingFile} from '../../../model/UploadingFile';
import {isNullOrUndefined} from 'util';

interface FileUploadQueueProps {
    filesToUpload: Immutable.List<QueuebleFile>;
    uploadStatus: FileUploadStatus;
    fileUploadCompletion: number;
    uploadingFile: UploadingFile;
}

interface FileUploadQueueLocalProps {

}

interface FileUploadQueueLocalState {

}

interface FileUploadQueueDispatch {

}

class FileUploadQueueModule extends React.Component<FileUploadQueueProps & FileUploadQueueLocalProps & FileUploadQueueDispatch, FileUploadQueueLocalState> {

    static mapStateToProps(state: AppState, localProps: FileUploadQueueLocalProps): FileUploadQueueProps {
        return {
            filesToUpload: state.appStore.filesToUpload(),
            uploadStatus: state.appStore.fileUploadStatus(),
            fileUploadCompletion: !isNullOrUndefined(state.appStore.uploadingFile()) ? state.appStore.uploadingFile().uploadStatus() : 0,
            uploadingFile: state.appStore.uploadingFile()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): FileUploadQueueDispatch {
        return {}
    }

    /**
     *
     * @returns {any}
     */
    render() {
        return (<div className="Report-Uploader">
            <List>
                {
                    !isNullOrUndefined(this.props.uploadingFile) ?
                        <div>
                            <Subheader>Currently Uploading</Subheader>
                            <ListItem>
                                {this.props.uploadingFile.file().name}
                                <LinearProgress
                                    mode="determinate"
                                    value={this.props.fileUploadCompletion}
                                    style={{marginTop: '20px', height: '10px'}}
                                />
                            </ListItem>
                        </div>: null
                }
                <Subheader>Queue</Subheader>
                {this.props.filesToUpload.map((file, index) => <ListItem key={index}>{file.file().name}</ListItem>).toArray()}
            </List>
        </div>);
    }
}

/**
 * @see FileUploadQueueModule
 * @author Niklas
 * @since 02.07.2017
 */
export const FileUploadQueue: React.ComponentClass<FileUploadQueueLocalProps> = connect(FileUploadQueueModule.mapStateToProps, FileUploadQueueModule.mapDispatchToProps)(FileUploadQueueModule);