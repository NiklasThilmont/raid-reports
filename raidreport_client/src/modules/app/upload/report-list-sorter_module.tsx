import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../../model/AppState';
import {Paper, RadioButtonGroup, RadioButton, Subheader} from 'material-ui';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import {DateUtils} from '../../../util/DateUtils';

interface ReportListSorterProps {
    selectedField: "date" | "name" | "wing_boss";
}

interface ReportListSorterLocalProps {

}

interface ReportListSorterLocalState {

}

interface ReportListSorterDispatch {
    changeSortField(value: "date" | "name" | "wing_boss"): void;
}

class ReportListSorterModule extends React.Component<ReportListSorterProps & ReportListSorterLocalProps & ReportListSorterDispatch, ReportListSorterLocalState> {

    static mapStateToProps(state: AppState, localProps: ReportListSorterLocalProps): ReportListSorterProps {
        return {
            selectedField: state.appStore.fileTreeSortField()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ReportListSorterDispatch {
        return {
            changeSortField: value => dispatch(AppActionCreator.SortReportFiles(true, value))
        };
    }

    private handleRadioButtonChange = (event: any, value: "date" | "name" | "wing_boss") => {
        this.props.changeSortField(value);
    };

    render() {
        return (<Paper style={{backgroundColor: '#21252B', color: 'white', padding: '30px', borderRadius: '20px', width: '100%'}}>
            <h4>Sort Options</h4>
            <RadioButtonGroup
                name="sorting"
                defaultSelected="wing_boss"
                valueSelected={this.props.selectedField}
                onChange={this.handleRadioButtonChange}
            >
                <RadioButton
                    value="wing_boss"
                    label="Wing and Boss"
                />
                <RadioButton
                    value="date"
                    label="Time of Kill"
                />
                <RadioButton
                    value="name"
                    label="Boss name"
                />
            </RadioButtonGroup>
        </Paper>);
    }
}

/**
 * @see ReportListSorterModule
 * @author Niklas
 * @since 03.07.2017
 */
export const ReportListSorter: React.ComponentClass<ReportListSorterLocalProps> = connect(ReportListSorterModule.mapStateToProps, ReportListSorterModule.mapDispatchToProps)(ReportListSorterModule);