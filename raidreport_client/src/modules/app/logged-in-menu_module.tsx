import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppState} from '../../model/AppState';
import {Divider, IconButton, IconMenu, MenuItem, SvgIcon} from 'material-ui';
import {Link, NavLink} from 'react-router-dom';
import {
    HomeIcon,
    GroupIcon,
    InfoIcon,
    PowerIcon,
    InfoOutlineIcon,
    SettingsIcon,
    AdbIcon,
    NotesIcon
} from '../icons/MaterialIcons';
import {AppActionCreator} from '../../reducer/app/AppActionCreator';
import {RouteConfig} from "../../util/Config";

interface LoggedInMenuProps {
    isAdmin: boolean;
}

interface LoggedInMenuLocalProps {

}

interface LoggedInMenuLocalState {
    open: boolean;
}

interface LoggedInMenuDispatch {
    logOutUser(): void;
}

class LoggedInMenuModule extends React.Component<LoggedInMenuProps & LoggedInMenuLocalProps & LoggedInMenuDispatch, LoggedInMenuLocalState> {

    constructor(props: LoggedInMenuProps & LoggedInMenuLocalProps & LoggedInMenuDispatch) {
        super(props);
        this.state = {
            open: false
        }
    }

    static mapStateToProps(state: AppState, localProps: LoggedInMenuLocalProps): LoggedInMenuProps {
        return {
            isAdmin: state.appStore.isAdmin()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): LoggedInMenuDispatch {
        return {
            logOutUser: () => dispatch(AppActionCreator.AsyncLogOutUser())
        };
    }


    private setOpen = (open: boolean) => {
        this.setState({ open: open })
    };

    private open = () => {
        this.setOpen(true);
    };

    private close = () => {
        this.setOpen(false);
    };



    private handeLogOut = () => {
        this.close();
        this.props.logOutUser();
    };

    render() {
        return (<IconMenu
                style={{display: 'inline-block'}}
                iconButtonElement={<IconButton iconClassName="material-icons">menu</IconButton>}
                open={this.state.open}
                onTouchTap={this.open}
                onRequestChange={(open) => this.setOpen(open)}
        >
            <NavLink to="/raidreport">
                <MenuItem primaryText="Home"
                          leftIcon={<HomeIcon className="Icon-In-Menu"/>}
                          onClick={this.close}
                >
                </MenuItem>
            </NavLink>
            <NavLink to="/raidreport/news">
                <MenuItem primaryText="News"
                          leftIcon={<NotesIcon className="Icon-In-Menu"/>}
                          onClick={this.close}
                >
                </MenuItem>
            </NavLink>
            {this.props.isAdmin  ?
                <NavLink to="/raidreport/users/manage">
                    <MenuItem primaryText="Manage Users"
                              leftIcon={<GroupIcon className="Icon-In-Menu"/>}
                              onClick={this.close}
                    />
                </NavLink> : null}
            {this.props.isAdmin ?
                <NavLink to={RouteConfig.ROUTE_MANAGE_BOSSES}>
                    <MenuItem primaryText="Manage Bosses"
                              leftIcon={<AdbIcon className="Icon-In-Menu"/>}
                              onClick={this.close}
                    />
                </NavLink> : null}
            <Divider/>
            <NavLink to={RouteConfig.ROUTE_MANAGE_OWN_ACCOUNT}>
                <MenuItem primaryText="Manage Account"
                          leftIcon={<SettingsIcon className="Icon-In-Menu"/>}
                          onClick={this.close}
                />
            </NavLink>
            <NavLink to="/raidreport/info">
                <MenuItem primaryText="Info"
                          leftIcon={<InfoOutlineIcon className="Icon-In-Menu"/>}
                          onClick={this.close}
                />
            </NavLink>
            <MenuItem
                primaryText="Report a Bug"
                href="https://bitbucket.org/NiklasThilmont/raid-reports/issues?status=new&status=open"
                target="_blank"
                leftIcon={<SvgIcon>
                    <g fill="currentColor" fill-rule="evenodd">
                        <path
                            d="M5 12.991c0 .007 14.005.009 14.005.009C18.999 13 19 5.009 19 5.009 19 5.002 4.995 5 4.995 5 5.001 5 5 12.991 5 12.991zM3 5.01C3 3.899 3.893 3 4.995 3h14.01C20.107 3 21 3.902 21 5.009v7.982c0 1.11-.893 2.009-1.995 2.009H4.995A2.004 2.004 0 0 1 3 12.991V5.01zM19 19c-.005 1.105-.9 2-2.006 2H7.006A2.009 2.009 0 0 1 5 19h14zm1-3a2.002 2.002 0 0 1-1.994 2H5.994A2.003 2.003 0 0 1 4 16h16z"
                            fill-rule="nonzero" role="presentation"/>
                        <path
                            d="M10.674 11.331c.36.36.941.36 1.3 0l2.758-2.763a.92.92 0 0 0-1.301-1.298l-2.108 2.11-.755-.754a.92.92 0 0 0-1.3 1.3l1.406 1.405z"
                            role="presentation"/>
                    </g>
                </SvgIcon>}
            />
            <Divider/>
            <NavLink to="/raidreport">
                <MenuItem primaryText="Log Out"
                          leftIcon={<PowerIcon className="Icon-In-Menu"/>}
                          onClick={this.handeLogOut}
                />
            </NavLink>
        </IconMenu>);
    }
}

/**
 * @see LoggedInMenuModule
 * @author Niklas
 * @since 14.10.2017
 */
export const LoggedInMenu: React.ComponentClass<LoggedInMenuLocalProps> = connect(LoggedInMenuModule.mapStateToProps, LoggedInMenuModule.mapDispatchToProps)(LoggedInMenuModule);