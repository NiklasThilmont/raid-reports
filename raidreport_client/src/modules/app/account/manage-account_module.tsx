import {connect} from 'react-redux';
import * as React from 'react';
import * as redux from 'redux';
import {AppStore} from '../../../model/AppStore';
import {User} from "../../../model/user/User";
import {AppState} from "../../../model/AppState";
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import {UserActionCreator} from '../../../reducer/user/UserActionCreator';
import {FlatButton, FontIcon, IconButton, RaisedButton, TextField} from 'material-ui';
import {SaveIcon} from '../../icons/MaterialIcons';

interface ManageAccountProps {
    apiKey: string;
    username: string;
}

interface ManageAccountLocalProps {

}

interface ManageAccountLocalState {

}

interface ManageAccountDispatch {
    loadAPIkey(): void;
    setAPIKey(key: string): void;
    saveAPIKey(): void;
    deleteAPIKey(): void;
}

class ManageAccountModule extends React.Component<ManageAccountProps & ManageAccountLocalProps & ManageAccountDispatch, ManageAccountLocalState> {

    constructor(props: ManageAccountProps & ManageAccountLocalProps & ManageAccountDispatch) {
        super(props);
    }

    static mapStateToProps(state: AppState, localProps: ManageAccountLocalProps): ManageAccountProps {
        return {
            apiKey: state.users.apiKey(),
            username: state.appStore.username()
        }
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): ManageAccountDispatch {
        return {
            loadAPIkey: () => dispatch(UserActionCreator.AsyncRetrieveKey()),
            setAPIKey: key => dispatch(UserActionCreator.SetAPIKey(key)),
            saveAPIKey: () => dispatch(UserActionCreator.AsyncSaveAPIKey()),
            deleteAPIKey: () => dispatch(UserActionCreator.AsyncDeleteAPIKey())
        }
    }



    public componentDidMount(): void {
        this.props.loadAPIkey();
    }

    private renderAPIKey() {
        return <div>
            <TextField value={this.props.apiKey}
                       onChange={(e, v) => this.props.setAPIKey(v)}
                       className="APIKey"
                       floatingLabelText="API Key"
            />
            <FlatButton label="Save"
                        icon={<FontIcon className="material-icons">save</FontIcon>}
                        onClick={this.props.saveAPIKey}
            />
            <FlatButton label="Delete"
                        icon={<FontIcon className="material-icons">delete</FontIcon>}
                        onClick={this.props.deleteAPIKey}
            />
        </div>
    }

    render() {
        return (<div>
            <div className="General-Container">
                <h2>Account Details for {this.props.username}</h2>
                <h3>API Key</h3>
                {this.renderAPIKey()}
            </div>
        </div>);
    }
}

/**
 * @see ManageAccountModule
 * @author Niklas
 * @since 11.11.2017
 */
export const ManageAccount: React.ComponentClass<ManageAccountLocalProps> = connect(ManageAccountModule.mapStateToProps, ManageAccountModule.mapDispatchToProps)(ManageAccountModule);