import * as React from 'react';
import {INewsPost} from '../../../model/NewsPost';
import axios, {AxiosRequestConfig, AxiosError} from 'axios';
import {Config} from '../../../util/Config';
import {SingleNewsPost} from './single-newspost';
import {AppActionCreator} from '../../../reducer/app/AppActionCreator';
import {Paper} from 'material-ui';

interface NewsFeedProps {

}


interface NewsFeedLocalState {
    newsPosts: Array<INewsPost>;
}

export class NewsFeedModule extends React.Component<NewsFeedProps, NewsFeedLocalState> {

    constructor(props: NewsFeedProps) {
        super(props);
        this.state = {
            newsPosts: []
        }
    }

    public componentDidMount() {
        this.loadPosts();
    }

    private loadPosts = () => {
        const config: AxiosRequestConfig = {
            params: {
                size: 5,
                sort: "postDate,desc"
            }
        };
        axios.get(Config.makeGetNewsPostURL(), config).then(response => {
            this.setState({newsPosts: response.data.content})
        }).catch(error => {
            console.error(error);
            AppActionCreator.showError("Could not load newsfeed.")
        })
    };

    render() {
        return (<div>
            <div className="General-Container Margined-Button">
                <h4>What's New</h4>
            </div>

            {this.state.newsPosts.map(post =>
                <SingleNewsPost key= {post.id} newsPost={post}/>
            )}
        </div>);
    }
}