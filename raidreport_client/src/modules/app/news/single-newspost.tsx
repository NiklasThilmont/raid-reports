import * as React from 'react';
import {INewsPost} from '../../../model/NewsPost';
import {Card, CardHeader, CardText} from 'material-ui';

interface SingleNewsPostProps {
    newsPost: INewsPost;
}


interface SingleNewsPostLocalState {

}

export class SingleNewsPost extends React.Component<SingleNewsPostProps, SingleNewsPostLocalState> {

    constructor(props: SingleNewsPostProps) {
        super(props);
    }

    render() {
        return (<div className="General-Container Margined-Button">
            <CardHeader title={this.props.newsPost.title}
                        subtitle={this.props.newsPost.subtitle}

            />
            <div style={{width: "60%"}}>
                <span dangerouslySetInnerHTML={{ __html: "<div class=\"News-Post-Body\">" + this.props.newsPost.body + "</div>" }}/>
            </div>
            <br/>
            <div>
                <span className="News-Post-Author-Text">Posted {this.props.newsPost.postDate} by {this.props.newsPost.author}</span>
            </div>
        </div>);
    }
}