import * as React from 'react';
import {Dialog, FlatButton} from 'material-ui';

interface ConfirmDialogProps {
    open: boolean;
    msg: string;
    declineLabel: string;
    onAccept(): void;
    onDecline(): void;
}

export const ConfirmDialog = (props: ConfirmDialogProps) => <div>
    <Dialog
        open={props.open}
        title="Discard Changes"
        actions={[<FlatButton
            label="Cancel"
            onClick={props.onDecline}
            />,<FlatButton
            label={props.declineLabel}
            secondary={true}
            onClick={props.onAccept}
        />]}
    >
        {props.msg}
    </Dialog>
</div>;