import * as React from 'react';
import {FontIcon, SvgIcon} from 'material-ui';
interface IconProps {
    className?: string;
}

export const GroupIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>group</FontIcon>;
export const AdbIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>adb</FontIcon>;
export const GroupAddIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>group_add</FontIcon>;
export const HomeIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>home</FontIcon>;
export const InfoIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>info</FontIcon>;
export const InfoOutlineIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>info_outline</FontIcon>;
export const PowerIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>power_settings_new</FontIcon>;
export const ExitToAppIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>exit_to_app</FontIcon>;
export const ShareIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>share</FontIcon>;
export const LockIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>lock</FontIcon>;
export const OpenInNewIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>open_in_new</FontIcon>;
export const SettingsIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>settings</FontIcon>;
export const SaveIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>save</FontIcon>;
export const NotesIcon = (props: IconProps) => <FontIcon className={"material-icons " + props.className}>notes</FontIcon>;
