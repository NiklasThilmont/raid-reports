import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin = require('react-tap-event-plugin');
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import {combineReducers, applyMiddleware} from 'redux';
import {AppReducer} from './reducer/app/AppReducer';
import {Provider, Store} from 'react-redux';
import thunkMiddleware from "redux-thunk";
import {createStore} from 'redux';
import {App} from './App';
import {isNullOrUndefined} from 'util';
import {Config} from './util/Config';
import * as Cookies from 'js-cookie';
import {AppActionCreator} from './reducer/app/AppActionCreator';
import {UserReducer} from './reducer/user/UserReducer';
import {RaidTeamReducer} from './reducer/teams/RaidTeamReducer';
import {ShareReducer} from './reducer/share/ShareReducer';
import axios from 'axios';

injectTapEventPlugin();

const rootReducer = combineReducers({
    appStore: AppReducer.Reduce,
    users: UserReducer.reduce,
    teams: RaidTeamReducer.reduce,
    share: ShareReducer.reduce
});

const store: Store<any> = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);

// Clean up after the mess previously made.
Cookies.remove(Config.USERNAME_COOKIE_NAME);
Cookies.remove(Config.PASSWORD_COOKIE_NAME);

let username = localStorage.getItem(Config.USERNAME_COOKIE_NAME);
let password = localStorage.getItem(Config.PASSWORD_COOKIE_NAME);
if(!isNullOrUndefined(username) && !isNullOrUndefined(password)) {
    store.dispatch(AppActionCreator.ChangeUsername(username));
    store.dispatch(AppActionCreator.ChangePassword(password));
    store.dispatch(AppActionCreator.AsyncLoginUser(username, password, true));
}

//
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
            <App />
        </MuiThemeProvider>
    </Provider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
