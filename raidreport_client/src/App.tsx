import {connect} from 'react-redux';
import * as React from 'react';
import {CSSProperties} from 'react';
import * as redux from 'redux';
import {AppState} from './model/AppState';
import {AppBar, BottomNavigation, BottomNavigationItem, FontIcon, IconButton, RaisedButton, SvgIcon} from 'material-ui';
import {Config, RouteConfig} from './util/Config';
import {LoginButton} from './modules/app/login-button_module';
import {AppActionCreator} from './reducer/app/AppActionCreator';

import {Route} from 'react-router';
import {UserList} from './modules/app/usermanagement/user-list_module';
import {BrowserRouter, Link} from 'react-router-dom';
import {LoggedInMenu} from './modules/app/logged-in-menu_module';
import {BuildInfo} from './model/BuildInfo';
import {RaidTeamDashboard} from './modules/app/teams/raid-team-dashboard_module';
import {RaidTeamReports} from './modules/app/teams/singleteam/single-raid-team_module';
import {AppInfo} from './modules/app/info/app-info_module';
import {SharedTeam} from './modules/app/shared/team/shared-team_module';
import {ChangePasswordDialog} from "./modules/app/usermanagement/change-password_module";
import {ManageAccount} from "./modules/app/account/manage-account_module";
import {TeamPerformance} from './modules/app/teams/singleteam/performance/team-performance_module';
import {RaidBossList} from './modules/app/management/raid-boss-list_module';
import {NewsFeedModule} from './modules/app/news/newsfeed-module';
const AlertContainer = require('react-alert').default;

interface AppProps {
    isLoggedIn: boolean;
}

interface AppLocalProps {
    location?: any; // set by router
}

interface AppLocalState {

}


interface AppDispatch {

}



class AppModule extends React.Component<AppProps & AppLocalProps & AppDispatch, AppLocalState> {

    static mapStateToProps(state: AppState, localProps: AppLocalProps): AppProps {
        return {
            isLoggedIn: state.appStore.isLoggedIn()
        };
    }

    static mapDispatchToProps(dispatch: redux.Dispatch<AppState>): AppDispatch {
        return {

        };
    }

    private renderLoginOrLogoutButton = () => {
        if(this.props.isLoggedIn) return <LoggedInMenu/>;
        return <LoginButton/>;
    };

    private readonly alertOptions = {
        offset: 14,
        position: 'bottom left',
        theme: 'dark',
        time: 0,
        transition: 'scale'
    };

    render() {
        return (
            <BrowserRouter>
                <div style={{height: "100%"}}>
                    <AppBar
                        style={{backgroundColor: '#21252B', color: 'white'}}
                        title={<span>
                            <span className="AppTitle">Raid-Report</span>
                        </span>}
                        iconElementLeft={<span className="Aligner">{this.renderLoginOrLogoutButton()}</span>}
                    />
                    <div style={{marginBottom: "40px", height: "100%"}}>
                        <Route exact path="/raidreport" component={RaidTeamDashboard}/>
                        <Route exact path="/raidreport/news" component={NewsFeedModule}/>
                        <Route path="/raidreport/users/manage" component={UserList}/>
                        <Route path={RouteConfig.ROUTE_TEAM_OVERVIEW} component={RaidTeamReports}/>
                        <Route path="/raidreport/info" component={AppInfo}/>
                        <Route path={RouteConfig.ROUTE_SHARED_TEAM} component={SharedTeam}/>
                        <Route path={RouteConfig.ROUTE_RESET_PASSWORD} component={ChangePasswordDialog}/>
                        <Route path={RouteConfig.ROUTE_MANAGE_OWN_ACCOUNT} component={ManageAccount}/>
                        <Route path={RouteConfig.ROUTE_TEAM_PERFORMANCE} component={TeamPerformance}/>
                        <Route path={RouteConfig.ROUTE_MANAGE_BOSSES} component={RaidBossList}/>
                        <AlertContainer  ref={(a:any) => AppActionCreator.APP_MESSAGE_CONTAINER = a} {...this.alertOptions}/>
                    </div>
                    <footer className="Bottom-Navbar">
                        <BottomNavigation style={{backgroundColor: '#21252b'}} selectedIndex={-1}>
                            <BottomNavigationItem
                                label="Home"
                                icon={  <Link to="/raidreport"><FontIcon className="material-icons">home</FontIcon> </Link>}
                            />
                            <BottomNavigationItem
                                label="Info"
                                    icon={  <Link to="/raidreport/info"><FontIcon className="material-icons">info_outline</FontIcon> </Link>}
                                />
                            </BottomNavigation>
                    </footer>
                </div>
            </BrowserRouter>
        );
    }
}

/**
 * @see AppModule
 * @author Niklas
 * @since 01.07.2017
 */
export const App: React.ComponentClass<AppLocalProps> = connect(AppModule.mapStateToProps, AppModule.mapDispatchToProps)(AppModule);