import {doop} from 'doop';
@doop
export class QueuebleFile {
    @doop public get file() {return doop<File, this>()};

    constructor(file: File) {
        return this.file(file);
    }

    public static of(file: File) {
        return new QueuebleFile(file);
    }
}