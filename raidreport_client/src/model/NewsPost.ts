export interface INewsPost {
    id: number;
    postDate: string;
    creationDate: string;
    title: string;
    subtitle: string;
    body: string;
    author: string;
}