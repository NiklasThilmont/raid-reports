import {isNullOrUndefined} from 'util';
export enum RaidBossCode{
    W1_B1,
    W1_B2,
    W1_B3,
    W2_B1,
    W2_B2,
    W2_B3,
    W3_B1,
    W3_B2,
    W3_B3,
    W4_B1,
    W4_B2,
    W4_B3,
    W4_B4
}

export namespace RaidBossCode {
    export function mapFromBoss(boss: string): RaidBossCode {
        if(boss === "VALE_GUARDIAN") return RaidBossCode.W1_B1;
        if(boss === "GORSEVAL") return RaidBossCode.W1_B2;
        if(boss === "SABETHA") return RaidBossCode.W1_B3;
        if(boss === "SLOTHASOR") return RaidBossCode.W2_B1;
        if(boss === "TRIO") return RaidBossCode.W2_B2;
        if(boss === "MATTHIAS") return RaidBossCode.W2_B3;
        if(boss === "KEEP_CONSTRUCT") return RaidBossCode.W3_B1;
        if(boss === "XERA") return RaidBossCode.W3_B3;
        if(boss === "CAIRN") return RaidBossCode.W4_B1;
        if(boss === "OVERSEER") return RaidBossCode.W4_B2;
        if(boss === "SAMAROG") return RaidBossCode.W4_B3;
        if(boss === "DEIMOS") return RaidBossCode.W4_B4;
        return undefined;
    }
}