import {doop} from 'doop';
import {RaidTeam} from '../team/RaidTeam';

@doop
export class PublicStore {
    @doop public get sharedTeam() { return doop<RaidTeam, this>()}

    private constructor(sharedTeam: RaidTeam) {
        return this.sharedTeam(sharedTeam);
    }

    public static empty() {
        return new PublicStore(null);
    }
}