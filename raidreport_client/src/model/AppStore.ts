
import {doop} from 'doop';
import * as Immutable from 'immutable';
import {ReportFile} from './ReportFile';
import {UploadingFile} from './UploadingFile';
import {FileUploadStatus} from './FileUploadStatus';
import {QueuebleFile} from './QueueableFile';
import {ReportFileTree, TreeNode} from './ReportFileTree';
import {Config} from '../util/Config';
import {isNullOrUndefined} from 'util';
import {IAuthUser, User} from './user/User';
import {AxiosRequestConfig} from 'axios';
import {RaidBoss} from './RaidBoss';


@doop
export class AppStore {
    /**
     * All uploaded report files. These files are retrieved from the API.
     * @returns {Doop<Immutable.List<ReportFile>, this>}
     */
    @doop public get reportFiles() {return doop<Immutable.List<ReportFile>, this>()};

    @doop public get username() {return doop<string, this>()};

    @doop public get password() {return doop<string, this>()};

    @doop public get isLoggedIn() {return doop<boolean, this>()};

    @doop public get loginErrorText() {return doop<string, this>()};

    @doop public get uploadingFile() {return doop<UploadingFile, this>()};

    @doop public get fileUploadStatus() {return doop<FileUploadStatus, this>()};

    @doop public get filesToUpload() {return doop<Immutable.List<QueuebleFile>, this>()};

    @doop public get fileTreeRoot() {return doop<ReportFileTree, this>()};

    @doop public get fileTreeSortField() {return doop<"date" | "name" | "wing_boss", this>()};

    @doop public get userAuthorities() {return doop<Immutable.List<string>, this>()};

    @doop public get currentNode() {return doop<TreeNode, this>()};

    @doop public get bosses() {return doop<Immutable.Map<number, RaidBoss>, this>()};



    private constructor(reportFiles: Immutable.List<ReportFile>, username: string, password: string, isLoggedIn: boolean,
        loginErrorText: string, uploadingFile: UploadingFile, fileUploadStatus: FileUploadStatus, filesToUpload: Immutable.List<QueuebleFile>,
        fileTreeRoot: ReportFileTree, fileTreeSortField: "date" | "name" | "wing_boss", userAuthorities: Immutable.List<string>,
                        currentNode: TreeNode, bosses: Immutable.Map<number, RaidBoss>) {
        return this.reportFiles(reportFiles).username(username).password(password).isLoggedIn(isLoggedIn).loginErrorText(loginErrorText)
            .uploadingFile(uploadingFile).fileUploadStatus(fileUploadStatus).filesToUpload(filesToUpload).fileTreeRoot(fileTreeRoot)
            .fileTreeSortField(fileTreeSortField).userAuthorities(userAuthorities).currentNode(null).bosses(bosses);
    }

    public static empty() {
        return new AppStore(Immutable.List<ReportFile>(), "", "", false, null, null, FileUploadStatus.NONE,
            Immutable.List<QueuebleFile>(), ReportFileTree.empty(), Config.DEFAULT_SORT_FIELD, Immutable.List<string>(),
            null,  Immutable.Map<number, RaidBoss>());
    }

    public isAdmin(): boolean {
        return !isNullOrUndefined(this.userAuthorities().find((value, key, iter) => value === "ADMIN"));
    }

    public makeAuthConfig(): AxiosRequestConfig {
        return {
            auth: {
                username: this.username(),
                password: this.password()
            }
        };
    }

}