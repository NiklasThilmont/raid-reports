import {IPlayerAccount} from './IPlayerAccount';
export interface IParticipant {
    id: number;
    playerAccount: IPlayerAccount;
    className: string;
}