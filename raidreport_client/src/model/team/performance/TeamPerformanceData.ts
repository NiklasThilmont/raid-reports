export interface ITeamPerformanceData {
    killTimesInMs: Map<String, Array<number>>;
    successCount: number;
    failCount: number;
    totalTries: number;
    participationPerPlayer: Object;
}