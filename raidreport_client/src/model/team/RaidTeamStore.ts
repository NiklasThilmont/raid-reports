import {doop} from 'doop';
import {RaidTeam} from './RaidTeam';
import * as Immutable from 'immutable';
import {CreateTeamStatus} from './CreateTeamStatus';
import {ITeamPerformanceData} from './performance/TeamPerformanceData';
import {IRenderTarget} from './RenderTarget';

@doop
export class RaidTeamStore {
    @doop public get raidTeams() {return doop<Immutable.List<RaidTeam>, this>()};
    @doop public get currentTeamId() {return doop<number, this>()};
    @doop public get manageDialogOpen() {return doop<boolean, this>()};
    @doop public get createTeamStatus() {return doop<CreateTeamStatus, this>()};
    @doop public get performanceData() {return doop<ITeamPerformanceData, this>()};
    @doop public get availableRenderTargets() {return doop<Array<IRenderTarget>, this>()};

    private constructor(raidTeams: Immutable.List<RaidTeam>, currentTeamId: number,manageDialogOpen: boolean,
                        createTeamStatus: CreateTeamStatus, performanceData: ITeamPerformanceData, availableRenderTargets: Array<IRenderTarget>) {
        return this.raidTeams(raidTeams).currentTeamId(currentTeamId).manageDialogOpen(manageDialogOpen)
            .createTeamStatus(createTeamStatus).performanceData(performanceData).availableRenderTargets(availableRenderTargets);
    }

    public static empty(): RaidTeamStore {
        return new RaidTeamStore(Immutable.List<RaidTeam>(), null, false, CreateTeamStatus.CLOSED, null, []);
    }

    public getTeamById(id: number) {
        return this.raidTeams().find(team => team.id() == id);
    }

    public hasPermissionInCurrentTeam (username: string, permission: string) {
        let currentTeam = this.getTeamById(this.currentTeamId());
        if(currentTeam) {
            let user = currentTeam.getUser(username);
            return user.hasPermission(permission);
        }
        return false;

    }
}