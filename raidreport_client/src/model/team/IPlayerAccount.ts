export interface IPlayerAccount {
    id: number;
    accountName: string;
}