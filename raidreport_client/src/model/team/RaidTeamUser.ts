import {IUser, User} from '../user/User';
import {doop} from 'doop';
import {RaidTeam} from './RaidTeam';
export interface IRaidTeamUser {
    user: IUser;
    teamInviteStatus: string;
    permissions: Array<string>;
}

@doop
export class RaidTeamUser {
    public static INVITE_STATUS_INVITE_SENT = "INVITE_SENT";
    public static INVITE_STATUS_JOINED = "JOINED";
    public static INVITE_STATUS_NONE = "NONE";
    public static INVITE_STATES = [RaidTeamUser.INVITE_STATUS_NONE, RaidTeamUser.INVITE_STATUS_JOINED, RaidTeamUser.INVITE_STATUS_INVITE_SENT];

   @doop public get user() {return doop<User, this>()};
   @doop public get teamInviteStatus() {return doop<string, this>()};
   @doop public get permissions() {return doop<Array<string>, this>()};

   private constructor(user: User, teamInviteStatus: string, permissions: Array<string>) {
       return this.user(user).teamInviteStatus(teamInviteStatus).permissions(permissions);
   }

   public static of(raidTeamUser: IRaidTeamUser) {
       return new RaidTeamUser(User.of(raidTeamUser.user), raidTeamUser.teamInviteStatus, raidTeamUser.permissions);
   }

   public hasPermission(permission: string): boolean {
       return this.permissions().indexOf(permission) >= 0;
   }
}