import {doop} from 'doop';
import * as Immutable from 'immutable';
import {RaidTeamUser} from './RaidTeamUser';
import {render} from 'react-dom';

export interface IRenderTarget {
    name: string;
    iconLocation: string;
    id: number;
}

@doop
export class RenderTarget {
    @doop public get id() {return doop<number, this>()};
    @doop public get name() {return doop<string, this>()};
    @doop public get iconLocation() {return doop<string, this>()};

    private constructor(id: number, name: string, iconLocation: string) {
        return this.id(id).name(name).iconLocation(iconLocation);
    }

    public static of(renderTarget: IRenderTarget) {
        return new RenderTarget(renderTarget.id, renderTarget.name, renderTarget.iconLocation);
    }
}