import {IRaidTeamUser, RaidTeamUser} from './RaidTeamUser';
import {doop} from 'doop';
import * as Immutable from 'immutable';
import {APIReportFile} from '../ReportFile';
import {isNullOrUndefined} from 'util';
import {IRenderTarget, RenderTarget} from './RenderTarget';

export interface IRaidTeam {
    id: number;
    teamName: string;
    shared: boolean;
    shareId: string;
    renderTarget: IRenderTarget;
    users?: Array<IRaidTeamUser>;
    reports?: Array<APIReportFile>;
}

@doop
export class RaidTeam {
    @doop public get id() {return doop<number, this>()};
    @doop public get teamName() {return doop<string, this>()};
    @doop public get shared() {return doop<boolean, this>()};
    @doop public get shareId() {return doop<string, this>()};
    @doop public get renderTarget() {return doop<IRenderTarget, this>()};
    @doop public get users() {return doop<Immutable.List<RaidTeamUser>, this>()};

    private constructor(id: number, teamName: string, shared: boolean, shareId: string, renderTarget: IRenderTarget, users: Immutable.List<RaidTeamUser>) {
        return this.id(id).teamName(teamName).users(users).shared(shared).shareId(shareId).renderTarget(renderTarget);
    }

    public static of(raidTeam: IRaidTeam) {
        console.debug("Parsing raid team from interface", raidTeam);
        let raidTeamUsers: Array<RaidTeamUser> = [];
        if(!isNullOrUndefined(raidTeam.users)) {
            raidTeamUsers = raidTeam.users.map(RaidTeamUser.of);
        }
        return new RaidTeam(raidTeam.id, raidTeam.teamName, raidTeam.shared, raidTeam.shareId,
            raidTeam.renderTarget, Immutable.List<RaidTeamUser>(raidTeamUsers));
    }

    public getUser(username: string): RaidTeamUser {
        return this.users().find(user => user.user().name() === username);
    }
}