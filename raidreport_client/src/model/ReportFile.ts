import {doop} from 'doop';
import {DateUtils} from '../util/DateUtils';
import {isNullOrUndefined} from 'util';
import {RaidBossCode} from './RaidBossCode';
import {IUser} from './user/User';
import {IParticipant} from './team/Participant';
import {StringUtils} from '../util/StringUtils';
import {Comparators} from '../util/Comparators';
import {IRaidBoss} from './RaidBoss';
export interface APIReportFile {
    id: number;
    raidBoss: IRaidBoss;
    uploadDate: string;
    killDate: string;
    comment: string;
    killTimeInMs: number;
    encounterKillStatus: string;
    user: IUser;
    shareId: string;
    shared: boolean;
    participants: Array<IParticipant>;
}


@doop
export class ReportFile {
    @doop public get id() {return doop<number, this>()}
    @doop public get boss() {return doop<IRaidBoss, this>()}
    @doop public get killDate() {return doop<Date, this>()}
    @doop public get uploaderName() {return doop<string, this>()}
    @doop public get killTimeMs() {return doop<number, this>()}
    @doop public get shareId() {return doop<string, this>()}
    @doop public get shared() {return doop<boolean, this>()}
    @doop public get encounterKillStatus() {return doop<string, this>()}
    @doop public get participants() {return doop<Array<IParticipant>, this>()}



    private constructor(id: number, boss: IRaidBoss, killDate: Date, uploaderName: string, killTimeMs: number,
                        encounterKillStatus: string, participants: Array<IParticipant>, shareId: string, shared: boolean) {
        return this.boss(boss).killDate(killDate).uploaderName(uploaderName)
            .killTimeMs(killTimeMs).encounterKillStatus(encounterKillStatus).id(id)
            .participants(participants).shareId(shareId).shared(shared);
    }

    public static fromAPI(apiReportFile: APIReportFile) {
        let killDate = apiReportFile.killDate ? new Date(Date.parse(apiReportFile.killDate)) : new Date(Date.parse(apiReportFile.uploadDate));
        let raidBoss: IRaidBoss = apiReportFile.raidBoss;
        if (!raidBoss) {
            raidBoss = {
                order: -1,
                name: "???",
                classIds: [],
                id: -2
            }
        }
        return new ReportFile(apiReportFile.id,
            raidBoss,
            killDate,
            apiReportFile.user.username,
            apiReportFile.killTimeInMs,
            apiReportFile.encounterKillStatus,
            apiReportFile.participants.sort(Comparators.compareParticipants),
            apiReportFile.shareId,
            apiReportFile.shared);
    }

    public getDateAndBossString() {
        return DateUtils.formatToDayAndMonth(this.killDate()) + ": " + this.boss();
    }
}