export interface BuildInfo {
    version: string;
    artifact: string;
    name: string;
    group: string;
    time: string;
}