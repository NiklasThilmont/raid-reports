
import {doop} from 'doop';
import {User} from './User';
import * as Immutable from 'immutable';
import {RegisterUserStatus} from './RegisterUserStatus';
@doop
export class UserStore {
    @doop public get users() { return doop<Immutable.List<User>, this>()};

    @doop public get registerUserStatus() {return doop<RegisterUserStatus, this>()}

    @doop public get registerRejectReason() {return doop<string, this>()};

    @doop public get apiKey() {return doop<string, this>()};

    private constructor(users: Immutable.List<User>, registerUserStatus: RegisterUserStatus, registerRejectReason: string, key: string) {
        return this.users(users).registerUserStatus(registerUserStatus).registerRejectReason(registerRejectReason).apiKey(key);
    }

    public static empty() {
        return new UserStore(Immutable.List<User>(), RegisterUserStatus.DIALOG_CLOSED, null, "");
    }
}