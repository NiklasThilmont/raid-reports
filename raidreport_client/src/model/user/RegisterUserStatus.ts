export enum RegisterUserStatus {
    DIALOG_OPEN,
    CREATION_PENDING,
    CREATION_FINISHED,
    DIALOG_CLOSED
}