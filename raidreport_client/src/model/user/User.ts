
import {doop} from 'doop';

export interface IAuthUser {
    username: string;
    password: string;
}

export interface IUser {
    id: number;
    username: string;
    enabled: boolean;
}

@doop
export class User {
    @doop public get id() { return doop<number, this>()};
    @doop public get name() { return doop<string, this>()};
    @doop public get enabled() { return doop<boolean, this>()};

    private constructor(id: number, name: string, enabled: boolean) {
        return this.id(id).name(name).enabled(enabled);
    }

    public static empty(): User {
        return new User(-1, "", false);
    }

    public static of(user: IUser): User {
        return new User(user.id, user.username, user.enabled);
    }
}