import * as React from 'react';
import {ReportFile} from './ReportFile';
import * as Immutable from 'immutable';
import {isNullOrUndefined} from 'util';
import {DateUtils} from '../util/DateUtils';
import {action, doop} from 'doop';
import {StringUtils} from '../util/StringUtils';
import {Comparators} from '../util/Comparators';

export interface TreeNode {
    active: boolean;
    type: string;

    isOwnedBy(username: string): boolean;

    newRef(): TreeNode;
}

@doop
export class ReportFileLeaf implements TreeNode {
    public name: string | JSX.Element;
    public reportFile: ReportFile;
    public active = false;
    public type = "leaf";

    private constructor(name: string | JSX.Element, reportFile: ReportFile) {
        this.name = name;
        this.reportFile = reportFile;
    }


    isOwnedBy(username: string): boolean {
        if(!isNullOrUndefined(this.reportFile)) {
            return this.reportFile.uploaderName() === username;
        }
        return false;
    }

    public newRef() {
        let res = new ReportFileLeaf(this.name, this.reportFile);
        res.active = this.active;
        res.type = this.type;
        return res;
    }

    public static of(file: ReportFile) {
        let name = (
            <span>
                <span className="BossName">{file.boss().name + " "}</span>
                {
                        <span className="SuccessOrFail">{StringUtils.extractKillStatString(file) + ", " }</span>
                }
                <span className="UploadedBy">uploaded by{" " + file.uploaderName()}</span>


        </span>);
        return new ReportFileLeaf(name, file);
    }
}

export class RaidDateNode implements TreeNode {
    public name: string;
    public date: Date;
    public children: Array<ReportFileLeaf>;
    public active = false;
    public type = "node";
    public toggled = false;


    private constructor(name: string, date: Date, children: Array<ReportFileLeaf>) {
        this.name = name;
        this.date = date;
        this.children = children;
    }

    public sameDay = (date: Date) => {
        return DateUtils.sameDay(date, this.date);
    };

    public newRef() {
        let res = new RaidDateNode(this.name, this.date, this.children);
        res.active = this.active;
        res.type = this.type;
        res.toggled = this.toggled;
        return res;
    }

    public isOwnedBy(username: string): boolean {
        return false;
    }

    public addAsLeaf(file: ReportFile) {
        this.children.push(ReportFileLeaf.of(file));
    }

    public toggle() {
        this.toggled = true;
    }


    public sortChildrenByName() {
        this.children.sort(Comparators.compareBoss);
    }

    public sortFileLeafsByKillTime(asc: boolean) {
        this.children.sort(Comparators.MakeReportFileLeafDateComparator(asc))
    }

    public sortFileLeafsByBoss(asc: boolean) {
        this.children.sort(Comparators.MakeReportFileLeafBossComparator(asc));
    }

    public static of(file: ReportFile) {
        let res = new RaidDateNode(DateUtils.formatToDayAndMonth(file.killDate()), file.killDate(), []);
        res.addAsLeaf(file);
        return res;
    }
}

export class ReportFileTree implements TreeNode{
    public name: string;
    public children: Array<RaidDateNode>;
    public toggled = true;
    public active = false;
    public type = "root";

    private constructor(name: string, children: Array<RaidDateNode>) {
        this.name = name;
        this.children = children;
    }


    public newRef() {
        let res = new ReportFileTree(this.name, this.children);
        res.toggled = this.toggled;
        res.active = this.active;
        res.type = this.type;

        return res;
    }

    public isOwnedBy(username: string): boolean {
        return false;
    }

    private addAsChild(file: ReportFile) {
        this.children.push(RaidDateNode.of(file));
    }

    public sortChildrenByDate() {
        this.children.sort((a, b) => DateUtils.compare(a.date, b.date) * -1);
        this.children.forEach(child => child.sortChildrenByName());
    }

    /**
     * Invokes sorting of all leafs that represents a report file.
     * These file leafs are being sorted by their point in time.
     * @param asc
     */
    public sortFileLeafsByKillTimePoint(asc: boolean) {
        this.children.forEach((value, index, array) => value.sortFileLeafsByKillTime(asc))
    }

    /**
     * Invokes sorting of report file leafs that represents a report file.
     * These file leafs are being sorted by their wing and boss.
     * @param asc
     */
    public sortFileLeafsByBoss(asc: boolean) {
        this.children.forEach((value, index, array) => value.sortFileLeafsByBoss(asc))
    }

    public sortFileLeafsByName(asc: boolean) {
        this.children.forEach((value, index, array) => value.sortChildrenByName());
    }



    private addFileToTree(file: ReportFile) {
        // Iterate through children, try to find one with the same date
        let childFound: boolean = false;
        this.children.forEach(child => {
            if(!childFound && child.sameDay(file.killDate())) {
                childFound = true;
                child.addAsLeaf(file);
            }
        });
        // If no same date was found, add as new date.
        if(!childFound) {
            this.addAsChild(file);
        }
    }

    public static empty() {
        return new ReportFileTree("Reports", []);
    }

    public defineInitialToggles() {
        let today = new Date(Date.now());
        let node = this.children.find(dateNode => dateNode.sameDay(today));
        // If a node with today was found, open it
        // otherwise, open the first
        if(isNullOrUndefined(node)) {
            node = this.children[0];
        }
        if(!isNullOrUndefined(node)) {
            node.toggle();
        }
    }

    /**
     * Copies this instance and returns it. Does not copy children.
     */
    public flatCopy() {
        return new ReportFileTree(this.name, this.children);
    }

    public static fromFileList(files: Immutable.List<ReportFile>) {

        let tree: ReportFileTree = ReportFileTree.empty();
        files.forEach((value, key, iter) => tree.addFileToTree(value));
        tree.sortChildrenByDate();
        tree.defineInitialToggles();
        return tree;
    }

}