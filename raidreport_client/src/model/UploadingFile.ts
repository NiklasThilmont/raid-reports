import {doop} from 'doop';
@doop
export class UploadingFile {
    @doop public get file() {return doop<File, this>()};
    @doop public get uploadStatus() {return doop<number, this>()};

    constructor(file: File, uploadStatus: number) {
        return this.file(file).uploadStatus(uploadStatus);
    }

    public static of(file: File) {
        return new UploadingFile(file, 0);
    }
}