import {AppStore} from './AppStore';
import {UserStore} from './user/UserStore';
import {RaidTeamStore} from './team/RaidTeamStore';
import {PublicStore} from './share/PublicStore';

export interface AppState {
    appStore: AppStore;
    users: UserStore;
    teams: RaidTeamStore;
    share: PublicStore;
}