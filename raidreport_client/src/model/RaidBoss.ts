import {doop} from 'doop';
import * as Immutable from "immutable";

export interface IRaidBoss {
    id: number;
    name: string;
    order: number;
    classIds: Array<number>;
}

@doop
export class RaidBoss {
    @doop public get id() {return doop<number, this>()};
    @doop public get name() {return doop<string, this>()};
    @doop public get order() {return doop<number, this>()};
    @doop public get classIds() {return doop<Immutable.List<number>, this>()};

    public constructor(id: number, name: string, order: number, classIds: Immutable.List<number>) {
        return this.id(id).name(name).order(order).classIds(classIds);
    }

    public static fromAPI(data: IRaidBoss) {
        return new RaidBoss(data.id, data.name, data.order, Immutable.List<number>(data.classIds));
    }

    public toApi(): IRaidBoss {
        return {
            id: this.id() === -1 ? null : this.id(),
            name: this.name(),
            classIds: this.classIds().toArray(),
            order: this.order()
        }
    }
 }